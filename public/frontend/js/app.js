var trailer = "null";

function getCookie(t) {
    for (var e = t + "=", i = decodeURIComponent(document.cookie).split(";"), a = 0; a < i.length; a++) {
        for (var s = i[a];
            " " == s.charAt(0);) s = s.substring(1);
        if (0 == s.indexOf(e)) return s.substring(e.length, s.length)
    }
    return ""
}

function setCookie(t, e, i) {
    var a = new Date;
    a.setTime(a.getTime() + 24 * i * 60 * 60 * 1e3);
    var s = "expires=" + a.toUTCString();
    document.cookie = t + "=" + e + ";" + s + ";path=/"
}
$(function () {
    $(".alert-home>button").click(function () {
        $(this).parent().fadeOut()
    }), $(".gplay-box>span").click(function () {
        $(".gplay-box").hide(), setCookie("gplay", "true", 10)
    }), "" == getCookie("gplay") && $(".gplay-box").fadeIn(), $(document).ready(function () {
        $(".menu-left-btn").on("click", function () {
            $(".sidebar").toggleClass("active-sidebar"), $(".close-menu-left-btn").show()
        }), $(".close-menu-left-btn").on("click", function () {
            $(".sidebar").toggleClass("active-sidebar"), $(".close-menu-left-btn").hide()
        })
    }), setTimeout(function () {
        $("#paypal-form-pay").submit()
    }, 2e3), setTimeout(function () {
        $(".alert-home").fadeOut()
    }, 3e3);
    var t = "none",
        e = "none",
        i = "none",
        a = "none";
    $(".btn-fav").click(function () {
        var t = $(this).attr("alt"),
            e = $(this);
        e.html("<i class='fas fa-circle-notch fa-spin'></i>");
        var i = {
            id: t,
            type: "poster"
        };
        $.ajax({
            type: "post",
            data: i,
            url: "/ajax/mylist/add.html",
            success: function (t) {
                200 == t && e.html("<i class='fa fa-heart'></i>"), 202 == t && e.html("<i class='far fa-heart'></i>")
            }
        })
    }), $(".search-btn").click(function () {
        $(".search-form").show()
    }), $(".search-btn-close").click(function () {
        $(".search-form").hide()
    }), $(".btn-fav-channel").click(function () {
        var t = $(this).attr("alt"),
            e = $(this);
        e.html("<i class='fas fa-circle-notch fa-spin'></i>");
        var i = {
            id: t,
            type: "channel"
        };
        $.ajax({
            type: "post",
            data: i,
            url: "/ajax/mylist/add.html",
            success: function (t) {
                200 == t && e.html("<i class='fa fa-heart'></i>"), 202 == t && e.html("<i class='far fa-heart'></i>")
            }
        })
    }), $(".pack").click(function () {
        $(".pack").removeClass("active"), $(this).addClass("active"), e = $(this).attr("alt"), i = $(this).find("span").html(), a = $(this).find("h5").html(), $(this).attr("id"), $("#selected_pack h5").html(a), $("#selected_pack span.price-step-1").html(i), $(".error-plan").hide()
    }), $(".payment").click(function () {
        t = $(this).attr("alt"), $(".payment").removeClass("active"), $(this).addClass("active"), $(".error-method").hide()
    }), $("#go-to-pay").click(function () {
        "none" == t ? $(".error-method").show() : ($(".error-method").hide(), $("#form_payment_step").submit())
    }), $(".file-input").click(function () {
        $(this).prev().click()
    }), $("input[type=file]").change(function () {
        $(this).next().addClass("active")
    }), $("#go-payment-method").click(function () {
        "none" == e ? $(".error-plan").show() : ($(".error-plan").hide(), $("#payment-method").show(), $("#subscribe-plan").hide())
    }), $(".movie-subtitles").click(function () {
        var t = $(this);
        $(this).html("<i class='fas fa-circle-notch fa-spin'></i> Subtitles"), $(".serie-dialog>.login-box .notif-title").html("<span class='fa fa-cc'></span> " + $(this).attr("data-title") + " -  Subtitles");
        var e = $(this).attr("data-id");
        return $.ajax({
            url: "/ajax/movie/subtitles/" + e + ".html",
            success: function (e) {
                $(".box-content").html(e), $(".serie-dialog").show(), t.html("<i class='fa fa-cc'></i> Subtitles")
            }
        }), !1
    }), $(document).on("click", ".movie-downloads", function () {
        var t = $(this);
        $(this).html("<i class='fas fa-circle-notch fa-spin'></i> Download"), $(".serie-dialog>.login-box .notif-title").html("<span class='fa fa-download'></span> " + $(this).attr("data-title") + " -  Download ");
        var e = $(this).attr("data-id");
        return $.ajax({
            data: {
                id: e
            },
            url: "/api/ajax/download-pdf/" + e + "",
            success: function (e) {
                $(".box-content").html(e),
                    $(".serie-dialog").show(),
                    t.html("<i class='fa fa-download'></i> Download")
            }
        }), !1
    }), $(document).on("click", ".episode-subtitles", function () {
        var t = $(this);
        $(this).html("<i class='fas fa-circle-notch fa-spin'></i>"), $(".serie-dialog>.login-box .notif-title").html("<span class='fa fa-cc'></span> " + $(this).attr("data-title") + " - Subtitles");
        var e = $(this).attr("data-id");
        return $.ajax({
            url: "/ajax/serie/subtitles/" + e + ".html",
            success: function (e) {
                $(".box-content").html(e), $(".serie-dialog").show(), t.html("<i class='fa fa-cc'></i>")
            }
        }), !1
    }), $(document).on("click", ".episode-download", function () {
        var t = $(this);
        $(this).html("<i class='fas fa-circle-notch fa-spin'></i>"), $(".serie-dialog>.login-box .notif-title").html("<span class='fa fa-download'></span> " + $(this).attr("data-title") + "  Download");
        var e = $(this).attr("data-id");
        return $.ajax({
            url: "/ajax/serie/downloads/" + e + ".html",
            success: function (e) {
                $(".box-content").html(e), $(".serie-dialog").show(), t.html("<i class='fa fa-download'></i>")
            }
        }), !1
    }), $(".flix-carousel .next_btn").click(function () {
        $(this).nextAll(".flix-scroll-x").animate({
            scrollLeft: $(this).nextAll(".flix-scroll-x").scrollLeft() + $(this).nextAll(".flix-scroll-x").width() - 50
        }, 200)
    }), $(".flix-carousel .prev_btn").click(function () {
        $(this).nextAll(".flix-scroll-x").animate({
            scrollLeft: $(this).nextAll(".flix-scroll-x").scrollLeft() - $(this).nextAll(".flix-scroll-x").width() - 50
        }, 200)
    }), $("#subtitles-btn").click(function () {
        $(".subtitles-table").slideToggle(), $(".downloads-table").show() && $(".downloads-table").hide()
    }), $("#downloads-btn").click(function () {
        $(".downloads-table").slideToggle(), $(".subtitles-table").show() && $(".subtitles-table").hide()
    }), $("#trailer-btn").click(function () {
        "null" == trailer && (trailer = $(".trailer-video").attr("alt")), $(".trailer-video").html("<div class='embed-responsive embed-responsive-16by9'><iframe width='560' height='315' src='https://www.youtube.com/embed/" + trailer + "' frameborder='0' allow='ccelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></div>"), $(".trailer-video").fadeIn()
    }), $(".trailer-video").click(function () {
        $(this).fadeOut(), $(".trailer-video").html("")
    }), $("#share-btn").click(function () {
        $(".share-buttons").slideToggle()
    }), $("#watch-movie-btn").click(function () {
        const slug = window.location.pathname.split('/').pop();
        swal({
            title: "Upgrade to Premium!",
            text: "Premium enables you to watch this video. Thanks!",
            icon: "warning",
            buttons: [
                "Later",
                "Upgrade to Premium"
            ],
            dangerMode: true,
        }).then(function (isConfirm) {
            if (isConfirm) {
                swal("Please wait we checking your data!", {
                    buttons: false,
                    timer: 3000,
                }).then(function () {
                    window.location.href = "/subscription/" + slug + "";
                });
            } else {
                swal("Cancelled", "Your upgrade to premium has been cancelled :)", "error");
            }
        });
    })
});
var rating = 1;

function setRate(t) {
    rating = t
}

function rate(t) {
    const lastSegment = window.location.pathname.split('/').pop();
    var e = $("#rate-input").attr("alt"),
        i = $(".input-review").val(),
        a = {
            id: e,
            rating: rating,
            slug: lastSegment,
            review: i,
            type: t
        };
    $(".submit-review").hide(), $(".loading-review").css("display", "block"), $.ajax({
        type: "POST",
        data: a,
        url: "../api/ajax/rating",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (t) {
            $(".input-review").val(""), $(".submit-review").show(), $(".loading-review").css("display", "none"), $(".success-review").show(), setTimeout(function () {
                $(".success-review").fadeOut()
            }, 2e3)
        },
        fail: function () {
            $(".input-review").val(""), $(".submit-review").show(), $(".loading-review").css("display", "none"), $(".error-review").show(), setTimeout(function () {
                $(".error-review").fadeOut()
            }, 2e3)
        },
        error: function (t) {
            $(".input-review").val(""), $(".submit-review").show(), $(".loading-review").hide(), $(".error-review").show(), setTimeout(function () {
                $(".error-review").fadeOut()
            }, 2e3)
        }
    })
}
$("#star5").click(function () {
    setRate(5)
}), $("#star4").click(function () {
    setRate(4)
}), $("#star3").click(function () {
    setRate(3)
}), $("#star2").click(function () {
    setRate(2)
}), $("#star1").click(function () {
    setRate(1)
}), $(".review-write").click(function (t) {
    t.stopPropagation()
}), $("#darkmode").click(function () {
    $("body").addClass("dark-mode"), $("body").removeClass("light-mode"), $(this).hide(), $("#lightmode").show();
    $.ajax({
        data: {
            theme: "dark"
        },
        type: "post",
        url: "/ajax/theme/",
        success: function (t) { }
    })
}), $("#lightmode").click(function () {
    $("body").addClass("light-mode"), $("body").removeClass("dark-mode"), $(this).hide(), $("#darkmode").show();
    $.ajax({
        data: {
            theme: "light"
        },
        type: "post",
        url: "/ajax/theme/",
        success: function (t) { }
    })
}), $(".open-login").click(function () {
    return $(".login-full").show(), !1
}), $(".notif-close").click(function () {
    $(".login-full").hide(), $(".serie-dialog").hide()
}), $(".submit-review").click(function () {
    rate($(this).attr("type"))
}), $(".submit-comment").click(function () {
    var t = $(".input-comment").val(),
        e = $(".input-comment").attr("alt"),
        i = {
            id: $(this).attr("alt"),
            comment: t,
            type: e
        };
    $(this).hide(), $(".loading-comment").show(), $.ajax({
        type: "post",
        data: i,
        url: "/ajax/comment/add.html",
        success: function (t) {
            var e = jQuery.parseJSON(t);
            $(".input-comment").val(""), $(".comment-list").append("<div class='comment-item'><a title='View profile' href='#'' class='avatar-thumb'><img alt='Mrigank_Gurudatt profile' src=" + e.image + "></a><div class='comment-text'><a>" + e.user + "</a><span class='float-right'><i class='fa fa-clock-o'></i> " + e.created + "</span><p>" + e.content + "</p></div></div>"), $(".comment-list").scrollTop($(".comment-list")[0].scrollHeight), $(".success-comment").show(), setTimeout(function () {
                $(".success-comment").fadeOut()
            }, 2e3), $(".submit-comment").show(), $(".loading-comment").hide()
        },
        fail: function () {
            $(".input-comment").val(""), $(".submit-comment").show(), $(".loading-comment").hide(), $(".error-comment").show(), setTimeout(function () {
                $(".error-comment").fadeOut()
            }, 2e3)
        },
        error: function (t) {
            $(".input-comment").val(""), $(".submit-comment").show(), $(".loading-comment").hide(), $(".error-comment").show(), setTimeout(function () {
                $(".error-comment").fadeOut()
            }, 2e3)
        }
    })
}), $(".season-btn").click(function () {
    var t = $(this).attr("alt");
    const lastSegment = window.location.pathname.split('/').pop();
    $(".season-btn-selected").html($(this).html()),
        $(".serie-episodes-loading").show(),
        $(".serie-episodes").hide(),
        $.ajax({
            type: "POST",
            data: {
                alt: t,
                type: lastSegment
            },
            url: "api/ajax/filter-data",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (t) {
                $(".serie-episodes-loading").hide(),
                    $(".serie-episodes").show(),
                    $(".serie-episodes").html(t)
            }
        })
}), $(function () {
    $(".flix-scroll-x").scroll(function () {
        var t = $(this).outerWidth(),
            e = $(this)[0].scrollWidth,
            i = $(this).scrollLeft();
        e - t === i ? $(this).prevAll(".next_btn").hide() : $(this).prevAll(".next_btn").show(), 0 === i ? $(this).prevAll(".prev_btn").hide() : $(this).prevAll(".prev_btn").show()
    })
}), $("#myCarousel").carousel({
    interval: 3e3
}), $(".carousel .carousel-item").each(function () {
    var t = $(this).next();
    t.length || (t = $(this).siblings(":first")), t.children(":first-child").clone().appendTo($(this));
    for (var e = 0; e < 4; e++)(t = t.next()).length || (t = $(this).siblings(":first")), t.children(":first-child").clone().appendTo($(this))
});

function button_login(params) {
    if (params == 'phone') {
        $('#form-phone').show();
        $('#form-email').hide();
        $('#button-phone').hide();
        $('#button-email').show();
        $('#username').removeAttr('name');
        $('#username_phone').attr('name', 'email');
        $('#username_phone').attr('required', 'required');
        $('#username').removeAttr('required');
    } else {
        $('#button-phone').show();
        $('#button-email').hide();
        $('#form-phone').hide();
        $('#form-email').show();
        $('#username_phone').removeAttr('name');
        $('#username').attr('name', 'email');
    }
};
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function onCheckout() {
    i = $(this).find("span").html()
    const price = $("#selected_pack span.price-step-1").html(i)[0].outerText
    const lastSegment = window.location.pathname.split('/').pop()
    $(".serie-episodes-loading").show(),

        fetch("/api/ajax/trx/payment", {
            method: "POST",
            body: JSON.stringify({
                id: lastSegment,
                price: price
            }),
            headers: {
                "Content-Type": "application/json",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).then(res => res.json()).then(result => {
            const token = result.token;
            $(".serie-episodes-loading").hide(),
                snap.pay(token, {
                    onSuccess: function (payResult) {
                        alert("Transaction Success");
                    },
                    onPending: function (payResult) {
                        swal({
                            title: "Transaction is being processed!",
                            text: "Thank you. We will send you a notification where there is an update. You can track the status at any time by going to Menu > Transaction. Order No : " + payResult.order_id + "",
                            icon: "warning",
                            dangerMode: true,
                        })
                    },
                    onError: function (payResult) {
                        alert("Transaction Error");
                    },
                })
        }).catch(error => {
            alert('Error Get Token')
        });
}

function showModalContent(obj) {
    var orderId = $(obj).data('id');
    $('#modal-content-payment').html('');
    $('#ajaxModel').modal('show');
    $('#ajaxSpinnerDemo').removeClass('hide');

    $('#modal-content-payment').html('');
    $.ajax({
        url: "/api/payment-status-midtrans/" + orderId + "",
        type: 'post',
        data: {
            orderId: orderId
        },
        success: function(response) {
            $('#modal-content-payment').html(response);
            $('#ajaxSpinnerDemo').addClass('hide')
        }
    });
}

$('#password-disabled').attr('disabled', true);
$('#reset-pwd').click(function() {
    $('#password-disabled').val('');
    $('#password-disabled').attr('disabled', false);
});