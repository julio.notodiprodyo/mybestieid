
$(document).ready(function () {
    $("#form-login").on("submit", function (e) {
        //e.preventDefault();
        var form = $(this);
        var field = form.find("input[name=password]");
        var hash = btoa(field.val());
        var hash2 = btoa(hash);
        var slice = hash2.substr(hash2.length - 2);
        hash2 = hash2.slice(0, -2);
        var random_char = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        var slice_random = random_char.substring(0, 14); //only get 14 characters
        var new_str = hash2 + slice_random + slice;
        field.val(new_str);
    });

    if (self === top) {
        var antiClickjack = document.getElementById("antiClickjack");
        antiClickjack.parentNode.removeChild(antiClickjack);
    } else {
        top.location = self.location;
    }
});

$(".toggle-password").click(function () {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});