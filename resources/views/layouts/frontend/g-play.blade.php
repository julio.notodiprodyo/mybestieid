<div class="gplay-box">
    <span><i class="fas fa-times-circle"></i></span>
    <div>
        <img src="{{ asset('decx-logo.png') }}">
        <h3>{{ webConfig()['website_title'] }}</h3>
        <p>{{ webConfig()['website_slogan'] }}</p>
    </div>
    <a href="#" target="_blank" class="btn">
        <i class="fas fa-download"></i> 
        Download
    </a>
</div>
