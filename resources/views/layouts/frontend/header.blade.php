@auth
    @php
    switch(auth()->user()){
        case auth()->user()->hasRole('member'):
            $dashboard = route('self.member.edit');
            break;
        default:
            $dashboard = route('admin.dashboard.index');
            break;
    }
    @endphp
@endauth
<div class="xs-header">
    <button class="btn btn-primary float-left menu-left-btn"><i class="fas fa-bars"></i></button>
    <img src="{{ webConfig()['website_logo_header'] }}">
    @auth
    <div class="btn-group  float-right " >
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="for-desktop"> {{ auth()->user()->name }} </span> 
            <img src="{{ !empty(auth()->user()->image) ? route('home').'/' . auth()->user()->image : asset('images/nophoto.jpg') }}">
        </button></button>
        <div class="dropdown-menu dropdown-login">
            <a class="dropdown-item" href="{{ $dashboard }}"><i class="fas fa-user"></i> Dashboard Account</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href=""onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i> Log out
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </a>
        </div>
    </div>
    @else 
    <button class="btn btn-primary float-right open-login"><i class="fas fas fa-user"></i></button>
    @endauth
    <button class="btn btn-primary float-right search-btn"><i class="fas fa-search"></i></button>
</div>
<div class="row top-nav">
    <div class="col-sm-3 col-md-3 ">
        <div class="tab-title">
            {{ tabTitle(request()->segment(1)) }}
        </div>
    </div>
    <div class="col-sm-6 col-md-6 search-form">
        <form action="{{ route('page.search') }}" method="GET" autocomplete="off" class="navbar-form" role="search">
            <div class="search-group">
                <i class="fas fa-search"></i>
                <input name="key" type="text" minlength="2" class="form-control" placeholder="Video Music, Tv Channels , Vod ...">
                <button class="btn btn-default" type="submit">Search</button>
            </div>
        </form>
        <button class="btn btn-primary float-right for-phone search-btn-close"><i class="fas fa-times"></i></button>
    </div>
    <div class="col-sm-3 col-md-3 account-action for-desktop">
        <div class="float-right">
            @auth    
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="for-desktop"> {{ auth()->user()->name }} </span> 
                    <img src="{{ !empty(auth()->user()->image) ? route('home').'/' . auth()->user()->image : asset('images/nophoto.jpg') }}" class="img-responsive rounded">
                </button></button>
                <div class="dropdown-menu dropdown-login">
                    <a class="dropdown-item" href="{{ $dashboard }}"><i class="fas fa-user"></i> Dashboard Account</a>
                    <div class="dropdown-divider"></div>
                    <a href=""onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>              
            @else
                <a href="{{ route('index.member.registration') }}" class="btn btn-link"> Sign-up </a>
                <a href="#" class="btn btn-primary open-login">Sign-in </a>  
            @endauth
        </div>
    </div>
</div>
