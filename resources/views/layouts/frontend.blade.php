<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />

    <title>{{ webConfig()['website_title'] }}</title>

    @foreach (metaData(request()->segment(2)) as $meta)
    {!! $meta !!}
    @endforeach

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('frontend/css/player.css') }}" />
        {{-- <link rel="stylesheet" href="https://cdn.plyr.io/1.8.2/plyr.css"> --}}
    <link rel="stylesheet" href="https://cdn.plyr.io/3.6.1/plyr.css" />

    <link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
    <script src="https://unpkg.com/video.js/dist/video.js"></script>

    <script src="https://kit.fontawesome.com/017eaf7e3f.js" crossorigin="anonymous"></script>
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    @stack('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet"
        href="{{ asset('frontend/css/sweetalert.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />

    <style id="antiClickjack">
        body {
            display: none !important;
        }

    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-REM0HWRPBL"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-REM0HWRPBL');
    </script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8358126254142358"
     crossorigin="anonymous"></script>
</head>
<body class="dark-mode">
    @if (session('success_login'))
    <div class="alert  alert-home">
        <i class="fas fa-bell"></i>
        <button type="button" aria-hidden="true" class="close">
            <i class="fas fa-times"></i>
        </button>
        <span data-notify="message">{{ session('success_login') }}</span>
    </div>
    @endif
    @if (request()->segment(1) == 'player')
        <div class="flix_app_player" style="overflow: hidden;">
            @yield('player')
        </div>
        <footer>
            <a href="{{ route('home').'/'.request()->segment(2).'/'.request()->segment(3) }}">
                <i class="fas fa-arrow-left"></i>
            </a>
            <img src="{{ route('home').''.webConfig()['website_logo_header'] }}">
        </footer>
    @elseif (request()->segment(1) == 'contact')
        @yield('contact')
    @elseif (request()->segment(1) == 'signup' || request()->segment(1) == 'verify' || request()->segment(1) == 'login' || request()->segment(1) == 'password')
        <div class="login-page-bg"></div>
        <div class="login-page-bg-blur"></div>
        <div class="login-page-content">
            <a id="lightmode" href="#"><i class="fas fa-sun"></i></a>
            <a id="darkmode" href="#" style="display:none"><i class="fas fa-moon"></i> </a>
            <img src="{{ webConfig()['website_logo_header'] }}">

            @yield('authentication')
        </div>
    @else
        <div class="container-fluid">
            <div class="row main-row">
                <div class="sidebar">
                    @include('layouts.frontend.sidebar')
                </div>
                <div class="close-menu-left-btn"></div>
                <div class="main">
                    @include('layouts.frontend.header')
                    <div class="content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        {{-- @include('layouts.frontend.g-play') --}}
        <div class="background-close login-full">
            <div class="login-box">
                @include('frontend.member-area.auth.login')
            </div>
        </div>
        </div>
    @endif

    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="//www.gstatic.com/cv/js/sender/v1/cast_sender.js?loadCastFramework=1"></script>
    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <script src="{{ asset('frontend/js/custom.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('frontend/js/sweetalert.min.js') }}"></script>
    @include('sweet::alert')

    <script src="https://cdn.plyr.io/3.6.1/plyr.polyfilled.js"></script>

    @stack('scripts')

    <script src="{{ asset('frontend/js/app.js') }}"></script>
</body>

</html>
