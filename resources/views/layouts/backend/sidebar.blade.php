<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="logo">                
                    <img alt="Decx-TV" width="100%" src="{{ webConfig()['website_logo_header'] }}">
                </div>
            </li>
            <!-- ++++++++++++++  ROLE ADMINISTRATOR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            @if(auth()->user()->hasRole('admin'))
            <li class="{{ Request::is('admin/dashboard*') ? 'active' : '' }}">
                <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ Request::is('admin/page*') ? 'active' : '' }}">
                <a href="{{route('page.index')}}"><i class="fa fa-file {{{ (Request::is('page') ? 'active' : '') }}}"></i> <span class="nav-label">Pages</span></a>
            </li>
            <li class="{{ Request::is('admin/posts*') ? 'active' : '' }}">
                <a href="{{route('admin.posts.index')}}"><i class="fa fa-play {{{ (Request::is('posts') ? 'active' : '') }}}"></i> <span class="nav-label">Posts</span></a>
            </li>
            <li class="{{ Request::is('admin/channel*') ? 'active' : '' }}">
                <a href="{{route('channel.index')}}"><i class="fa fa-adjust {{{ (Request::is('channel') ? 'active' : '') }}}"></i> <span class="nav-label">Channel</span></a>
            </li>
            <li class="{{ Request::is('admin/slideshow*') ? 'active' : '' }}">
                <a href="{{route('slideshow.index')}}"><i class="fa fa-file-image-o {{{ (Request::is('slideshow') ? 'active' : '') }}}"></i> <span class="nav-label">Slideshow</span></a>
            </li>
            <li class="{{ Request::is('admin/ereference*') ? 'active' : '' }}">
                <a href="{{route('ereference.index')}}"><i class="fa fa-file-pdf-o {{{ (Request::is('ereference') ? 'active' : '') }}}"></i> <span class="nav-label">E-Reference</span></a>
            </li>
            <li class="{{ Request::is('admin/ads*', 'admin/category*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('admin/ads*') ? 'active' : '' }}">
                        <a href="{{route('ads.index')}}"><i class="fa fa-buysellads {{{ (Request::is('ads') ? 'active' : '') }}}"></i> <span class="nav-label">Ads</span></a>
                    </li>
                    <li class="{{ Request::is('admin/category*') ? 'active' : '' }}">
                        <a href="{{route('category.index')}}"><i class="fa fa-language {{{ (Request::is('category') ? 'active' : '') }}}"></i> <span class="nav-label">Category Channel</span></a>
                    </li>       
                </ul>
            </li>
            <li class="{{ Request::is('admin/menu*', 'admin/user*', 'admin/role*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">System Admin</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('admin/user*') ? 'active' : '' }}">
                        <a href="{{route('user.index')}}"><i class="fa fa-user-plus"></i> User </a>
                    </li>
                    <li class="{{ Request::is('admin/role*') ? 'active' : '' }}">
                        <a href="{{route('role.index')}}"><i class="fa fa-users"></i> Role </a>
                    </li>
                    <li class="{{ Request::is('admin/menu*') ? 'active' : '' }}">
                        <a href="{{route('menu.index')}}"><i class="fa fa-navicon"></i> Menu </a>
                    </li>
                    <li class="{{ Request::is('admin/account_management*') ? 'active' : '' }}">
                        <a href="{{route('account_management.index')}}"><i class="fa fa-navicon"></i> Akun </a>
                    </li>
                    <li class="{{ Request::is('admin/config*') ? 'active' : '' }}">
                        <a href="{{route('config.index')}}"><i class="fa fa-gears {{{ (Request::is('config') ? 'active' : '') }}}"></i> <span class="nav-label">Config</span></a>
                    </li> 
                </ul>
            </li>
            
             <!-- ++++++++++++++  ROLE FINANCE  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @elseif(auth()->user()->hasRole('finance'))
            <li class="{{ Request::is('admin/dashboard*') ? 'active' : '' }}">
                <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ Request::is('admin/posts*') ? 'active' : '' }}">
                <a href="{{route('admin.posts.index')}}"><i class="fa fa-play {{{ (Request::is('posts') ? 'active' : '') }}}"></i> <span class="nav-label">Posts</span></a>
            </li>
            <li class="{{ Request::is('admin/channel*') ? 'active' : '' }}">
                <a href="{{route('channel.index')}}"><i class="fa fa-adjust {{{ (Request::is('channel') ? 'active' : '') }}}"></i> <span class="nav-label">Channel</span></a>
            </li>
            <li class="{{ Request::is('admin/slideshow*') ? 'active' : '' }}">
                <a href="{{route('slideshow.index')}}"><i class="fa fa-file-image-o {{{ (Request::is('slideshow') ? 'active' : '') }}}"></i> <span class="nav-label">Slideshow</span></a>
            </li>
            <li class="{{ Request::is('admin/ereference*') ? 'active' : '' }}">
                <a href="{{route('ereference.index')}}"><i class="fa fa-file-pdf-o {{{ (Request::is('ereference') ? 'active' : '') }}}"></i> <span class="nav-label">E-Reference</span></a>
            </li>
            <li class="{{ Request::is('admin/ads*', 'admin/category*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('admin/ads*') ? 'active' : '' }}">
                        <a href="{{route('ads.index')}}"><i class="fa fa-buysellads {{{ (Request::is('ads') ? 'active' : '') }}}"></i> <span class="nav-label">Ads</span></a>
                    </li>
                    <li class="{{ Request::is('admin/category*') ? 'active' : '' }}">
                        <a href="{{route('category.index')}}"><i class="fa fa-language {{{ (Request::is('category') ? 'active' : '') }}}"></i> <span class="nav-label">Category Channel</span></a>
                    </li>       
                </ul>
            </li>
            <!-- ++++++++++++++  END ROLE PARTNER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @elseif(auth()->user()->hasRole('partner'))
            <li class="{{ Request::is('admin/dashboard*') ? 'active' : '' }}">
                <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ Request::is('admin/partner*') ? 'active' : '' }}">
                <a href="{{route('admin.partner.index')}}"><i class="fa fa-file-excel-o {{{ (Request::is('partner') ? 'active' : '') }}}"></i> <span class="nav-label">Report</span></a>
            </li>
            @endif
        </ul>
    </div>
</nav>