<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style type="text/css">
    .dropdown-submenu {
        position: relative;
    }

    .dropdown-submenu .dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -1px;
    }

    .menu-collapse>ul>li {
        border-radius: 3px;
        color: inherit;
        line-height: 25px;
        margin: 4px;
        font-weight: normal;
        margin-left: -40px;
    }

    .menu-collapse>ul>li:hover {
        font-weight: bold;
    }

    .menu-collapse>ul>li>a {
        font-style: none;
        /*border: #000 solid 1px;*/
        text-decoration: none;
        color: #777;
    }

    .img-wrap {
        float: left;
        width: 20%;
        margin-right: 2%;
    }

    .user-name {
        float: right;
        width: 75%;
    }

    .user-name p {
        margin-top: 0;
        line-height: 20px;
    }

    .user-name p {
        margin: 0;
        padding: 0;
    }

    .user_group ul>li {
        padding: 10px !important;
        cursor: pointer;
    }

    .user_group ul>li>a {
        padding: 0 !important;
    }

    .user_group ul li a .small {
        font-size: 10px !important;
    }

    .user_group ul {}

    .user_group ul>li {
        padding-top: 10px;
        padding-bottom: 10px;
        border: none;
        border-radius: 3px;
        transition: 1s;
        overflow: hidden;
    }

    .user_group ul>li:hover {
        background-color: #f0f0f0;
        transition: 0.5s;
    }

    .addaccount {
        background-color: #F6F6F6;
    }

</style>
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" style="margin:30px !important" href="#"><i
                    class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="" class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            {{-- <li class="dropdown">
            <a class="dropdown-toggle" id="notifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <span class="glyphicon glyphicon-globe"></span> Notification <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="notificationsMenu" id="notificationsMenu">
                <li class="dropdown-header">No notifications</li>
            </ul>
        </li> --}}
            <li class="dropdown">
                <a class="dropdown-toggle" id="notifications" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="true">
                    <span>
                        @if (!empty(auth()->user()->image))
                            <img width="48" height="48" alt="image" class="img-circle"
                                src="{{ asset(auth()->user()->image) }}">
                        @else
                            <img width="48" height="48" alt="image" class="img-circle"
                                src="{{ asset('img/default_avatar_male.jpg') }}">
                        @endif
                    </span>
                    <strong class="font-bold">{{ auth()->user()->name }}</strong></span> <b
                        class="caret"></b></span>
                </a>
                <ul class="dropdown-menu" style="width: 250px;">
                    <li class="dropdown-header">
                        <a href="http://{{ Request::server('HTTP_HOST') }}" target="_blank">
                            <i class="fa fa-globe"></i> View Website
                        </a>
                    </li>

                    <li class="dropdown-header">
                        <a href="{{ route('admin.personal.index')}}">
                            <i class="fa fa-user"></i> Personal Setting
                        </a>
                    </li>

                    @php
                        $authGroup = Session::get('user_group');
                        $authMaster = Session::get('group_master');
                        $authRole = Session::get('user_role');
                        $menuRole = Session::get('menu_role');
                    @endphp

                    @if (!empty($menuRole[0]->name) && !empty($authRole))
                        <li role="role" class="dropdown-header have-child menu-collapse"> <a role="menuitem"
                                tabindex="-1" data-toggle="collapse" data-parent="#accordion" href="#role"><i
                                    class="fa fa-cog"></i> Switch Role <span class="caret pull-right"
                                    style="margin-top:10px;"></span>
                            </a>
                            <ul id="role" class="panel-collapse collapse ">
                                @php
                                    $nowRole = auth()
                                        ->user()
                                        ->roles()
                                        ->where('name', request()->segment(1))
                                        ->first();
                                    //dump($nowRole);
                                @endphp

                                <li style="text-align:left !important;padding-left:15px;"><a
                                        href="{{ url('change_auth') }}/{{ $nowRole->id }}"><b> Login As
                                            {{ strtoupper($nowRole->name) }} </b></a></li>

                                @foreach ($authRole as $role)
                                    @if ($nowRole->id != $role->id)
                                        <li style="text-align:left !important;padding-left:15px;"><a
                                                href="{{ url('change_auth') }}/{{ $role->id }}"><i
                                                    class="fa fa-angle-double-right"></i> {{ strtoupper($role->name) }}
                                            </a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @endif

                    <li class="dropdown-header">
                        <a href="" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>

    </nav>
</div>

<script>
    $(document).ready(function() {
        $('.dropdown-submenu a.test').on("click", function(e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
        $('a[data-toggle="collapse"]').click(function(event) {
            event.stopPropagation();
            event.preventDefault();
            var drop = $(this).closest(".dropdown");
            $(drop).addClass("open");
            $('.user_group .collapse.in').collapse('hide');
            var col_id = $(this).attr("href");
            $(col_id).collapse('toggle');
        });

        // $('#user_group').on('shown.bs.collapse', function() {
        //     $(this).parent().find(".glyphicon-chevron-left").removeClass("glyphicon-chevron-left").addClass("glyphicon-chevron-left");
        // }).on('hidden.bs.collapse', function() {
        //     $(this).parent().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-down");
        // });
        $('#user_groups').on('collapse', function() {
            $(".user_group .glyphicon").addClass('glyphicon-chevron-left').removeClass(
                'glyphicon-chevron-down');
        });

        $('#user_groups').on('collapse.in', function() {
            $(".user_group .glyphicon").addClass('glyphicon-chevron-down').removeClass(
                'glyphicon-chevron-left');
        });
    });

    // $(document).ready(function () {
    //     $('ul[role="navigation"]')
    //         .on('show.bs.collapse', function (e) {
    //         $(e.target).prev('a[role="menuitem"]').addClass('active');
    //     })
    //         .on('hide.bs.collapse', function (e) {
    //         $(e.target).prev('a[role="menuitem"]').removeClass('active');
    //     });

    //     $('a[data-toggle="collapse"]').click(function (event) {

    //         event.stopPropagation();
    //         event.preventDefault();

    //         var drop = $(this).closest(".dropdown");
    //         $(drop).addClass("open");

    //         $('.collapse.in').collapse('hide');
    //         var col_id = $(this).attr("href");
    //         $(col_id).collapse('toggle');

    //     });
    // });
</script>
