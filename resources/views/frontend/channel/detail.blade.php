@extends('layouts.frontend')

@section('content')
<div class="row content-section">
    <div class="col-md-12">
        <div class="movie-header-bg"
            style=" background-image: url({{ $image }});">
            <div class="movie-header">
                <div class="movie-title">{{ $data['title'] }}</div>
                <div class="movie-infos"> 
                    {{ $tags }}
                </div>
                <div class="header-buttons">
                    <button id="share-btn">
                        <i class="fa fa-share "></i>SHARE
                        <div class="share-buttons">
                            <a href="http://www.facebook.com/sharer.php?u={{ route('channel.slug', $data['slug']) }}"
                                target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ route('channel.slug', $data['slug']) }}"
                                target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="https://twitter.com/share?url={{Request::fullUrl()}}&amp;text={{ $data->title }}"
                                target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </button>
                </div>
                <div class="header-ratings">
                    <a href="{{ route('channel') }}"> <img class="country-logo" src="{{ asset('frontend/images/indonesia.png') }}">
                    </a> • {{ $rate['total'] }}/5
                    <div class="rating-box">
                        <div class="rating" style="width:{{ $rate['percentage'] }}%;"></div>
                    </div>
                </div>
            </div>
            <img class="channel-logo" src="{{ $image }}">
        </div>
    </div>
    <div class="col-md-12">
        <div class="dropdown">
            @if ($data['parent']['slug'] == 'radio-streaming')
            <div class="pull-right">
                @if ($data['post_type'] == 'm3u8')
                <audio preload="true" id="player" controls crossorigin autoplay>
                @push('scripts')
                <script src="https://cdn.jsdelivr.net/hls.js/latest/hls.js"></script>
                <script type="text/javascript">
                    (function () {
                    var audio = document.querySelector('#player');

                    if (Hls.isSupported()) {
                        var hls = new Hls();
                        hls.loadSource("{{ $data['m3u8'] }}");
                        hls.attachMedia(audio);
                    }
                    
                    plyr.setup(audio);
                    })();
                </script>
                @endpush
                @else
                <audio preload="true" controls crossorigin autoplay>
                    <source src="{{ $data['mpeg'] }}" type="audio/mpeg">
                    Your browser does not support the audio element.
                </audio> 
                @endif
            </div>
            @else    
            <a href="{{ route('player.hsl', ['slug' => 'channel', 'hsl' => $data->slug]) }}" class="btn play-btn">
                <span class="fa fa-play"></span> 
                Play Channel
            </a>
            @endif
            @if (!empty($data->website_url))   
            <a href="{{ $data->website_url }}" target="_blank" id="trailer-btn" class="btn play-btn">
                <i class="fas fa-globe-americas"></i> 
                Visit website
            </a>
            @endif
            <a class="btn play-btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span class="fa fa-star"></span> Rate Channel
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <div class="comment-item  review-write">
                    @include('frontend.component.review-rate')
                </div>
            </div>
        </div>
    </div>
    @if (!empty($data['content']))    
    <div class="col-md-12 padding-top-20">
        <div class="movie-section-title">Overview</div>
    </div>
    <div class="col-md-12">
        <p>{!! $data['content'] !!}</p>
    </div>
    @endif
    @if (!empty($newOtherRecomendation))
    @include('frontend.component.recomendation')
    @endif
    <div class="col-md-6 padding-top-20">
        @include('frontend.component.comment')
        @guest
        <div class="comment-item comment-write">
            <a title="View profile" href="#" class="avatar-thumb open-login">
                <img src="{{  asset('images/nophoto.jpg') }}"></a>
            <div class="comment-text">
                <textarea class="input-comment open-login" alt="channel" placeholder="Write you comment here"></textarea>
                <button alt="40" class="btn btn-primary float-right open-login">Submite Comment <i class="fas fa-paper-plane" aria-hidden="true"></i> </button>
            </div>
        </div>
        @endguest
    </div>
    <div class="col-md-6 padding-top-20">
        @include('frontend.component.review')
    </div>
</div>
@endsection
