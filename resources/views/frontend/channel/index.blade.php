@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="filter-section">
            <div class="btn-group season-dropdown float-right" role="group" aria-label="Basic example">
                @include('frontend.component.button-filter')
            </div>
            <div class="btn-group season-dropdown" role="group">
                <div class="btn-group float-right season-dropdown">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        All Categories
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        @foreach ($category as $key => $item)
                            <button class="dropdown-item season-btn" alt="{{ $item['id'] }}"
                                type="button">{{ $item['title'] }}</button>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row content-section serie-episodes">
        @foreach ($channel as $item)
            <a class="channel col-4 col-md-2 col-xxl-2" href="{{ route('channel.slug', $item['slug']) }}">
                <img
                    src="{{ !empty($item['image']) ? $item['image'] : asset('frontend/dummy_150x75.png') }}" style="width:150px; height:75px"/>
                @if ($item['is_hd'] == 1)
                    <div>
                        <span>HD</span>
                        <p class="two">LIVE</p>
                    </div>
                @endif
            </a>
        @endforeach
    </div>
    <div class="serie-episodes-loading">
        <i class="fas fa-circle-notch fa-spin"></i>
    </div>
    {{-- <div class="row content-section">
        <div class="filter-section">
            <div class=" float-right">

                {{ $paginator->appends($channel)->render('vendor.pagination.semantic-ui') }}

            </div>
        </div>
    </div> --}}
@endsection
