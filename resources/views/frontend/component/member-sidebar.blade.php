<a href="{{ route('self.member.edit') }}" class="btn btn-block btn-sm play-btn"><i class="fas fa-user-edit"></i> My
    Profile</a>
<a href="{{ route('list.transaction') }}" class="btn btn-block btn-sm play-btn"><i class="fas fa-star"></i> Transaction</a>
<a class="btn btn-block btn-sm play-btn" href=""onclick="event.preventDefault();document.getElementById('logout-form').submit();">
    <i class="fas fa-sign-out-alt"></i> Log out
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</a>