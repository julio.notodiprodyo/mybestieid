@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="warning-content">
            <p>
                The page is under construction. Please forgive the inconvenience. <br />
                We will be celebrating the launch of our new site very soon!
            </p>
            <p>
                It's okay, we're excited too!
            </p>
        
        </div>
    </div>
@endsection
