<div class="movie-section-title"><i class="fa fa-comment"></i> {{ $comments->count() }} Comments</div>
<div class="comment-section comment-list">
    @foreach ($comments as $comment)
        <div class="comment-item">
            <a title="View profile" href="#" class="avatar-thumb"><img
                    src="{{ $comment['hasUser']['image'] ? asset($comment['hasUser']['image']) : asset('images/nophoto.jpg') }}"></a>
            <div class="comment-text">
                <a>{{$comment['hasUser']['name']}}</a><span class="float-right"><i class="fa fa-clock-o"></i> {{$comment->created_at->diffForHumans()}} </span>
                <p> {{$comment['comment']}}</p>
            </div>
        </div>
    @endforeach
</div>
<!-- <div class='alert alert-success success-comment' role='alert'>
    <i class='fas fa-comment-alt'></i> Your comment has been added successfully!
</div>
<div class='alert alert-danger error-comment' role='alert'>
    <i class='fas fa-comment-alt'></i> Your comment could not be submitted
</div> -->
@if(auth()->check())
    <div class="comment-item comment-write">
        <a title="View profile" href="#" class="avatar-thumb">
            <img src="{{ auth()->user()->image ? asset(auth()->user()->image) : asset('images/nophoto.jpg') }}">
        </a>
        <div class="comment-text">
            <a>{{ auth()->user()->name }}</a>
            <textarea class="input-comment" alt="poster" placeholder="Write you comment here"></textarea>
            <a alt="3" class="btn btn-primary float-right submit-comment">
                <i class="fas fa-paper-plane"></i> 
                Submit Comment
            </a>
            <button alt="3" class="btn btn-primary float-right loading-comment">
                <i class="fas fa-circle-notch fa-spin"></i> 
                Comment Interting
            </button>
        </div>
    </div>
@endif
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
  function submitComment(comment) {
        $.ajax({
            url: "{{ route('comments.send') }}",
            method: "POST",
            data: {
                comment: comment,
                content_id: "{{ $data->id }}",
                user_id: "{{ auth()->check() ? auth()->user()->id : '' }}",
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function(response) {
                // var asset = window.location.origin+'/';
                $(".comment-list").append( response
                    // '<div class="comment-item">'+
                    //     '<a title="View profile" href="#" class="avatar-thumb"><img src="'+ asset+response['has_user']['image'] +'"></a>'+ 
                    //     '<div class="comment-text">'+
                    //     '<a>'+response['has_user']['name']+'</a><span class="float-right"><i class="fa fa-clock-o">'+response['created_at']+'</i></span>'+
                    //     '<p>'+response['comment']+'</p>'+
                    //     '</div>'+
                    // '</div>'
                )
            },
            error: function(response) {
                console.log('error', response);
            }
        });
    }
    $(document).on('click', '.submit-comment', function(e) {
        var comment = $('.input-comment').val();
        submitComment(comment);
    });

</script>
