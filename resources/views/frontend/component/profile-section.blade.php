<div class="col-md-12">
    <div class="actor-header-bg" style=" background-image: url('../frontend/background.jpg');">
        <div class="movie-header actor-header">
            <div class="movie-title">{{ Auth::user()->name }}</div>
        </div>
    </div>
    <img class="actor-logo" src="{{ asset('frontend/web_profileIcon.svg') }}">
</div>
<div class="col-md-3">
    @include('frontend.component.member-sidebar')
</div>