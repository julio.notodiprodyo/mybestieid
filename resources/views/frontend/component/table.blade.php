@push('css')
    @include('layouts.backend.datatables_css')
@endpush

{!! $dataTable->table(['width' => '100%']) !!}

@push('scripts')
    @include('layouts.backend.datatables_js')
    {!! $dataTable->scripts() !!}
    @include('layouts.backend.datatables_limit')
@endpush
