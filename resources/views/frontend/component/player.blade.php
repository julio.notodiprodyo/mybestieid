@extends('layouts.frontend')

@section('player')
    @if ($data['type'] == 'channel')
        @if (!empty($contentAds))
            <!-- Add some extra attribute as video-url and mimetype which we can later play once we are done playing ads  -->
            <video id="video_1" class="video-js playads video-js vjs-fluid vjs-default-skin"
                video-url="{{ $data['hls_url'] }}"
                mimetype="application/x-mpegURL" playsinline controls controlsList="nodownload" preload="auto" data-setup='{ "controls": true, "autoplay": true, "preload": "auto" }' allow="autoplay">
                <!-- ad source ad ad video url   -->
                <source src="{{ route('home') . '/' . $ads->video }}" type='video/mp4' />
            </video>
            <!-- text to be displayed while playing ad   -->
            <div class="moveToVideoJs">
                <div class="advertisement hide">Advertisement</div>
            </div>
        @else
            <video id="my_video_1" class="video-js vjs-fluid vjs-default-skin" playsinline controls controlsList="nodownload"
                preload="auto" data-setup='{}' allow="autoplay">
                <source src="{{ $data['hls_url'] }}" type="application/x-mpegURL">
            </video>
        @endif

        @push('scripts')
        <script type="text/javascript">
            $(document).ready(function() {
                var videotag = $('.playads');
                var myPlayer = videojs('video_1', { "controls": true, "autoplay": true, "preload": "auto" });
                myPlayer.play();
                $(".moveToVideoJs").appendTo($('#video_1'));
                // show advertisement label while play advertisement
                myPlayer.on('play', function() {
                    if (myPlayer.hasClass("playads")) {
                        $('.advertisement').removeClass('hide');
                    }
                });
                // Stop from seeking while playing advertisement
                myPlayer.on("seeking", function(event) {
                    if (currentTime < myPlayer.currentTime() && myPlayer.hasClass("playads")) {
                        myPlayer.currentTime(currentTime);
                    }
                });
                myPlayer.on("seeked", function(event) {
                    if (currentTime < myPlayer.currentTime() && myPlayer.hasClass("playads")) {
                        myPlayer.currentTime(currentTime);
                    }
                });
                setInterval(function() {
                    if (!myPlayer.paused() && myPlayer.hasClass("playads")) {
                        currentTime = myPlayer.currentTime();
                    }
                }, 1000);

                // Hide Advertisement label and change data-src of player to play actual video
                myPlayer.on('ended', function() {
                    if (this.hasClass("playads")) {
                        this.src({
                            type: videotag.attr('mimetype'),
                            src: videotag.attr('video-url')
                        });
                        this.play();
                        this.removeClass('playads');
                        $('.advertisement').addClass('hide');
                    }
                });
            });

            var player_2 = videojs('my_video_1');
            player_2.play();
            var vid = document.getElementById("my_video_1");
            vid.muted = true;
        </script>
        @endpush
    @else
        @if (!empty($data['hls_url']))
            <video  id="player" playsinline controls>
                <source src="{{ $data['hls_url'] }}" type="video/mp4"/>
            </video>
        @else  
        <div class="plyr__video-embed " id="player">
            <iframe
                src="https://www.youtube.com/embed/{{ $data['youtube'] }}?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1"
                allowfullscreen allowtransparency></iframe>
        </div>
        @endif
        
        @push('scripts')
        <script type="text/javascript">
            const player = new Plyr('#player',{
                autoplay: true,
                title: 'Example Title',
                controls:['play-large', 'play', 'progress', 'current-time', 'volume', 'captions', 'settings', 'pip', 'airplay', 'fullscreen']
            });

            window['__onGCastApiAvailable'] = function(isAvailable) {
                if (isAvailable) {
                    initializeCastApi();
                }
            };

            initializeCastApi = function() {
                cast.framework.CastContext.getInstance().setOptions({
                    receiverApplicationId: applicationId,
                    autoJoinPolicy: chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED
                });
            };
        </script>
        @endpush
    @endif
@endsection
