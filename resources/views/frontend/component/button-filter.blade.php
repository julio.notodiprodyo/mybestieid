<button class="btn btn-secondary season-btn" alt="newest" type="button">
    <i class="far fa-clock"></i>
    <span class="for-desktop">Newest</span></button>
<button class="btn btn-secondary season-btn" alt="views" type="button">
    <i class="far fa-eye"></i>
    <span class="for-desktop">Views</span></button>
<button class="btn btn-secondary season-btn" alt="rating" type="button">
    <i class="fas fa-star-half-alt"></i>
    <span class="for-desktop">Rating</span></button>
<button class="btn btn-secondary season-btn" alt="title" type="button">
    <i class="fas fa-sort-alpha-down"></i>
    <span class="for-desktop">Title</span></button>
