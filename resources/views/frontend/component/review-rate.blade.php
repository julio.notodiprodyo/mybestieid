<div class='alert alert-success success-review' role='alert'>
    <i class='fas fa-comment-alt'></i> Your Review has been added successfully!
</div>
<div class='alert alert-danger error-review' role='alert'>
    <i class='fas fa-comment-alt'></i> Your Review could not be submitted
</div>
<div class="rate" id="rate-input" alt="38">
    <input type="radio" id="star5" name="rate" value="5" />
    <label for="star5" title="text">5 stars</label>
    <input type="radio" id="star4" name="rate" value="4" />
    <label for="star4" title="text">4 stars</label>
    <input type="radio" id="star3" name="rate" value="3" />
    <label for="star3" title="text">3 stars</label>
    <input type="radio" id="star2" name="rate" value="2" />
    <label for="star2" title="text">2 stars</label>
    <input type="radio" checked="true" id="star1" name="rate" value="1" />
    <label for="star1" title="text">1 star</label>
</div>
<textarea class="input-review" placeholder="Write you review here"></textarea>
@auth
<button alt="40" class="btn btn-primary  submit-review btn-block">Submite Review <i class="fas fa-paper-plane"></i> </button>
<button alt="40" class="btn btn-primary  loading-review btn-block">Review Interting <i class="fas fa-circle-notch fa-spin"></i> </button>
@else  
<button class="btn btn-primary btn-block open-login"> <i class="fas fa-check"></i> Login to leave a review </button>
@endauth