<div class="col-md-12 padding-top-20">
        <div class="movie-section-title">For You</div>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            @foreach ($newOtherRecomendation as $recomendation)    
            <a class="channel" href="{{ route(''.request()->segment(1).'.slug', $recomendation['slug']) }}">
                <img style="width:150px; height:75px" src="{{ $recomendation['image'] }}" />
            </a>
            @endforeach
        </div>
    </div>