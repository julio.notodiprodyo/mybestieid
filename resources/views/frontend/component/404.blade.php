@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="empty-box">
            <img src="{{ asset('frontend/404.png') }}" style="max-inline-size: 100%;block-size: auto;">
            <h3>Oh no! Page not found.</h3>
            <p>The link you followed may be broken, or the page may have been removed.</p>
        </div>
    </div>
@endsection
