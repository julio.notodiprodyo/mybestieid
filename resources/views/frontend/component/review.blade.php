<div class="movie-section-title"><i class="fa fa-star"></i> {{ $rate['rate']->count() }} Reviews</div>
<div class="comment-section ">
    @foreach ($rate['rate'] as $item)    
    <div class="review">
        <div class="review-properties">
            Reviewed by
            <span class="review-author">{{ $item['users']['name'] }}</span>
            <span class="fa fa-star checked"></span>
            <span class="review-rating">{{ $item['rating'] }} / 5</span>
            <span class="float-right"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($item['created_at'])->diffForHumans() }} </span>
        </div>
        <article>
            <p></p>
        </article>
    </div>
    @endforeach
</div>