@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="filter-section">
            <div class="btn-group season-dropdown float-right" role="group" aria-label="Basic example">
                @include('frontend.component.button-filter')
            </div>
        </div>
    </div>
    <div class="row content-section serie-episodes">
        @foreach ($podcast as $item)
            <a class="channel col-4 col-md-2 col-xxl-2" href="{{ route('podcast.slug', $item['slug']) }}">
                <img height="75" width="150"
                    src="{{ !empty($item['image']) ? $item['image'] : asset('frontend/dummy_150x75.png') }}" />
            </a>
        @endforeach
    </div>
    <div class="serie-episodes-loading">
        <i class="fas fa-circle-notch fa-spin"></i>
    </div>
    {{-- <div class="row content-section">
        <div class="filter-section">
            <div class=" float-right">

                {{ $paginator->appends($channel)->render('vendor.pagination.semantic-ui') }}

            </div>
        </div>
    </div> --}}
@endsection
