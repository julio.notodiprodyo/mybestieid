@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="col-sm-12 col-md-12 ">
            {!! $data['content'] !!}
        </div>
    </div>
@endsection

