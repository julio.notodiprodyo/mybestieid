@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="col-md-12">
            <div class="actor-header-bg"
                style=" background-image: url({{ !empty($data['image']) ? $data['image'] : asset('frontend/dummy_170x255.png') }});">
                <div class="movie-header actor-header">
                    <div class="movie-title">{{ $data['title'] }}</div>
                </div>
                <img class="actor-logo"
                    src="{{ !empty($data['image']) ? $data['image'] : asset('frontend/dummy_170x255.png') }}">
            </div>
        </div>
        <div class="col-md-12">
            <div class="dropdown">
                <button id="downloads-btn" class="btn play-btn movie-downloads pull-right" data-id={{ $data['id'] }} data-title="{{ $data['title'] }}"><span
                        class="fas fa-download"></span> Download</button>
            </div>
        </div>
        @if (!empty($data['content']))
            <div class="col-md-12 padding-top-20">
                <div class="movie-section-title">Bio</div>
            </div>
            <div class="col-md-12">
                <p>
                    {!! $data['content'] !!}
                </p>
            </div>
        @endif
        @if (!empty($newOtherRecomendation))    
            <div class="col-md-12 padding-top-20">
                <div class="movie-section-title">Other eReference</div>
            </div>
            <div class="flix-carousel">
                <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
                <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
                <div class="flix-scroll-x">
                    @foreach ($newOtherRecomendation as $otherEReference) 
                        <a href="{{ route('eReference.slug', $otherEReference['slug']) }}" alt="{{ $otherEReference['title'] }}"
                            class="poster ">
                            <div>
                                <img style="width: 170px;height: 255px;" src="{{ $otherEReference['image'] }}" />
                                <div>
                                </div>
                            </div>
                        </a>  
                    @endforeach
                </div>
            </div>
        @endif

        <div class="background-close  serie-dialog">
            <div class="login-box ">
                <div class="notif-head">
                    <span class="notif-close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <span class="notif-title"><i class="fa fa-fw fa-lock"></i>  </span>
                </div>
                <div class="notif-body">
                    <div class="carda__body">
                        <div class="box-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
