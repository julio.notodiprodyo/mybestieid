@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        @if (!empty($slideshow))
            <div class="col-sm-12 col-md-12">
                <div id="myCarousel" class="carousel slide w-100" data-ride="carousel">
                    <ol class="carousel-indicators float-right">
                        @foreach ($slideshow as $key => $item)
                            <li data-target="#myCarousel" data-slide-to="{{ $key }}"
                                class="@if ($loop->first) active @endif"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner w-100" role="listbox">
                        @foreach ($slideshow as $value)
                            <div class="carousel-item @if ($loop->first) active @endif">
                                <div class="col-xxl-5 col-lg-8 col-md-12">
                                    <img height="370" width="750" class="img-fluid" src="{{ $value['image'] }}">
                                    <a href="{{ $value['url'] }}" class="carousel-detail">
                                        <div>
                                            <h3>{{ $value['title'] }}</h3>
                                            <p>{{ str_limit($value['content'], 120, ' ...') }}</p>
                                            @if (!empty($value['content']))
                                                <button>
                                                    <i class="fa fa-bookmark-o"></i>
                                                    <span>Read More<span>
                                                </button>
                                            @endif
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8358126254142358"
                    crossorigin="anonymous"></script>
                <ins class="adsbygoogle"
                    data-ad-layout="in-article"
                    data-ad-format="fluid"
                    data-ad-client="ca-pub-8358126254142358"
                    data-ad-slot="7480537568"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        @endif

        @foreach ($content as $key => $item)
            @if (!empty($item['data']))
                <div class="col-sm-12 col-md-12 ">
                    <a href="#" class="title-more">
                        <span>{{ $item['label'] }}</span>
                        <i class="fa fa-th float-right"></i>
                    </a>
                </div>
                <div class="flix-carousel">
                    <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
                    <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
                    <div class="flix-scroll-x">
                        @foreach ($item['data'] as $value)
                            <a href="{{ $key == 'channel' ? route('channel.slug', $value['slug']) : route('vod.slug', $value['slug']) }}"
                                title="{{ $value['title'] }}" class="{{ $key == 'channel' ? 'channel ' : 'poster ' }}">
                                <img {!! $item['size_image'] !!}
                                    src="{{ !empty($value['image']) ? $value['image'] : ($key == 'channel' ? asset('frontend/dummy_150x75.png') : asset('frontend/dummy_170x255.png')) }}" />
                                <div>
                                    @if ($key == 'channel')
                                        @if ($value['is_hd'] == 1)
                                            <span>HD</span>
                                            <p class="two">LIVE</p>
                                        @endif
                                    @endif
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif
        @endforeach
    </div>
@endsection
