@extends('layouts.frontend')

@section('contact')
    <body class="dark-mode">
        <div class="login-page-bg"></div>
        <div class="login-page-bg-blur"></div>
        <div class="login-page-content">
            <a id="lightmode" href="#"><i class="fas fa-sun"></i></a>
            <a id="darkmode" href="#" style="display:none"><i class="fas fa-moon"></i> </a>
            <img src="{{ webConfig()['website_logo_header'] }}">
            <div class="login-box">
                <div class="notif-head">
                    <span class="notif-title"><i class="fas fa-envelope"></i> Contact us</span>
                </div>
                <div class="notif-body">
                    <div class="carda__body pdn--al">
                        <div class="row">
                            <form id="contact" name="form" method="post" action="{{ route('contact') }}" autocomplete="off" class="fos_user_registration_register">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="email" name="email" required="required" class="form-control" placeholder="E-mail" />
                                    @if ($errors->has('email'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('email') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="text" name="subject" required="required" class="form-control" placeholder="Subject" />
                                    @if ($errors->has('subject'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('subject') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" required="required" class="form-control" placeholder="Full Name" />
                                    @if ($errors->has('name'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('name') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <textarea name="message" required="required" class="form-control" placeholder="Your message"rows="5"></textarea>
                                    @if ($errors->has('message'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('message') }}
                                    </div>
                                    @endif
                                </div>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-raised btn-primary float-right">
                                        Send Message
                                    </button>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
@endsection

@push('scripts')
    <script type="text/javascript">
        document.querySelector('#contact').addEventListener('submit', function(e) {
            var form = this;
            let timerInterval;
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'Your message will be entered on our inbox!',
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, i am sure!'
                ],
                dangerMode: true,
            }).then(function(isConfirm) {
                if (isConfirm) {
                    swal('Please wait we checking your message!', {
                        buttons: false,
                        timer: 3000,
                    }).then(function() {
                        form.submit();
                    });
                } else {
                    swal('Cancelled', 'Your message has been cancelled :)', "error");
                }
            });
        });
    </script>
@endpush