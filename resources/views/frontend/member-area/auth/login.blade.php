<div class="notif-head">
    <span class="notif-close"><i class="fa fa-times" aria-hidden="true"></i></span>
    <span class="notif-title"><i class="fa fa-fw fa-lock"></i> Sign in to your account </span>
</div>
<div class="notif-body">
    <div class="carda__body pdn--al">
        <div class="row">
            {{-- <div class="col-sm-6 col-xs-6 mrg--bs">
                <a href="#" class="btn  btn-facebook btn-block">
                    <i class="fab fa-facebook-f"></i> Facebook
                </a>
            </div>
            <br>
            <br>
            <div class="col-sm-6 col-xs-6 mrg--bs">
                <a href="#" class="btn  btn-google btn-block">
                    <i class="fab fa-google"></i> Google
                </a>
            </div>
            <h4 class="dividr mrg--vh">
                <b class="dividr__content"><i class="dividr__label">OR</i></b>
            </h4> --}}
            <form autocomplete="off" id="form-login" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group ">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                </div>
                <div class="form-group ">
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>
                <div class="form-group  row mrg--vh">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="fas fa-sign-in-alt"></i> Sign-in
                        </button>
                    </div>
                </div>
                <div class="form-group checkbox clearfix">
                    <a href="{{ route('password.request') }}" class="btn btn-link float-right pdn--an-imp">
                        Reset Password
                    </a>
                    <label class="float-left remember-me">
                        <input type="checkbox" id="remember_me" name="_remember_me" value="on">
                            Remember Me
                    </label>
                </div>
                <div>
                    <p>Are you new here ? <a href="{{ route('index.member.registration') }}">Sign-up</a></p>
                </div>
        </div>
        </form>
    </div>
</div>