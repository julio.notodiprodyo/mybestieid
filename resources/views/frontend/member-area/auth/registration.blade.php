@extends('layouts.frontend')

@section('authentication')
<div class="login-box">
    <div class="notif-head">
        <span class="notif-title"><i class="fa fa-fw fa-user-plus"></i> Create your account now </span>
    </div>
    <div class="notif-body">
        <div class="carda__body pdn--al">
            <div class="row">
                {{-- <div class="col-sm-6 col-xs-6 mrg--bs">
                    <a href="#" class="btn  btn-facebook btn-block">
                        <i class="fab fa-facebook-f"></i> Facebook
                    </a>
                </div>
                <div class="col-sm-6 col-xs-6 mrg--bs">
                    <a href="#" class="btn  btn-google btn-block">
                        <i class="fab fa-google"></i> Google
                    </a>
                </div>
                <h4 class="dividr mrg--vh">
                    <b class="dividr__content"><i class="dividr__label">OR</i></b>
                </h4> --}}
                <form name="fos_user_registration_form" method="POST" action="{{ route('store.member.registration') }}" class="fos_user_registration_register" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input id="fos_user_registration_form_email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail" required autofocus>
                        <span class="validate-input"></span>
                        @if ($errors->has('email'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('email') }}
                            </div>
                        @endif

                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input id="fos_user_registration_form_name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Full Name" required autofocus>
                        <span class="validate-input"></span>
                        @if ($errors->has('name'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="fos_user_registration_form_plainPassword_first" type="password" class="form-control" name="password" placeholder="Password" required>
                        <span class="validate-input"></span>
                        @if ($errors->has('password'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input id="fos_user_registration_form_plainPassword_second" type="password" class="form-control" name="password_confirmation" placeholder="Password confirmation" required>
                        <span class="validate-input"></span>
                    </div>
                    <div class="form-group checkbox clearfix">
                        <label class="float-left remember-me">
                            <input type="checkbox" id="fos_user_registration_form_privacypolicy" name="privacypolicy" required="required" value="1" /> 
                            I read and i agree with this <a href="{{ route('home') }}">Privacy Policy</a>
                        </label>
                        <span class="validate-input"></span>
                    </div>
                    <div class="form-group  row mrg--vh">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-block">
                                <i class="fa fa-fw fa-user-plus"></i> Register
                            </button>
                        </div>
                    </div>
                    <div>
                        <p>You have already an account ? 
                            <a href="{{ route('login') }}">Log in</a><br>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
