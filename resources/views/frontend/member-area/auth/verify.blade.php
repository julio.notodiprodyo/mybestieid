@extends('layouts.frontend')

@section('authentication')
<div class="login-box">
    <div class="notif-head">
        <span class="notif-title"><i class="fa fa-fw fa-check"></i> {{ $status }}</span>
    </div>
    <div class="notif-body">
        <div class="carda__body pdn--al">
            <br>
            {!! $msg !!}
            <a href="{{ route('home') }}"><i class="fa fa-fw fa-home"></i> Go Home</a>
            <br>
            <br>
        </div>
    </div>
</div>
@endsection
