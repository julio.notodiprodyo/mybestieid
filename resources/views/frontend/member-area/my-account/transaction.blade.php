@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        @include('frontend.component.profile-section')
        <div class="col-md-9">
            <div id="ajaxModel" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" style="color: #000;">Payment Status Detail</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="ajaxSpinnerDemo" class="p-4 text-center">
                                <div class="d-flex align-items-center">
                                    <strong>Loading...</strong>
                                    <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                                </div>
                            </div>
                            <div id="modal-content-payment"></div>
                        </div>
                    </div>
                </div>
            </div>
            @include('frontend.component.table')
        </div>
    </div>
@endsection
