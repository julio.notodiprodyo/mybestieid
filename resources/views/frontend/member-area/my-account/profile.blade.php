@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        @include('frontend.component.profile-section')
        <div class="col-md-9">
            {!! Form::model($user, ['route' => ['self.member.update', $user->id], 'method' => 'put', 'class' => 'form', 'files' => true]) !!}
            <table>
                <table class="table">
                    <thead>
                    <tbody>
                        <tr>
                            <th scope="row">{!! Form::label('name', 'Full Name:', []) !!}</th>
                            <td>{!! Form::text('name', null, ['class' => 'form-control required']) !!}</td>
                        </tr>
                        <tr>
                            <th scope="row">{!! Form::label('phone', 'Phone Number:', []) !!} </th>
                            <td>{!! Form::text('phone', null, ['class' => 'form-control required']) !!}</td>
                        </tr>
                        <tr>
                            <th scope="row">{!! Form::label('email', 'Email:', []) !!} </th>
                            <td>{!! Form::email('email', null, ['class' => 'form-control required']) !!}</td>
                        </tr>
                        <tr>
                            <th scope="row">{!! Form::label('gender', 'Gender:', []) !!} </th>
                            <td>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="F"
                                        {{ $user->gender == 'F' ? 'checked' : '' }}>Female
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="M"
                                        {{ $user->gender == 'M' ? 'checked' : '' }}>Male
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">{!! Form::label('password', 'Password:', []) !!} </th>
                            <td>
                                <div class="input-group">
                                    {!! Form::password('password', ['class' => 'form-control required', 'id' => 'password-disabled']) !!}
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-warning pull-right"
                                            id="reset-pwd">Reset</button>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </table>
            <button type="submit" class="btn btn-raised btn-primary float-right">Save Change</button>
            </form>
        </div>
    </div>
@endsection
