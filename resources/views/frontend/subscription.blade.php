@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="col-md-6 padding-top-20">
            <div id="subscribe-plan">
                <div class="subscribe-title">
                    <span>1</span>
                    <h5>Select a plan that work for you.</h5>
                </div>
                <br>

                @foreach ($premiumPackage as $package)
                    <label class="pack" alt="{{ $package['time_period'] }}">
                        <div>
                            <input type="radio" id="form_packs_{{ $package['time_period'] }}" name="form[packs]"
                                required="required" value="{{ $package['time_period'] }}" />

                            <h5>{{ $package['package_name'] }}</h5>
                            <span>IDR {{ $package['amount'] }}</span>
                        </div>
                    </label>
                @endforeach

                <p class="error-plan">• Please select a plan</p>
                <span id="go-payment-method" class="float-right btn btn-primary">CONTINUE <i
                        class="fas fa-chevron-right"></i></span>
            </div>
            <div id="payment-method">
                <div class="subscribe-title" id="selected_pack">
                    <span>1</span>

                    <h5>x </h5>
                    <span class="fas fa-check float-right"></span>
                    <div alt></div>
                    <span class="float-right price-step-1">x</span>
                </div>
                <div class="subscribe-title">
                    <span>2</span>
                    <h5>Payment Information : </h5>
                </div>
                <br>
                <div class="alert alert-primary" role="alert">
                    You are going to be redirect to a third party site to make your payment.
                </div>
                <button class="float-right btn btn-primary" onclick="onCheckout()">
                    PAY NOW
                </button>
            </div>
        </div>
        <div class="col-md-6 padding-top-20">
            <div id="subscribe-plan">
                <div class="subscribe-title">
                    <h5>Why Premium?</h5>
                </div>
                <br>
                <br>
                <ul class="reason-list">
                    <li class="reason-item"><span>
                            Premium Content

                        </span></li>
                    <li class="reason-item"><span>
                            Download on Mobile

                        </span></li>
                    <li class="reason-item"><span>
                            International Channels

                        </span></li>
                    <li class="reason-item"><span>
                            No Ads

                        </span></li>
                    <li class="reason-item"><span>
                            DecxTV Originals

                        </span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="serie-episodes-loading">
        <i class="fas fa-circle-notch fa-spin"></i>
    </div>
@endsection

@push('scripts')
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ $clientKey }}"></script>
    {{-- <script src="https://app.midtrans.com/snap/snap.js" data-client-key="{{ $clientKey }}"></script> --}}
@endpush
