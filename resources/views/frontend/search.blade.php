@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        @if (!empty($query))
            @if (!empty($query['channel']))
                <div class="col-sm-12 col-md-12 ">
                    <a class="title-more">
                        <span>TV Channels</span>
                    </a>
                </div>
                <div class="flix-carousel">
                    <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
                    <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
                    <div class="flix-scroll-x">
                        @foreach ($query['channel'] as $item)
                            <a class="channel col-4 col-md-2 col-xxl-2" href="{{ route('channel.slug', $item['slug']) }}">
                                <img height="75" width="150"
                                    src="{{ !empty($item['image']) ? $item['image'] : asset('frontend/dummy_150x75.png') }}" />
                                @if ($item['is_hd'] == 1)
                                    <div>
                                        <span>HD</span>
                                        <p class="two">LIVE</p>
                                    </div>
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif

            @if (!empty($query['vod']))
                <div class="col-sm-12 col-md-12 ">
                    <a class="title-more">
                        <span>VOD</span>
                    </a>
                </div>
                @foreach ($query['data'] as $item)
                    <a href="{{ route('vod.slug', $item['slug']) }}" alt="{{ $item['title'] }}"
                        class="poster col-4 col-md-2 col-xxl-2">
                        <div>
                            <img width="170" height="255" src="{{ $item['image'] }}" alt="" />
                            <div></div>
                        </div>
                    </a>
                @endforeach

                <div class="filter-section">
                    <div class=" float-right">
                        <div class="pagination">
                            {{ $query['paginator']->appends(request()->input())->render('vendor.pagination.semantic-ui') }}
                        </div>
                    </div>
                </div>
            @endif
        @else
            <div class="empty-box">
                <img src="{{ asset('empty.svg') }}">
                <h3>Empty list</h3>
                <p>start adding some content in your list</p>
            </div>
        @endif
    </div>
@endsection
