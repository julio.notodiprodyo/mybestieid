@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="col-md-12">
            <div class="movie-header-bg" style=" background-image: url({{ $image }});">
                <div class="movie-header">
                    <div class="movie-title">{{ $data->title }}</div>
                    <div class="movie-infos">{{ $tags }}</div>
                    <div class="header-buttons">
                        <button id="share-btn">
                            <i class="fa fa-share "></i>SHARE
                            <div class="share-buttons">
                                <a href="http://www.facebook.com/sharer.php?u={{ route('vod.slug', $data['slug']) }}"
                                    target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ route('vod.slug', $data['slug']) }}"
                                    target="_blank">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="https://twitter.com/share?url={{ Request::fullUrl() }}&amp;text={{ $data->title }}"
                                    target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </button>
                    </div>
                    <div class="header-ratings">
                        <div class="rating-box">
                            <div class="rating" style="width:{{ $rate['percentage'] }}%;"></div>
                        </div>
                        • {{ strtok($rate['percentage'], '.') }}/100
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <button id="trailer-btn" class="btn play-btn">
                <span class="fa fa-bullhorn"></span> Watch Trailer
            </button>
            @if (!empty($premium))    
                @if ($premium->end_date >= Carbon::now()->toDateTimeString()) 
                <a href="{{ route('player.hsl', ['slug' => 'vod', 'hsl' => $data->slug]) }}" class="btn play-btn">
                    <span class="fa fa-play"></span> Watch Movie
                </a>
                @else
                <button id="watch-movie-btn" class="btn play-btn">
                    <span class="fa fa-play"></span> Watch Movie
                </button>
                @endif
            @else
            <button id="watch-movie-btn" class="btn play-btn">
                <span class="fa fa-play"></span> Watch Movie
            </button>
            @endif
            <a class="btn play-btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span class="fa fa-star"></span> Rate Movie
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <div class="comment-item  review-write">
                    @include('frontend.component.review-rate')
                </div>
            </div>
        </div>
        <div class="trailer-video" alt="{{ $data->youtube }}"></div>
        @if (!empty($data['content']))
        <div class="col-md-12 padding-top-20">
            <div class="movie-section-title">Overview</div>
        </div>
        <div class="col-md-12">
            {!! $data->content !!}
        </div>
        @endif
        <div class="col-md-12 padding-top-20">
            <div class="movie-section-title">For You</div>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">
                @foreach ($newOtherRecomendation as $recomendation)
                    <a href="{{ route('vod.slug', $recomendation['slug']) }}" alt="{{ $recomendation['title'] }}"
                        class="poster ">
                        <div>
                            <img style="width: 170px;height: 255px;" src="{{ $recomendation['image'] }}" />
                            <div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="col-md-6 padding-top-20">
            @include('frontend.component.comment')
            @guest
                <div class="comment-item comment-write">
                    <a title="View profile" href="#" class="avatar-thumb open-login">
                        <img src="{{ asset('images/nophoto.jpg') }}"></a>
                    <div class="comment-text">
                        <textarea class="input-comment open-login" alt="channel" placeholder="Write you comment here"></textarea>
                        <button alt="40" class="btn btn-primary float-right open-login">Submite Comment <i
                                class="fas fa-paper-plane" aria-hidden="true"></i> </button>
                    </div>
                </div>
            @endguest
        </div>
        <div class="col-md-6 padding-top-20">
            @include('frontend.component.review')
        </div>
    </div>
@endsection
