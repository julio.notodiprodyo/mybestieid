@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="filter-section">
            <div class="btn-group season-dropdown float-right" role="group" aria-label="Basic example">
                @include('frontend.component.button-filter')
            </div>
        </div>
    </div>
    <div class="row content-section serie-episodes">
        <div></div>
        @foreach ($vod as $item)
            <a href="{{ route('vod.slug', $item['slug']) }}" alt="{{ $item['title'] }}"
                class="poster col-4 col-md-2 col-xxl-2">
                <div>
                    <img width="170" src="{{ !empty($item['image']) ? $item['image'] : asset('/frontend/dummy_170x255.png') }}" alt="{{ $item['title'] }}" style="height:220px;" />
                    <div></div>
                </div>
            </a>
        @endforeach
    </div>
    <div class="serie-episodes-loading">
        <i class="fas fa-circle-notch fa-spin"></i>
    </div>
    {{-- <div class="row content-section">
        <div class="filter-section">
            <div class=" float-right">
                <div class="pagination">

                    {{ $paginator->appends($get)->render('vendor.pagination.semantic-ui') }}
                    
                </div>
            </div>
        </div>
    </div> --}}
@endsection
