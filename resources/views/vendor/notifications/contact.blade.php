<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        body {
            font-family: Poppins;
            background-color: #DDD;
        }

        .small {
            font-size: 14px;
            line-height: 18px;
            font-weight: 300;
        }

        a {
            color: #42004f;
            transition: 0.2s linear all;
        }

        a:hover {
            color: #01E4D3;
        }

        p {
            font-size: 16px;
        }

        .workload-header {
            background-image: url('/img/howitworks_header_desktop-1920x320.png');
            background-size: cover;
            margin-left: -15px;
            margin-right: -15px;
            padding-left: 15px;
            padding-right: 15px;
            color: #eee;
            margin-top: -15px;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
            height: 60px;
            padding-top: 25px;
        }

        hr {
            border: 0;
            border-top: 1px solid #42004f;
            margin-left: -15px;
            margin-right: -15px;
            opacity: 0.1;
        }

        h1 {
            font-size: 24px;
            letter-spacing: -0.2px;
        }

        .wrapper {
            text-align: center;
        }

        .cta a {
            color: #FFF;
            text-decoration: none;
            font-weight: 600;
        }

        .cta a:hover {
            color: #FFF;
        }

        .cta:hover {
            background-color: #01E4D3;
        }

        .cta:hover a {
            color: #42004f;
            transition: 0.2s linear all;
        }

        .cta {
            transition: 0.2s linear all;
            color: #EEE;
            border: 0;
            border-radius: 15px;
            cursor: pointer;
            background-color: #42004f;
            padding: 20px 70px !important;
            font-size: 16px !important;
            font-family: Poppins;
            letter-spacing: 0.2px;
            margin-bottom: 25px;
        }

        .footer {
            margin-top: -20px;
            font-size: 12px;
            font-weight: 100;
            letter-spacing: 0.2px;
        }

        .card {
            background-color: #FFF;
            border-radius: 15px;
            max-width: 600px;
            margin: 0 auto;
            text-align: left;
            padding: 15px;
            margin-top: 15px;
        }

    </style>
</head>

<body>
    <!-- partial:index.partial.html -->
    <!DOCTYPE html>
    <html>

    <head>
        <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    </head>

    <body>
        <div class="wrapper">
            <div class="card">
                <p>Hello Admin,</p>
                <p>You received an email from : {{ $email }}, </p>
                <p>Here are the details :</p>
                <table border="0" cellpadding="0" cellspacing="0" width="600">
                    <tbody>
                        <tr>
                            <td height="60" valign="top">
                                <table cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="16" valign="top" width="300">
                                                Name
                                            </td>
                                            <td align="center" height="16" valign="top" width="20">
                                                :
                                            </td>
                                            <td height="16" valign="top" width="422">
                                                {{ $name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="16" valign="top" width="300">
                                                Subject
                                            </td>
                                            <td align="center" height="16" valign="top" width="20">
                                                :
                                            </td>
                                            <td height="16" valign="top" width="422">
                                                {{ $subject }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="16" valign="top" width="300">
                                                Message
                                            </td>
                                            <td align="center" height="16" valign="top" width="20">
                                                :
                                            </td>
                                            <td height="16" valign="top" width="422">
                                                {{ $body }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" valign="top">
                                                &nbsp;
                                            </td>
                                            <td align="center" height="8" valign="top" width="20">
                                                &nbsp;
                                            </td>
                                            <td height="8" valign="top" width="422">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <p class="small">If you believe you've reveived this email errorneously, you can ignore this
                    email. If you are having any issues with this email, please email <a href="njulioiyoo@gmail.com"> IT
                        DEV PT DTM</a>.</p>
            </div>
            </br>
            <img src="{{ route('home') . '' . webConfig()['website_logo_header'] }}" width="120" height="53"></br></br>
            <p class="footer">{{ strtoupper(webConfig()['website_title']) }} © {{ date('Y') }} . ALL RIGHTS
                RESERVED.</p>
        </div>
    </body>
    <!-- partial -->
</body>

</html>
