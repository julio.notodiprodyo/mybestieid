@if ($paginator->hasPages())
    <div class="ui pagination menu">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="icon item disabled"> 
                <i class="left chevron icon"></i> 
            </a>
        @else
            <span class="previous">
                <a class="icon item" href="{{ $paginator->previousPageUrl() }}" rel="prev"> 
                    &lt;
                </a>
            </span>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <a class="icon item disabled">
                    {{ $element }}
                </a>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <span class="current">
                            {{ $page }}
                        </span>
                    @else
                        <span class="page">
                            <a class="item" href="{{ $url }}">
                                {{ $page }}
                            </a>
                        </span>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <span class="next">
                <a class="icon item" href="{{ $paginator->nextPageUrl() }}" rel="next"> 
                    &gt;
                </a>
            </span>
        @else
            <a class="icon item disabled"> 
                <i class="right chevron icon"></i> 
            </a>
        @endif
    </div>
@endif
