
@section('css')
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.backend.datatables_css')
@endsection

@if(auth()->user()->roles->first()->name === 'brand')
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('brand-name', 'Brand Name:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('brand_name', null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- PIC Field -->
<div class="form-group">
    {!! Form::label('pic-name', 'PIC:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('email', 'Email Address:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => true, 'readonly' => ($formType == 'edit') ? true : false]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone Number:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    </div>
</div>
@if($formType == 'edit')
<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-8">
        {!! Form::text('password', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
    
    <div class="col-sm-2">
        <button type="button" id="reset-pwd" class="btn btn-default">Reset</button>
    </div>
    
</div>
@else
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('password', null, ['class' => 'form-control']) !!}
    </div>
</div>
@endif
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('saldo', 'Saldo:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="input-group m-b">
            <span class="input-group-addon">Rp</span>
            {!! Form::text('saldo', null, ['class' => 'form-control', 'readonly' => true]) !!}
        </div>
        
    </div>
</div>
@else
<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('image', 'Photo:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>
<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('cover', 'Background Image:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="cover" name="cover" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-2" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('full-name', 'Full Name:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('name', ($formType == 'edit') ? $user->name : null, ['class' => 'form-control', 'maxlength' => 50]) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('gender', ['M' => 'Male', 'F' => 'Female'], ($formType == 'edit') ? $user->gender : null, ['placeholder' => 'Select Gender', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('email', 'Email Address:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('email', ($formType == 'edit') ? $user->email : null, ['class' => 'form-control', 'readonly' => true, 'maxlength' => 50]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('dob', 'Date of Birth:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('dob', isset($user->dob) ? $user->dob->format('d-m-Y') : '', ['class' => 'form-control dateinput']) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone Number:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('phone', ($formType == 'edit') ? $user->phone : null, ['class' => 'form-control', 'maxlength' => 30]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('country', 'Country:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('country', ($formType == 'edit') ? $user->country : null, ['class' => 'form-control', 'maxlength' => 40]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('city', 'City:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
    {!! Form::select('city', $cities, ($formType == 'edit') ? $user->city : null, ['class' => 'form-control selectpicker', 'placholder' => 'Select City', 'data-live-search' => 'true', 'data-size' => 7]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('ktp', 'KTP/SIM:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('ktp', null, ['class' => 'form-control', 'maxlength' => 30]) !!}
    </div>
</div>

<!-- Sub Title Field -->
<div class="form-group">
    {!! Form::label('biodata', 'Biodata:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('biodata', null, ['class' => 'form-control']) !!}
    </div>
</div>
@endif
<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(auth()->user()->roles->first()->name === 'creator')
        <a href="{!! route('creator.personal.index') !!}" class="btn btn-default">Cancel</a>
    @elseif(auth()->user()->roles->first()->name === 'brand')
        <a href="{!! route('brand.personal.index') !!}" class="btn btn-default">Cancel</a>
    @elseif(auth()->user()->roles->first()->name === 'finance')
        <a href="{!! route('finance.personal.index') !!}" class="btn btn-default">Cancel</a>
    @elseif(auth()->user()->roles->first()->name === 'legal')
        <a href="{!! route('legal.personal.index') !!}" class="btn btn-default">Cancel</a>
    @else
        <a href="{!! route('admin.personal.index') !!}" class="btn btn-default">Cancel</a>
    @endif
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
<script>

    var formType = '{{ $formType }}';
    $('#photo').val('');
    $('.dateinput').datepicker({
        format: 'dd-mm-yyyy'
    });

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $user->image) ? asset($user->image) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    $("#cover").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $user->cover) ? asset($user->cover) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    tinymce.init({
        forced_root_block : "",
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        file_picker_types: 'image',
        images_upload_credentials: true,
        automatic_uploads: false,
        theme: "modern",
        paste_data_images: true,
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor",
        image_advtab: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.
        
            input.onchange = function() {
              var file = this.files[0];
              
              var reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
        
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
              };
            };
            
            input.click();
        }
    });

    @if($formType == 'edit')
    $('#password').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password').val('');
        $('#password').attr('disabled', false);
    });
    @endif
    
</script>
@if($formType == 'edit')
@include('layouts.backend.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
@endif
@endsection