@section('css')
    @include('layouts.backend.datatables_css')
@endsection
{{-- <div class="table-responsive"> --}}
{!! $dataTable->table(['width' => '100%']) !!}
{{-- </div> --}}
@section('scripts')
    @include('layouts.backend.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        function changeRecommend(_id) {
            $.ajax({
                url: "{{ route('admin.posts.updatePopular') }}",
                method: "POST",
                data: {
                    id: _id
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function(response) {
                    console.log(response);
                    toastr.success("Mengubah Popular Berhasil");
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
        $(document).on('click', '.popular', function(e) {
            var id = $(this).attr('data-id');
            changeRecommend(id);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
    </script>
    @include('layouts.backend.datatables_limit')
@endsection
