<div class="form-group">
    <div class="row" style="padding: 10px;">
        <div class="input-group">
            <div class="form-line">
                <div class="alert alert-success" role="alert">
                    <p class="text-center">Max 5 to create premium package!</p>
                </div>
                <input type="hidden" name="premium" id="premium" value="1">
                <table class="table table-bordered" id="dynamicTable">
                    <tr>
                        <th>Package Name</th>
                        <th>Amount</th>
                        <th>Vat Payable</th>
                        <th>Time Period</th>
                        <th>Action</th>
                    </tr>

                    <tr>

                        <td>
                            <input type="text" name="addmore[0][package_name]" placeholder="Package Name" class="form-control" />
                        </td>

                        <td>
                            <input type="number" name="addmore[0][amount]" placeholder="Amount" class="form-control" />
                        </td>

                        <td>
                            <input type="number" name="addmore[0][vat_payable]" placeholder="Vat Payable" class="form-control" />
                        </td>

                        <td>
                            <select class="form-control" name="addmore[0][time_period]">
                                <option selected="selected" value="">Select Time Period</option>
                                <option value="1 Days">1 Days</option>
                                <option value="1 Months">1 Months</option>
                                <option value="3 Months">3 Months</option>
                                <option value="6 Months">6 Months</option>
                                <option value="12 Months">12 Months</option>
                            </select>
                        </td>

                        <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>

                    </tr>

                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var i = 0;
    var max_fields = 5;
    $("#add").click(function(){
        if (i < max_fields) {
            ++i;
            $("#dynamicTable").append('<tr><td><input type="text" name="addmore['+i+'][package_name]" placeholder="Package Name" class="form-control" /></td><td><input type="number" name="addmore['+i+'][amount]" placeholder="Amount" class="form-control" /></td><td><input type="number" name="addmore['+i+'][vat_payable]" placeholder="Vat Payable" class="form-control" /></td><td><select class="form-control" name="addmore['+i+'][time_period]"><option selected="selected" value="">Select Time Period</option><option value="1 Days">1 Day</option><option value="1 Months">1 Month</option><option value="3 Months">3 Month</option><option value="6 Months">6 Month</option><option value="12 Months">12 Month</option></select></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
        }
    });

    $(document).on('click', '.remove-tr', function(){  
         $(this).parents('tr').remove();
    });  

    $('select[name="category"]').change(function() {
        if ($(this).val() == "video") {
            $('.premium-package').show();
        } else {
            $('.premium-package').hide();
        }
    });
</script>
