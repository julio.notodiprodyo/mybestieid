@extends('layouts.backend.crud')

@section('breadcrumb')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Posts</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.posts.index') }}">Posts</a>
                </li>
                <li class="active">
                    <strong>Create Automatic</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('flash::message')
        {!! Form::open(['route' => 'admin.posts.store', 'class' => 'form-horizontal', 'files' => true]) !!}
        <div class="col-lg-6">
            
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Create Post Automatic</h5>
                </div>
                <div class="ibox-content">
                    
                    @include('backend.posts.fields', [
                        'formType' => 'create',
                        'postType' => 'automatic',
                    ])

                    
                </div>
            </div>
        </div>
        <div class="col-lg-6 premium-package" style="display: none;">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Create Premium Package</h5>
                </div>

                <div class="ibox-content">

                    @include('backend.posts.premium_package.fields')

                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection