{!! Form::open(['route' => ['admin.posts.destroy', $id], 'method' => 'delete']) !!}
@if($post_type == 'manual')
<div class='btn-group'>
	{{-- <a href="{{ route('creator.serie.show', $id) }}" class='btn btn-info btn-xs' title="Show Series">
	Show
	</a> --}}
	<a href="{{ route('admin.posts.editManual', $id) }}" class='btn btn-primary btn-xs' title="Edit Post">
		<i class="glyphicon glyphicon-edit"></i>
	</a>
	@if($is_active == 1)
	{!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
		'type' => 'submit',
		'class' => 'btn btn-warning btn-xs',
		'title' => 'Inactive Video Music',
		'onclick' => "return confirm('Do you want to inactive this video music?')",
		'name' => 'action',
		'value' => 'inact'
	]) !!}
	@else
	{!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
		'type' => 'submit',
		'class' => 'btn btn-success btn-xs',
		'title' => 'Activated Video Music',
		'onclick' => "return confirm('Do you want to activated this video music?')",
		'name' => 'action',
		'value' => 'act'
	]) !!}
	@endif
	{!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
		'type' => 'submit',
		'class' => 'btn btn-danger btn-xs',
		'title' => 'Delete Post',
		'onclick' => "return confirm('Do you want to delete this video music?')",
		'name' => 'action',
		'value' => 'del'
	]) !!}
</div>
@else
<div class='btn-group'>
	<a href="{{ route('admin.posts.editAutomatic', $id) }}" class='btn btn-primary btn-xs' title="Edit Post">
		<i class="glyphicon glyphicon-edit"></i>
	</a>
	@if($is_active == 1)
	{!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
		'type' => 'submit',
		'class' => 'btn btn-warning btn-xs',
		'title' => 'Inactive Video Music',
		'onclick' => "return confirm('Do you want to inactive this video music?')",
		'name' => 'action',
		'value' => 'inact'
	]) !!}
	@else
	{!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
		'type' => 'submit',
		'class' => 'btn btn-success btn-xs',
		'title' => 'Activated Video Music',
		'onclick' => "return confirm('Do you want to activated this video music?')",
		'name' => 'action',
		'value' => 'act'
	]) !!}
	@endif
	{!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
		'type' => 'submit',
		'class' => 'btn btn-danger btn-xs',
		'title' => 'Delete Post',
		'onclick' => "return confirm('Do you want to delete this video music?')",
		'name' => 'action',
		'value' => 'del'
	]) !!}
</div>
@endif
{!! Form::close() !!}