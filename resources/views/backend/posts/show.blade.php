@extends('layouts.backend.crud')

@section('breadcrumb')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Content</h2>
            <ol class="breadcrumb">
                <li>
                    Posts
                </li>
                <li>
                    @if (auth()->user()->roles->first()->name === 'admin')
                        <a href="{{ route('admin.posts.index') }}">Content</a>
                    @elseif(auth()->user()->roles->first()->name === 'user')
                        <a href="{{ route('user.posts.index') }}">Content</a>
                    @else
                        <a href="{{ route('admin.posts.index') }}">Content</a>
                    @endif
                </li>
                <li class="active">
                    Detail
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('contentCrud')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Content</h5>
        </div>
        <div class="ibox-content">
            <div class="form-horizontal">
                @include('backend.content.show_fields')
            </div>
        </div>
    </div>
@endsection
