{!! Form::open(['route' => ['ereference.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('ereference.edit', $id) }}" class='btn btn-default btn-xs' title="Edit E-Reference">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    @if($is_active == 1)
    {!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-warning btn-xs',
        'title' => 'Inactive E-Reference',
        'onclick' => "return confirm('Do you want to inactive this E-Reference?')",
        'name' => 'action',
        'value' => 'inact'
    ]) !!}
    @else
    {!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-success btn-xs',
        'title' => 'Activated E-Reference',
        'onclick' => "return confirm('Do you want to activated this E-Reference?')",
        'name' => 'action',
        'value' => 'act'
    ]) !!}
    @endif
    {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Delete E-Reference',
        'onclick' => "return confirm('Do you want to delete this E-Reference?')",
        'name' => 'action',
        'value' => 'del'
    ]) !!}
</div>
