@section('css')
    <link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">

    <style>
        .kv-photo .krajee-default.file-preview-frame,
        .kv-photo .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }

        .kv-photo .file-input {
            display: table-cell;
            max-width: 220px;
        }

        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }

    </style>
    @include('layouts.backend.datatables_css')
@endsection
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 100]) !!}
    </div>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('content', 'Description', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('content', null, ['id' => 'description', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('image', 'Thumbnail (Custom)', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10" >
        <input id="input-id" type="file" name="pdf" data-preview-file-type="text" >
    </div>
</div>


<div class="form-group">
    {!! Form::label('is_active', 'Status:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <input type="hidden" name="is_active" value="0">
        <input type="checkbox" name="is_active" class="js-switch"
            {{ isset($ereference) && $ereference->is_active == 1 ? 'checked' : '' }} />
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('ereference.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('scripts')
    <script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
    <script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
    <script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, {
                color: '#1AB394'
            });
            initTinyMce();
            initFileInput();
        });

        var formType = '{{ $formType }}';

        $("#input-id").fileinput({
            uploadUrl: "#",
            showUpload: false,
            allowedFileExtensions: ["csv", 'xlsx', 'pdf'],
            fileActionSettings: {
                showUpload: false,
            }
        });

        // init file input
        function initFileInput() {
            $('#photo').val('');

            $("#photo").fileinput({
                overwriteInitial: true,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                browseLabel: '',
                removeLabel: '',
                browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                removeTitle: 'Cancel or reset changes',
                elErrorContainer: '#kv-photo-errors-1',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="{{ $formType == 'edit' && $ereference->image ? $ereference->image : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
                layoutTemplates: {
                    main2: '{preview} {remove} {browse}'
                },
                allowedFileExtensions: ["jpg", "png", "jpeg"]
            });
        }
        // end init file input

         // init tiny mce
        function initTinyMce() {
            tinymce.init({
                selector: "textarea",
                setup: function(editor) {
                    editor.on('change', function() {
                        tinymce.triggerSave();
                    });
                },
                entity_encoding: "raw",
                element_format: 'html',
                allow_html_in_named_anchor: true,
                forced_root_block: 'p',
                file_picker_types: 'image',
                schema: 'html5',
                images_upload_credentials: true,
                automatic_uploads: false,
                theme: "modern",
                paste_data_images: true,
                plugins: [
                    "advlist autolink lists link image media  charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime nonbreaking save table contextmenu directionality",
                    "template paste textcolor colorpicker textpattern"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | media image",
                image_advtab: true,
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    // Note: In modern browsers input[type="file"] is functional without 
                    // even adding it to the DOM, but that might not be the case in some older
                    // or quirky browsers like IE, so you might want to add it to the DOM
                    // just in case, and visually hide it. And do not forget do remove it
                    // once you do not need it anymore.

                    input.onchange = function() {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function() {
                            // Note: Now we need to register the blob in TinyMCEs image blob
                            // registry. In the next release this part hopefully won't be
                            // necessary, as we are looking to handle it internally.
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            // call the callback and populate the Title field with the file name
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                    };

                    input.click();
                }
            });
        }
        // end tiny mce
    </script>
    @if ($formType == 'edit')
        @include('layouts.backend.datatables_js')
        <script>
            $.fn.dataTable.ext.buttons.create = {
                action: function(e, dt, button, config) {
                    $('#myModal').modal('show');
                }
            };

            var hash = document.location.hash;
            if (hash) {
                console.log(hash);
                $('.nav-tabs a[href="' + hash + '"]').tab('show')
            }

            // Change hash for page-reload
            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                window.location.hash = e.target.hash;
            });
        </script>
    @endif
@endsection
