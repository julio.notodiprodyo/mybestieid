@section('css')
    <link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">

    <style>
        .kv-photo .krajee-default.file-preview-frame,
        .kv-photo .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }

        .kv-photo .file-input {
            display: table-cell;
            max-width: 220px;
        }

        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }

    </style>
    @include('layouts.backend.datatables_css')
@endsection
{{ Form::hidden('type', 'channel') }}
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 125]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('website_url', 'Website URL:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::url('attr_4', null, ['class' => 'form-control', 'maxlength' => 125]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('type_audio', 'Type Audio:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('post_type', ['m3u8' => 'M3U8', 'mpeg' => 'MPEG'], null, ['placeholder' => 'Select Type Audio', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group m3u8" style="display: none;">
    {!! Form::label('type_mpeg', 'MPEG:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::url('attr_7', null, ['class' => 'form-control', 'maxlength' => 125]) !!}
    </div>
</div>

<div class="form-group mpeg" style="display: none;">
    {!! Form::label('type_m3u8', 'M3U8:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::url('attr_3', null, ['class' => 'form-control', 'maxlength' => 125]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('category', 'Category', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <select name="category_id" class="form-control selectpicker" required>
            <option value="">Select Category</option>
            @foreach ($category as $val)
            <option value="{{ $val->id }}" {{ isset($channel) ? (($val->id == $channel->parent_id) ? 'selected' : '') : '' }}> {{ $val->title }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group mpeg" style="display: none;">
    {!! Form::label('ads', 'Ads', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <select name="ads_id" class="form-control selectpicker">
            <option value="">Select Ads</option>
            @foreach ($ads as $val)
            <option value="{{ $val->ads_id }}" {{ isset($contentAds) ? (($val->ads_id == $contentAds->ads_id) ? 'selected' : '') : '' }}> {{ $val->title }}</option>
            @endforeach
        </select>
    </div>
</div>

<!-- Tags Field -->
<div class="form-group">
    {!! Form::label('tag', 'Tag (maks 5)', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('tags', null, ['id' => 'tags-input', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Sub Title Field -->
<div class="form-group">
    {!! Form::label('content', 'Description:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('image', 'Picture:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('order', 'Order:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::number('order', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('channel.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('scripts')
    <script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
    <script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
    <script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
    <script>
        var formType = '{{ $formType }}';

        $(document).ready(function() {
            initSourceTags();
        });

        $('#photo').val('');

        $("#photo").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-photo-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="{{ $formType == 'edit' && $channel->image ? $channel->image : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
            layoutTemplates: {
                main2: '{preview} {remove} {browse}'
            },
            allowedFileExtensions: ["jpg", "png", "jpeg"]
        });

        tinymce.init({
            forced_root_block: "",
            selector: "textarea",
            setup: function(editor) {
                editor.on('change', function() {
                    tinymce.triggerSave();
                });
            },
            file_picker_types: 'image',
            images_upload_credentials: true,
            automatic_uploads: false,
            theme: "modern",
            paste_data_images: true,
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime nonbreaking save table contextmenu directionality",
                "template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor",
            image_advtab: true,
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                // Note: In modern browsers input[type="file"] is functional without 
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.

                input.onchange = function() {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function() {
                        // Note: Now we need to register the blob in TinyMCEs image blob
                        // registry. In the next release this part hopefully won't be
                        // necessary, as we are looking to handle it internally.
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        cb(blobInfo.blobUri(), {
                            title: file.name
                        });
                    };
                };

                input.click();
            }
        });

        // init source tags
        function initSourceTags() {
            var source_ = new Array();
            @foreach ($terms as $value)
                source_.push('{{ $value }}');
            @endforeach

            $('#tags-input').tagsinput({
                tagClass: 'label label-primary',
                maxTags: 5,
                typeahead: {
                    afterSelect: function(val) {
                        this.$element.val("");
                    },
                    source: source_
                }
            });
        }
        // end init source tags

        $('select[name="post_type"]').change(function() {
            if ($(this).val() == "mpeg") {
                $('.mpeg').hide();
                $('.m3u8').show();
            } else {
                $('.mpeg').show();
                $('.m3u8').hide();
            }
        });
    </script>
    @if ($formType == 'edit')
        @include('layouts.backend.datatables_js')
        <script>
            $.fn.dataTable.ext.buttons.create = {
                action: function(e, dt, button, config) {
                    $('#myModal').modal('show');
                }
            };

            var hash = document.location.hash;
            if (hash) {
                console.log(hash);
                $('.nav-tabs a[href="' + hash + '"]').tab('show')
            }

            // Change hash for page-reload
            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                window.location.hash = e.target.hash;
            });
        </script>
    @endif
@endsection
