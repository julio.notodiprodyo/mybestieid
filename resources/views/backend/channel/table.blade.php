@section('css')
    @include('layouts.backend.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    @include('layouts.backend.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        function changeRecommend(_id, flag) {
            $.ajax({
                url: '{{ route('admin.channel.updateFeatured') }}',
                method: 'POST',
                data: {
                    id: _id,
                    flag: flag
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function(response) {
                    toastr.success(`Mengubah ${flag.toUpperCase()} Berhasil`);
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
        $(document).on('click', '.featured', function(e) {
            var id = $(this).attr('data-id');
            changeRecommend(id, 'featured');
        });
        $(document).on('click', '.hd', function(e) {
            var id = $(this).attr('data-id');
            changeRecommend(id, 'hd');
        });
    </script>
    @include('layouts.backend.datatables_limit')
@endsection
