@extends('layouts.backend.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Account Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Account Management</a>
            </li>
            <li class="active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
@endsection

@section('css')
@include('layouts.backend.datatables_css')
@endsection 

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Account Management
            </div>
            <div class="panel-body">
               <div class="form-group">
                    <div class="col-sm-12">
                        <table class="table table-bordered" id="table-revenue-creator">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>User Master</th>
                                    <th>User Parent</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach($datas as $data)
                                    @php
                                        $master = App\Model\User::where(['id'=> $data->user_master])->first();
                                        $child = App\Model\User::where(['id'=> $data->user_child])->first();
                                    @endphp
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$master->name}}</td>
                                        <td>{{$child->name}} {{ $data->id }}</td>
                                        <td align='center'><a href="{{route('account_management.delete', [$data->user_master, $data->user_child])}}" class="btn btn-md btn-primary"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

    @section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>

        <script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
        @include('layouts.backend.datatables_js')
        <script>    
            $(document).ready(function(){
                $('#table-revenue-creator').DataTable();
                $("div").removeClass("ui-toolbar");
            });
        </script>
    @endsection


@endsection