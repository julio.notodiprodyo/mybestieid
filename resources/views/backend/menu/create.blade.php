@extends('layouts.backend.crud')

@section('breadcrumb')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menu Management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('menu.index') }}">Menu</a>
                </li>
                <li class="active">
                    <strong>Create</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection
@section('contentCrud')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @include('flash::message')

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Category</h5>
        </div>
        <div class="ibox-content">
            {!! Form::open(['route' => 'menu.store', 'class' => 'form-horizontal', 'files' => true]) !!}
            @if (isset($parent))
                @include('backend.menu.fields', ['formType' => 'create'])
            @else
                @include('backend.menu.field', ['formType' => 'create'])
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
