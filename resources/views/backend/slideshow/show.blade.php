@extends('layouts.backend.crud')

@section('breadcrumb')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Slideshow</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('slideshow.index') }}">Slideshow</a>
                </li>
                <li class="active">
                    <strong>Detail</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('contentCrud')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Slideshow</h5>
        </div>
        <div class="ibox-content">
            {!! Form::model($slideshow, ['route' => ['slideshow.update', $slideshow->slideshow_id], 'class' => 'form-horizontal', 'files' => true]) !!}
            @include('backend.slideshow.show_fields', ['formType' => 'edit'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection
