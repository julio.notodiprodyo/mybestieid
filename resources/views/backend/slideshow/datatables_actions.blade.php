{!! Form::open(['route' => ['slideshow.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('slideshow.show', $id) }}" class='btn btn-default btn-xs' title="Show Slideshow">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('slideshow.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Slideshow">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    @if($is_active == 1)
    {!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-warning btn-xs',
        'title' => 'Inactive Slideshow',
        'onclick' => "return confirm('Do you want to inactive this slideshow?')",
        'name' => 'action',
        'value' => 'inact'
    ]) !!}
    @else
    {!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-success btn-xs',
        'title' => 'Activated Slideshow',
        'onclick' => "return confirm('Do you want to activated this slideshow?')",
        'name' => 'action',
        'value' => 'act'
    ]) !!}
    @endif
    {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Delete Post',
        'onclick' => "return confirm('Do you want to delete this slideshow?')",
        'name' => 'action',
        'value' => 'del'
    ]) !!}
</div>
{!! Form::close() !!}