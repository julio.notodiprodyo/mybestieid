@section('css')
@endsection
<div class="form-group">
    {!! Form::label('parent_id', 'Parent:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('parent_id', $pageList, $page->parent_id, ['placeholder' => 'Select', 'class' => 'form-control m-b', 'readonly' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', $page->title, ['class' => 'form-control', 'readonly' => true]) !!}
    </div>
</div>

<!-- Sub Title Field -->
<div class="form-group">
    {!! Form::label('subtitle', 'Sub Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('subtitle', $page->subtitle, ['class' => 'form-control', 'readonly' => true]) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Status:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('is_active', ['1' => 'Active', '0' => 'Inactive'], $page->is_active, ['class' => 'form-control', 'readonly' => true]) !!}
    </div>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('content', $page->content, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
        <a href="{!! route('page.index') !!}" class="btn btn-default">Back</a>
    </div>
</div>

@section('scripts')
    <script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
    <script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
    <script>
        tinymce.init({
            selector: "textarea",
            file_picker_types: 'image',
            images_upload_credentials: true,
            automatic_uploads: false,
            theme: "modern",
            paste_data_images: true,
            plugins: [],
            toolbar1: "",
            image_advtab: true,
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                // Note: In modern browsers input[type="file"] is functional without 
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.

                input.onchange = function() {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function() {
                        // Note: Now we need to register the blob in TinyMCEs image blob
                        // registry. In the next release this part hopefully won't be
                        // necessary, as we are looking to handle it internally.
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        cb(blobInfo.blobUri(), {
                            title: file.name
                        });
                    };
                };

                input.click();
            }
        });
    </script>
@endsection
