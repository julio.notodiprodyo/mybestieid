<div class='btn-group'>
    <a href="{{ route('user.edit', $id) }}" class='btn btn-default btn-xs' title="Edit User">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>
