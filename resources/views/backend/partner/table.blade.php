@section('css')
    @include('layouts.backend.datatables_css')
@endsection
{!! $dataTable->table(['width' => '100%']) !!}
@section('scripts')
    @include('layouts.backend.datatables_js')
    {!! $dataTable->scripts() !!}
    @include('layouts.backend.datatables_limit')

    <script>
        $("#enddate").change(function () {
        var startDate = document.getElementById("startdate").value;
        var endDate = document.getElementById("enddate").value;
    
        if ((Date.parse(endDate) <= Date.parse(startDate))) {
            alert("End date should be greater than Start date");
            document.getElementById("enddate").value = "";
        }
    });
    </script>
@endsection
