<?php
    ob_end_clean();
    header('Content-Type: application/xlsx');
?>
<html>

<head></head>
<body>
    <table>
        <tr>
            <td>DECXTV </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>VOD REPORTING CLIENT</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Client</td>
            <td>: {{ Auth::user()->name }}</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Periode</td>
            <td>: {{ request()->input('startdate').' - '.request()->input('enddate') }}</td>
            <td></td>
        </tr>
        <tr>
            <th scope="col">#</th>
            <td>Date</td>
            <td>Time</td>
            <td>VOD Content</td>
            <td>Acount Cust</td>
            <td>Periode</td>
            <td>Exp</td>
            <td>Paid</td>
            <td>Adm</td>
            <td>Remaks</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php
            $i = 0;
        ?>
        @foreach ($transactions as $key => $trx)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ !empty($trx->created_at) ? \Carbon\Carbon::parse($trx->created_at)->format('Y-m-d') : '' }}</td>
            <td>{{ !empty($trx->created_at) ? \Carbon\Carbon::parse($trx->created_at)->format('h:i:s') : '' }}</td>
            <td>{{ !empty($trx->title) ? $trx->title : '' }}</td>
            <td>{{ !empty($trx->email) ? $trx->email : '' }}</td>
            <td>{{ !empty($trx->time_period) ? $trx->time_period : '' }}</td>
            <td>{{ !empty($trx->end_date) ? \Carbon\Carbon::parse($trx->end_date)->format('Y-m-d h:i:s') : '' }}</td>
            <td>IDR {{ !empty($trx->amount) ? number_format($trx->amount) : 0 }}</td>
            <td>IDR {{ !empty($trx->vat_payable) ? number_format($trx->vat_payable) : 0 }}</td>
            <td>{{ !empty($trx->payment_type) ? $trx->payment_type : '' }}</td>
        </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>IDR {{ !empty($totalAmount) ? number_format($totalAmount->amount) : 0 }}</td>
            <td>IDR {{ !empty($totalVatPayable) ? number_format($totalVatPayable->vat_payable) : 0 }}</td>
            <td></td>
        </tr>
    </table>
</body>
</html>
