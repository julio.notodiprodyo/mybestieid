@extends('layouts.backend.crud')

@section('breadcrumb')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Partner</h2>
            <ol class="breadcrumb">
                <li>
                    Partner
                </li>
                <li class="active">
                    <a href="{{ route('admin.partner.index') }}"><strong>Table</strong></a>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('contentCrud')
    @include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Report Transaction</h5>
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal"
                        data-target="#modalExportReport" style="margin-top: -10px;margin-bottom: 5px">
                        Export Report
                    </button>
                    <div class="modal inmodal fade" id="modalExportReport" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Select Period</h4>
                                </div>
                                <div class="modal-body" style="text-align: center">
                                    {!! Form::open(['route' => 'admin.report.transaction.index', 'class' => 'form-horizontal', 'files' => true]) !!}
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" placeholder="Start Date" id="startdate" name="startdate"/>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" placehoder="End Date" id="enddate" name="enddate" />
                                            </div>
                                        </div>

                                        <!-- Submit Field -->
                                        <div class="form-group">
                                            <div class="col-sm-12 col-sm-offset-5">
                                                {!! Form::submit('Export', ['class' => 'btn btn-primary btn-publish']) !!}
                                            </div>
                                        </div>
                        
                                        <div class="clearfix"></div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('admin.report.transaction.index')}}">Export Report</a> --}}
                </div>
                <div class="ibox-content">
                    @include('backend.partner.table')
                </div>
            </div>
        </div>
    </div>
@endsection