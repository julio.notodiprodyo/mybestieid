@extends('layouts.frontend')

@section('authentication')
<div class="login-box">
    <div class="notif-body">
        <div class="carda__body pdn--al">
            <i class="fas fa-shield-alt login-icon"></i>
            <p> 
                Well, that's no fun.Fill out your email address, and we’ll send you instructions to reset your password.
            </p>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <div>
                    <div class="form-group label-floating is-empty {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter Your email" required autofocus>
                        
                        @if ($errors->has('email'))
                            <span class="error-form">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div>
                    <button type="submit" class="btn btn-primary btn-raised col-md-12">Send Password Reset Link</button>
                </div>
            </form>
            <br>
        </div>
    </div>
</div>
@endsection
