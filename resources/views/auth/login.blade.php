@extends('layouts.frontend')

@section('authentication')
<div class="login-box">
    <div class="notif-head">
        <span class="notif-title"><i class="fa fa-fw fa-lock"></i> Sign in to your account </span>
    </div>
    <div class="notif-body">
        <div class="carda__body pdn--al">
            {{-- <div class="row ">
                <div class="col-sm-6 col-xs-6 mrg--bs">
                    <a href="#" class="btn  btn-facebook btn-block">
                        <i class="fab fa-facebook-f"></i> Facebook
                    </a>
                </div>
                <div class="col-sm-6 col-xs-6 mrg--bs">
                    <a href="#" class="btn  btn-google btn-block">
                        <i class="fab fa-google"></i> Google
                    </a>
                </div>
                <h4 class="dividr mrg--vh">
                    <b class="dividr__content"><i class="dividr__label">OR</i></b>
                </h4>
            </div> --}}
            <div class="row email-login-section">
                <form method="POST" action="{{ route('login') }}" id="form-login">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="text" class="form-control required" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>
                        @if ($errors->has('email'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control required" name="password" placeholder="Password">
                        {{-- <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span> --}}
                        <span toggle="#password" class="eye-input-form toggle-password"><i class="fa fa-eye"></i></span>
                        @if ($errors->has('password'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group  row mrg--vh">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-block">
                                <i class="fas fa-sign-in-alt"></i> Sign-in
                            </button>
                        </div>
                    </div>
                    <div class="form-group checkbox clearfix">
                        <a href="{{ route('password.request') }}" class="btn btn-link float-right pdn--an-imp">Reset Password</a>
                    </div>
                    <div>
                        <p>Are you new here ? <a href="{{ route('index.member.registration') }}">Sign-up</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
