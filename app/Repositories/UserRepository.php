<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Model\User;
use App\Model\Country;
use App\Model\Province;
use App\Model\City;
use App\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    

    public function create(array $input){

        $input['slug'] = rtrim(str_replace(' ', '-', strtolower($input['name'])),'-');
        $input['password'] = bcrypt($input['password']);
        
        return $this->model->create($input);
    }

    public function update(array $input, $id){
        $input['slug'] = rtrim(str_replace(' ', '-', strtolower($input['name'])),'-');

        if(isset($input['password'])){
            $input['password'] = bcrypt($input['password']);
        }
        
        return parent::update($input, $id);
    }

    public function getCountries(){
        return Country::all()->pluck('country_name', 'id')->toArray();
    }

    public function getProvince(){
        return Province::all()->whereNotIn('id', [1])->pluck('name', 'id')->toArray();
    }

    public function getCities(){
        return City::all()->pluck('name', 'id')->toArray();
    }

    public function getListCreator($blackList = []){
        return User::where('is_active', 1)
                    ->whereHas('roles', function($query){
                        $query->where('name', 'creator');
                    })
                    ->whereNotIn('id', $blackList)
                    ->pluck('name', 'id')
                    ->toArray();
    }
}
