<?php

namespace App\DataTables;

use App\Model\Channels;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ChannelDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('is_active', function($m){
                return $m->is_active? 'Ya' : 'Tidak';
                
            })->addColumn('is_featured', function($m){
                return $m->is_featured? '<div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" checked class="onoffswitch-checkbox featured" id="featured'.$m->id.'" data-id="'.$m->id.'">
                        <label class="onoffswitch-label" for="featured'.$m->id.'">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>' : '<div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" class="onoffswitch-checkbox featured" id="featured'.$m->id.'" data-id="'.$m->id.'">
                        <label class="onoffswitch-label" for="featured'.$m->id.'">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>';
            })->addColumn('is_hd', function($m){
                return $m->is_hd? '<div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" checked class="onoffswitch-checkbox hd" id="hd'.$m->id.'" data-id="'.$m->id.'">
                        <label class="onoffswitch-label" for="hd'.$m->id.'">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>' : '<div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" class="onoffswitch-checkbox hd" id="hd'.$m->id.'" data-id="'.$m->id.'">
                        <label class="onoffswitch-label" for="hd'.$m->id.'">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>';
            })
            ->addColumn('m3u8_url', function($m){
                return $m->attr_3? $m->attr_3 : '';})
            ->addColumn('mpeg_url', function($m){
                return $m->attr_7? $m->attr_7 : '';})
            ->addColumn('order', function($m){
                return $m->order? $m->order : '';})
            ->addColumn('action', 'backend.channel.datatables_actions')
            ->rawColumns(['is_featured', 'is_hd', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Channels $model)
    {   
        return $model->with('parent')->newQuery()->where('parent_id', '!=', NULL)->orderBy('updated_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    [ 
                        "extend" => 'create', 
                        "text" => '<i class="fa fa-plus"></i> Add New',
                        "className" => 'btn-primary',
                    ],
                    [ 
                        "extend" => 'reset', 
                        "text" => '<i class="fa fa-undo"></i> Reset',
                        "className" => 'btn-primary'
                    ],
                    [ 
                        "extend" => 'reload', 
                        "text" => '<i class="fa fa-refresh"></i> Reload',
                        "className" => 'btn-primary'
                    ]
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'parent_id' => [ 'title' => 'Category', 'name' => 'title', 'data' => 'parent.title'],
            'title' => [ 'title' => 'Title'],
            // 'title',
            'm3u8_url',
            'mpeg_url',
            'is_featured',
            'is_hd',
            'order',
            'is_active',
            // 'title' => [ 'title' => 'Category'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'channel_datatable_' . time();
    }
}