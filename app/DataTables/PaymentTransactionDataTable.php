<?php

namespace App\DataTables;

use App\Model\PremiumPackageUser;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class PaymentTransactionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('premium_package', function($m){
                            return $m->premiumPackage->package_name;
                        })
                        ->addColumn('item', function($m){
                            return $m->premiumPackage->content->title;
                        })
                        ->addColumn('order_id', function($m){
                            return $m->paymentTransaction->order_id;
                        })
                        ->addColumn('payment_status', function($m){
                            return strtoupper($m->paymentTransaction->payment_status);
                        })
                        ->addColumn('expired_package', function($m){
                            return $m->end_date;
                        })
                        ->addColumn('action', 'frontend.component.datatables_actions')
                        ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PremiumPackageUser $model)
    {   
        return $model->newQuery()->with('premiumPackage', 'paymentTransaction')->where('user_id', Auth::user()->id);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'bFilter' => false, 
                'bInfo' => false,
                'buttons' => [],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'premium_package',
            'item',
            'order_id',
            'payment_status',
            'expired_package'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'slideshow_datatable_' . time();
    }
}