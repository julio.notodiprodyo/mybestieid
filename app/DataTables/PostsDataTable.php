<?php

namespace App\DataTables;

use App\Model\Posts;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Carbon;
use Auth;

class PostsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        return $dataTable
            ->addcolumn('created', function($m) {
                return with(new Carbon($m->created_at))->format('d-m-Y h:m:s');
            })
            ->addColumn('is_active', function ($m) {
                return $m->is_active ? 'Publish' : 'Draft';
            })
            ->addColumn('is_popular', function ($m) {
                return $m->is_popular ? '<div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" checked class="onoffswitch-checkbox popular" id="example'.$m->id.'" data-id="'.$m->id.'">
                        <label class="onoffswitch-label" for="example'.$m->id.'">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>' : '<div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" class="onoffswitch-checkbox popular" id="example'.$m->id.'" data-id="'.$m->id.'">
                        <label class="onoffswitch-label" for="example'.$m->id.'">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>';
            })
            ->rawColumns(['is_popular', 'action'])
            ->addColumn('action', 'backend.posts.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Posts $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Posts $model)
    {
        return $model->with(['channel','user'])->newQuery()->orderBy('created_at', 'desc');
    }
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->addAction(['width' => '80px'])
        ->parameters([
            'dom'     => 'Bfrtlip',
            'targets' => 'no-sort',
            'bSort' =>  false,
            'order'   => [[0, 'desc']],
            'responsive' => true,
            'autoWidth' => false,
            'buttons' => [
                [ 
                    "extend" => 'create', 
                    "text" => '<i class="fa fa-plus"></i> Add Automatic',
                    "className" => 'btn-primary',
                    'action' => 'function( e, dt, button, config){ 
                        window.location = "posts/create-automatic";
                    }'
                ],
                [ 
                    "extend" => 'create', 
                    "text" => '<i class="fa fa-plus"></i> Add Manual',
                    "className" => 'btn-primary',
                    'action' => 'function( e, dt, button, config){ 
                        window.location = "posts/create-manual";
                    }'
                ],
                [ 
                    "extend" => 'reset', 
                    "text" => '<i class="fa fa-undo"></i> Reset',
                    "className" => 'btn-primary'
                ],
                [ 
                    "extend" => 'reload', 
                    "text" => '<i class="fa fa-refresh"></i> Reload',
                    "className" => 'btn-primary'
                ]
            ],
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'created' => ['created' => 'created_at'],
            'title',
            'is_active' => ['title' => 'Status'],
            'is_popular',
            'type' => ['title' => 'Type']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'posts_datatable' . time();
    }
}
