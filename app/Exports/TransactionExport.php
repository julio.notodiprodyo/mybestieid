<?php
  
namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class TransactionExport implements FromView, WithColumnWidths
{
    public function columnWidths(): array
    {
        return [
            'A' => 7,
            'B' => 11,            
            'C' => 8,            
            'D' => 70,            
            'E' => 20,            
            'F' => 13,            
            'G' => 19,            
            'H' => 13,            
            'I' => 22,            
            'J' => 15,            
            'K' => 15,            
        ];
    }

    public function view(): View
    {
        $query = DB::table('premium_package_user as ppu')
        ->leftJoin('users as u', 'ppu.user_id', '=', 'u.id')
        ->leftJoin('premium_package as pp', 'ppu.premium_package_id', '=', 'pp.id')
        ->leftJoin('contents as c', 'pp.content_id', '=', 'c.id')
        ->leftJoin('payment_transaction as pt', 'ppu.id', '=', 'pt.premium_package_user_id')
        ->where(['c.attr_4' => Auth::user()->id, 'pt.payment_status' => 'paid'])
        ->whereBetween('ppu.created_at', [request()->input('startdate'), request()->input('enddate')]);

        $transaction = $query->select('pt.payment_status', 'pt.created_at', 'pt.payment_type', 'pp.package_name', 'pp.amount', 'pp.vat_payable', 'pp.time_period', 'u.email', 'c.title', 'pt.created_at', 'ppu.end_date', 'pp.amount', 'pp.vat_payable')->get();
        $totalAmount = $query->select(DB::raw("SUM(pp.amount) as amount"))->first();
        $totalVatPayable = $query->select(DB::raw("SUM(pp.vat_payable) as vat_payable"))->first();

        return view('backend.partner.revenue_calculation_excel', [
            'transactions'      => $transaction,
            'totalAmount'       => $totalAmount,
            'totalVatPayable'   => $totalVatPayable
        ]);
    }
}
