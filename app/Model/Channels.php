<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Channels extends Content
{
    protected static function boot()
    {
        parent::boot();

        $type = 'channel';
        $map  = [
            // 'channel' => 'attr_1',
        ];

        static::addGlobalScope('', function (Builder $builder) use ($type) {
            $builder->where('type', $type);
        });

        // self::creating(function ($model) use ($type, $map) {
        //     $model->type = $type;
        //     foreach ($map as $k => $v) {
        //         $model->$v = request()->$k;
        //     }
        // });

        self::retrieved(function ($model) use ($map) {
            foreach ($map as $k => $v) {
                $model->$k = $model->$v;
            }
        });
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }
}
