<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Category extends Content
{
	protected static function boot()
    {
        parent::boot();
        $type = 'category';
        $map  = [

        ];

        static::addGlobalScope('', function (Builder $builder) use ($type) {
            $builder->where('type', $type);
        });

        self::creating(function ($model) use ($type, $map) {
            $model->type        = $type;
            $model->created_by  = Auth::user()->id;
            $model->slug        = rtrim(str_replace(' ', '-', strtolower(request()->title)),'-');
            foreach ($map as $k => $v) {
                $model->$v = request()->$k;
            }
        });

        self::retrieved(function ($model) use ($map) {
            foreach ($map as $k => $v) {
                $model->$k = $model->$v;
            }
        });
    }
}
