<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    //
    protected $table   = 'comments';
    protected $fillable = ['user_id', 'content_id', 'name', 'email', 'title', 'comment','is_read'];

    public function hasContent(){
        return $this->belongsTo(Content::class, 'content_id');
    }

    public function hasUser(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
