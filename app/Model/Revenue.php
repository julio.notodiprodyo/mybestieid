<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
	protected $table = 'revenue';
	protected $fillable = [
		'month', 'year', 'status'
	];

	function creator()
	{
		return $this->belongsToMany(User::class, 'revenue_creator', 'revenue_id', 'creator_id')
			->withPivot('estimate_gross', 'revenue', 'cost_production', 'nett_revenue', 'share_revenue', 'date_payment', 'status_paid');	
	}
}
