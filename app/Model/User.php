<?php

namespace App\Model;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class User extends Authenticatable implements AuditableContract
{
    use Auditable;
    use Notifiable;
    use EntrustUserTrait;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['dob', 'last_sign_in', 'current_sign_in'];
    protected $with = ['roles'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'biodata', 'slug', 'password', 'image', 'cover',
        'provider_id', 'subscribers', 'percentage', 'is_active', 'brand_name', 'phone', 'is_login', 'city',
        'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_login'
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function verifyUser()
    {
        return $this->hasOne('App\Model\VerifyUser');
    }
    
    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function videos()
    {
        return $this->hasMany(Video::class, 'user_id')
                    ->where('is_active', '=', '1');
    }

    public function role()
    {
        return $this->belongsTo('App\Model\Role');
    }

    public function scopeOfRole($query, $roleName)
    {
        return $query->whereHas('roles', function($q) use($roleName){
            $q->where('name', $roleName);
        });
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function socials()
    {
        return $this->hasMany(Socials::class, 'user_id')
        ->limit(4);
    }

    public function bookmarkPost()
    {
        return $this->belongsToMany('App\model\Posts', 'bookmark', 'user_id', 'content_id')->withPivot('user_id', 'content_id');
    }

    public function accessrole(){
        return $this->hasMany(AccessRole::class,'user_id');
    }

    public function roleUser(){
        return $this->hasMany(RoleUser::class,'user_id');
    }

    public function userCity()
    {
        return $this->belongsTo(City::class, 'city');
    }
}
