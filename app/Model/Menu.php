<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Menu extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'menus';
	protected $fillable = ['parent_id', 'position', 'title', 'url', 'is_active', 'order'];

	protected $auditInclude = [
        'title',
        'content',
    ];

    public function parent() {
		return $this->belongsTo(static::class, 'parent_id', 'id');
	}
}
