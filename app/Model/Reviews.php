<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Reviews extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table    = 'reviews';
    protected $fillable = [
		'id', 'content_id', 'comment', 'rating', 'user_id', 'created_at', 'updated_at'
	];

    protected $auditInclude = [
        'title',
        'content',
    ];

    public static function boot() {
        parent::boot();

        self::creating(function($create){
            $create->user_id       = auth()->user()->id; 
        });
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function channels()
    {
        return $this->belongsTo(Channels::class, 'content_id', 'id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function video()
    {
        return $this->belongsTo(Video::class, 'content_id', 'id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function podcasts()
    {
        return $this->belongsTo(Podcast::class, 'content_id', 'id');
    }
}
