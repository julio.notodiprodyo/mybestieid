<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    protected $guarded = [];
    protected $table = 'payment_transaction';
}
