<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Video extends Content
{
    protected static function boot()
    {
        parent::boot();
        $type = 'video';
        $map  = [

        ];

        static::addGlobalScope('', function (Builder $builder) use ($type) {
            $builder->where('type', $type);
        });

        self::creating(function ($model) use ($type, $map) {
            $model->type = $type;
            foreach ($map as $k => $v) {
                $model->$v = request()->$k;
            }
        });

        self::retrieved(function ($model) use ($map) {
            foreach ($map as $k => $v) {
                $model->$k = $model->$v;
            }
        });
    }

    public function premiumPackage()
    {
        return $this->belongsTo(PremiumPackage::class, 'id', 'content_id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/

    public function channelVideo()
    {
        return $this->hasOne(Channel::class, 'attr_1', 'attr_1');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function scopeDateVideo($query)
    {
        $query->whereBetween('created_at', [Carbon::now()->subWeek(4), Carbon::now()]);
    }
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function scopeTwoDateVideo($query2)
    {
        $query2->whereBetween('created_at', [Carbon::now()->subWeek(2), Carbon::now()]);
    }
}
