<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PremiumPackage extends Model
{
    protected $guarded = [];
    protected $table = 'premium_package';

    public function content()
    {
        return $this->belongsTo(Video::class, 'content_id');
    }
}
