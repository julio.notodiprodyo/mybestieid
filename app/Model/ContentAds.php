<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ContentAds extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps  = false;
    protected $fillable = ['ads_id', 'content_id'];
    protected $auditInclude = [
        'title',
        'content',
    ];
}
