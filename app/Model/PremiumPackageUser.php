<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PremiumPackageUser extends Model
{
    protected $guarded = [];
    protected $table = 'premium_package_user';

    public function premiumPackage()
    {
        return $this->belongsTo(PremiumPackage::class, 'premium_package_id');
    }

    public function paymentTransaction()
    {
        return $this->belongsTo(PaymentTransaction::class, 'id', 'premium_package_user_id');
    }
}
