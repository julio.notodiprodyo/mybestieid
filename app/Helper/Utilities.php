<?php
#    Output easy-to-read function helpers
#    by Julio Notodiprodyo

use App\Model\Menu;
use App\Model\Page;
use App\Model\Video;
use App\Model\Content;
use App\Model\Channels;
use App\Model\EReference;
use App\Model\SiteConfig;
use Illuminate\Support\Facades\Auth;

function menu()
{
    return Menu::select('title', 'url', 'icon')->where('is_active', '=', '1')->orderBy('order', 'asc')->get();
}

function page()
{
    return Page::select('title', 'slug', 'attr_3')->where(['is_active' => '1'])->get();
}

function tabTitle($slug)
{
    switch ($slug) {
        case 'channels':
            if(!empty(request()->segment(2))){
                $data = Channels::select('title', 'slug')->where(['slug' => request()->segment(2)])->first();
                !empty($data) ? ($label = $data['title']) : ($label = '');
            }else{
                $label = 'TV Channels';
            }
            break;
        case 'vod':
            if(!empty(request()->segment(2))){
                $data = Video::with('users')->select('title', 'user_id', 'slug')->where(['slug' => request()->segment(2)])->first();
                !empty($data) ? ($label = $data['users']['name']) : ($label = '');
            }else{
                $label = 'VOD';
            }
            break;
        case 'e-reference':
            if(!empty(request()->segment(2))){
                $data = EReference::select('title', 'slug')->where(['slug' => request()->segment(2)])->first();
                !empty($data) ? ($label = $data['title']) : ($label = '');
            }else{
                $label = 'E-Reference';
            }
            break;
        case 'page':
            $data = Page::select('title')->where(['slug' => request()->segment(2)])->first();
            !empty($data) ? ($label = $data['title']) : ($label = '');
            break;
        case 'my-account':
            $label = 'Dashboard Account';
            break;
        case 'subscription':
            $label = 'Subscription';
            break;
        case 'search':
            $label = 'Search : "'.request()->input('key').'"';
            break;
        default:
            $label = 'Home';
            break;
    }

    return $label;
}

function webConfig()
{
    $configs = SiteConfig::get();

    $data = [];
    if (sizeof($configs) > 0) {
        foreach ($configs as $key => $value) {
            $data[$value->key] = $value->value;
        }
    }

    return $data;
}

function metaData($slug) 
{
    if(!empty($slug)){
        $data = Content::select('title', 'content', 'excerpt', 'image')->where(['slug' => $slug])->whereIn('type', ['channel', 'video', 'page'])->first();
        $metaData = [
            'description'       => '<meta property="description" content="'.$data['content'].'">',
            'keywords'          => '<meta property="keywords" content="'.$data['excerpt'].'">',
            'og:type'           => '<meta property="og:type" content="website">',
            'og:title'          => '<meta property="og:title" content="'.$data['title'].'">',
            'og:description'    => '<meta property="og:description" content="'.$data['content'].'">',
            'og:site_name'      => '<meta property="og:site_name" content="'.route('home').'">',
            'og:image'          => '<meta property="og:image" content="'.route('home').''.$data['image'].'">',
            'og:url'            => '<meta property="og:url" content="'.url()->full().'">'
        ];
        !empty($data) ? $meta = $metaData : $meta = defaultMetaData();
    }else{
        $meta = defaultMetaData();
    }

    return $meta;
}

function defaultMetaData()
{
    return [
        'description'       => '<meta property="description" content="'.webConfig()['website_title_about'].'">',
        'keywords'          => '<meta property="keywords" content="'.webConfig()['website_slogan'].'">',
        'og:type'           => '<meta property="og:type" content="website">',
        'og:title'          => '<meta property="og:title" content="'.webConfig()['website_title'].'">',
        'og:description'    => '<meta property="og:description" content="'.webConfig()['website_title_about'].'">',
        'og:site_name'      => '<meta property="og:site_name" content="'.route('home').'">',
        'og:image'          => '<meta property="og:image" content="'.route('home').''.webConfig()['website_logo_header'].'">',
        'og:url'            => '<meta property="og:url" content="'.url()->full().'">'
    ];
}
