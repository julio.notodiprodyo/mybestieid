<?php

namespace App\Mail;

use App\Model\VerifyUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyMemberMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $verify = VerifyUser::where('user_id', $this->user->id)->first();

        return $this->view('email.registerMemberEmail')->with(['user' => $this->user, 'verify' => $verify]);
    }
}