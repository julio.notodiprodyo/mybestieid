<?php

namespace App\Http\Controllers\Backend;

use App\Model\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\DataTables\MenuDataTable;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreOrUpdateMenuRequest;
use Flash;
use App\Model\User;
use App\Model\UserGroup;
use Auth;
use Session;

class AccountManController extends Controller
{
	public function index()
	{	
		if(Auth::user()->email == 'admin@decxtv.id'){
			$datas = UserGroup::all();	
			//return $datas;
			return view('backend.menu.account_management.index',compact('datas'));
		}else{
			Flash::error("Sorry Can't be Access.");
			return redirect(route('admin.dashboard.index'));
		}
	}

	public function DeleteAction($master, $child){
		//return $master;
		if(Auth::user()->email == 'admin@decxtv.id'){
			$delete =  UserGroup::where(['user_master'=>$master, 'user_child'=>$child])->delete();
			if($delete){
				Session::forget('user_group');
				$UserGroup = UserGroup::where(['user_master'=>Auth::user()->id])->get();
				session(['user_group'=>$UserGroup]);
				Flash::success('Delete Account successfully.');
				return redirect(route('account_management.index'));			 
			}else{
				Flash::error('Delete Account Failed.');
				return redirect(route('account_management.index'));
			}        	
		}else{
			 Flash::error('Sorry Cant Access.');
        	return redirect(route('account_management.index'));
		}
	}
}