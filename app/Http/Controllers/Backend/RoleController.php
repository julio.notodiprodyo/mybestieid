<?php

namespace App\Http\Controllers\Backend;

use Flash;
use App\DataTables\RoleDataTable;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use App\Http\Controllers\Backend\Controller;
use App\Http\Requests\StoreOrUpdateRoleRequest;

class RoleController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $roles;
    protected $permissions;

    /**
     * Create a new controller instance.
     *
     * @param  RoleRepository  $roles
     * @return void
     */
    public function __construct(RoleRepository $roles, PermissionRepository $permissions)
    {
        $this->roles = $roles;
        $this->permissions = $permissions;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RoleDataTable $RoleDatatable)
    {
        return $RoleDatatable->render('backend.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listPermission = $this->permissions->getList();

        return view('backend.role.create', ['permissions' => $listPermission]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateRoleRequest $request)
    {
        $input = $request->except('_token');
        
        $role = $this->roles->create($input);

        if($request->has('permissions')){
            $role->permissions()->sync($request->input('permissions'));
        }

        Flash::success('Role saved successfully.');

        return redirect(route('role.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->roles->find($id);

        if (empty($role)) {
            Flash::error('Role not found');

            return redirect(route('role.index'));
        }
        $listPermission = $this->permissions->getList();
        
        return view('backend.role.edit', [
            'role' => $role,
            'permissions' => $listPermission
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOrUpdateRoleRequest $request, $id)
    {
        $role = $this->roles->find($id);
        
        if (empty($role)) {
            Flash::error('role not found');

            return redirect(route('role.index'));
        }
        
        $input = $request->except(['_method', '_token']);
        $role = $this->roles->update($input, $id);
        
        if($request->has('permissions')){
            $role->permissions()->sync($request->input('permissions'));
        }

        Flash::success('user updated successfully.');

        return redirect(route('role.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
