<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Model\Channel;
use App\Model\Posts;
use App\Model\Series;
use App\Model\User;
use App\Model\Video;
use Flash;

class ErrorHandlerController extends Controller
{
	public function errorCode404()
    {
    	return view('frontend.component.404');
    }
}
