<?php

namespace App\Http\Controllers\Backend;

use Flash;
use App\Model\Ads;
use App\Model\Terms;
use App\Model\Channels;
use App\Model\Category;
use App\Model\ContentAds;
use Illuminate\Http\Request;
use App\Model\ContentsTerms;
use App\DataTables\ChannelDataTable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Backend\Controller;
use App\Http\Requests\StoreOrUpdateChannelRequest;

class ChannelsController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(ChannelDataTable $channelDataTable)
    {   
        return $channelDataTable->render('backend.channel.index');
    }

    public function create()
    {
        $ads        = Ads::where('status', '=', '1')->get();
        $category   = Category::where(['is_active' => 1])->get();
        $tags       = Terms::all()->pluck('name')->toArray();

        return view('backend.channel.create', ['terms' => $tags, 'ads' => $ads, 'category' => $category]);
    }

    public function store(StoreOrUpdateChannelRequest $request) {
        $input['user_id']       = Auth::id();
        $input['type']          = $request->type;
        $input['title']         = $request->title;
        $input['parent_id']     = $request->category_id;
        $input['post_type']     = $request->post_type;
        $input['attr_4']        = $request->attr_4;
        $input['order']         = $request->order;
        $input['slug']          = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');
        $input['content']       = strip_tags($request->content);

        if($request->post_type == 'm3u8'){
            $input['attr_7']          = null;
            $input['attr_3']          = $request->attr_3;
        }else{
            $input['attr_7']          = $request->attr_7;
            $input['attr_3']          = null;
        }
        
        if ($request->image != NULL) {
            $input['image'] = '/uploads/channel/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/channel'), time().'.'.$request->image->getClientOriginalExtension());
        }
        $input['is_active'] = 1;
        
        $id_posts = Channels::create($input)->id;
        
        // save terms
        $tags = explode(',', $request->tags);
        foreach($tags as $value){
            $input_terms['name'] = $value;
            $input_terms['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($value))),'-');
            $input_terms['type'] = 'tag';

            $terms = Terms::where('name', '=', $value)->first();
            if ($terms === null) {
                $id_terms = Terms::create($input_terms)->id;
                $input_content_terms['content_id'] = $id_posts;
                $input_content_terms['term_id'] = $id_terms;
                ContentsTerms::create($input_content_terms);
            } else {
                $input_content_terms['content_id'] = $id_posts;
                $input_content_terms['term_id'] = $terms->id;
                ContentsTerms::create($input_content_terms);
            }
        }
        // end save terms
        
        if(!empty($request->ads_id)){
            ContentAds::create(['ads_id' => $request->ads_id, 'content_id' => $id_posts]);
        }

        Flash::success('Channel saved successfully.');

        return redirect(route('channel.index'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function show($id)
    {
    	$channel = Channels::find($id);

        if (empty($channel)) {
            Flash::error('Channel not found');

            return redirect(route('channel.index'));
        }

        return view('backend.channel.show')->with('channel', $channel);
    }

    function edit($id)
    {
        $channel = Channels::find($id);

        if (empty($channel)) {
            Flash::error('Channel not found');

            return redirect(route('channel.index'));
        }
        
        $tags = Terms::all()->pluck('name')->toArray();
        $content_terms = ContentsTerms::where('content_id', $id)->get();
        $terms = array();
        foreach ($content_terms as $value){
            $term = Terms::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        $channel['tags'] = implode(',', $terms);

        $ads = Ads::where('status', '=', '1')->get();
        $category   = Category::where(['is_active' => 1])->get();
        $contentAds = ContentAds::where('content_id', '=', $id)->first();

        return view('backend.channel.edit')->with(['channel' => $channel, 'terms' => $tags, 'ads' => $ads, 'contentAds' => $contentAds, 'category' => $category]);
    }

    public function update($id, StoreOrUpdateChannelRequest $request) {
        $channel                    = Channels::find($id);
        $channel['user_id']         = Auth::id();
        $channel['type']            = $request->type;
        $channel['title']           = $request->title;
        $channel['parent_id']       = $request->category_id;
        $channel['post_type']       = $request->post_type;
        $channel['attr_4']          = $request->attr_4;
        $channel['order']           = $request->order;
        $channel['content']         = strip_tags($request->content);
        $channel['slug']            = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');

        if($request->post_type == 'm3u8'){
            $channel['attr_7']          = null;
            $channel['attr_3']          = $request->attr_3;
        }else{
            $channel['attr_7']          = $request->attr_7;
            $channel['attr_3']          = null;
        }

        // save terms
        $tags = explode(',', $request->tags);
        ContentsTerms::where('content_id', $id)->delete();
        foreach($tags as $value){
            $input_terms['name'] = $value;
            $input_terms['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $value))))), '-'));
            $input_terms['type'] = 'tag';

            $terms = Terms::where('name', '=', $value)->first();
            if ($terms == null) {
                $id_terms                           = Terms::create($input_terms)->id;
                $input_content_terms['content_id']  = $id;
                $input_content_terms['term_id']     = $id_terms;
                ContentsTerms::create($input_content_terms);
            } else {
                $input_content_terms['content_id']  = $id;
                $input_content_terms['term_id']     = $terms->id;
                ContentsTerms::create($input_content_terms);
            }
        }
        
        // end save terms
        if(!empty($request->ads_id)){
            ContentAds::updateOrCreate(['content_id' => $id],['ads_id' => $request->ads_id]);
        }else{
            ContentAds::where('content_id', $id)->delete();
        }

        if ($request->image != NULL) {
            $channel['image'] = '/uploads/channel/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/channel'), time().'.'.$request->image->getClientOriginalExtension());
        }

        $channel->save();
        
        Flash::success('Channel update successfully.');

        return redirect(route('channel.index'));
    }

    public function updateFeatured(Request $request) 
    {   
        $posts = Channels::find($request->id);

        switch($request->flag){
            case 'featured':
                if($posts->is_featured == 0){
                    $posts['is_featured'] = 1;
                }else{
                    $posts['is_featured'] = 0;
                }
                break;
            default:
                if($posts->is_hd == 0){
                    $posts['is_hd'] = 1;
                }else{
                    $posts['is_hd'] = 0;
                }
                break;
        }

        $posts->save();
        
        return $posts;
    }

    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $channel = Channels::find($id);

            if (empty($channel)) {
                Flash::error('Channel not found');

                return redirect(route('channel.index'));
            }
            $channel['is_active'] = 1;

            $channel->save();

            Flash::success('Channel activated successfully.');
        }else{
            $channel = Channels::find($id);

            if (empty($channel)) {
                Flash::error('Channel not found');

                return redirect(route('channel.index'));
            }
            $channel['is_active'] = 0;
            
            $channel->save();

            Flash::success('Channel inactivated successfully.');
        }

        

        return redirect(route('channel.index'));
    }

    function channeltable(){
        return view('backend.channel.table');
    }
}
