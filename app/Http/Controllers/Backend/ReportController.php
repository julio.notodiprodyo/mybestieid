<?php

namespace App\Http\Controllers\Backend;

use App\Exports\TransactionExport;
use App\DataTables\ReportDataTable;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Backend\Controller;

class ReportController extends Controller
{
    public function partner(ReportDataTable $reportDataTable)
    {
        return $reportDataTable->render('backend.partner.index');
    }

    public function reportTransaction()
    {
        return Excel::download(new TransactionExport, 'DECXTV - VOD Reporting Client.xlsx', \Maatwebsite\Excel\Excel::XLSX, [
            'Content-Type' => 'application/xlsx',
        ]);
    }
}
