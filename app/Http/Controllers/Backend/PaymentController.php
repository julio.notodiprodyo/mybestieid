<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\User;
use Flash;
use DateTime;
use Auth;

class PaymentController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {
        $user = User::find(Auth::id());

        if (empty($user)) {
            Flash::error('Payment not found');

            return redirect(route('payment.index'));
        }
        return view('payment.index')->with('user', $user);
    }

    // public function create()
    // {
    //     return view('config.create');
    // }

    // public function store(Request $request) {
    //     $input['key'] = $request->key;
    //     $input['value'] = $request->value;

    //     SiteConfig::create($input);
    //     Flash::success('Config saved successfully.');

    //     return redirect(route('config.index'));
    // }

    // /**
    //  * undocumented function
    //  *
    //  * @return void
    //  * @author 
    //  **/
    // function show($id)
    // {
    // 	$config = SiteConfig::find($id);

    //     if (empty($config)) {
    //         Flash::error('Config not found');

    //         return redirect(route('config.index'));
    //     }

    //     return view('config.show')->with(['config'=> $config]);
    // }

    function edit($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('Payment not found');

            return redirect(route('payment.index'));
        }
        return view('payment.edit')->with('user', $user);
    }

    public function update($id, Request $request) {
        // $set = User::find($id);
        // $set['name'] = $request->name;
        // $set['gender'] = $request->gender;
        // $set['email'] = $request->email;
        // $set['dob'] = date("Y-m-d", strtotime($request->dob));
        // $set['phone'] = $request->phone;
        // $set['country'] = $request->country;
        // $set['city'] = $request->city;
        // $set['ktp'] = $request->ktp;
        // $set['biodata'] = $request->biodata;
        // if ($request->image != NULL) {
        //     $set['image'] = '/uploads/creators/image/'.time().'.'.$request->image->getClientOriginalExtension();
        //     $request->image->move(public_path('uploads/creators/image/'), time().'.'.$request->image->getClientOriginalExtension());
        // }
        // if ($request->cover != NULL) {
        //     $set['cover'] = '/uploads/creators/image/'.time().'.'.$request->image->getClientOriginalExtension();
        //     $request->cover->move(public_path('uploads/creators/image/'), time().'.'.$request->image->getClientOriginalExtension());
        // }
        
        // $set->save();

        // Flash::success('Setting update successfully.');

        return redirect(route('personal.index'));
    }
}
