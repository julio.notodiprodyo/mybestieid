<?php

namespace App\Http\Controllers\Backend;

use Flash;
use App\Model\EReference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DataTables\EReferenceDataTable;
use App\Http\Controllers\Backend\Controller;
use App\Http\Requests\StoreOrUpdateEReferenceRequest;

class EReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EReferenceDataTable $eReferenceDataTable)
    {
        return $eReferenceDataTable->render('backend.ereference.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.ereference.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateEReferenceRequest $request)
    {
        $input['user_id']       = Auth::id();
        $input['title']         = $request->title;
        $input['slug']          = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');
        $input['content']       = strip_tags($request->content);
        if ($request->image != NULL) {
            $input['image'] = '/uploads/ereference/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/ereference'), time().'.'.$request->image->getClientOriginalExtension());
        }
        if ($request->pdf != NULL) {
            $input['attr_3'] = '/uploads/ereference/'.time().'.'.$request->pdf->getClientOriginalExtension();
            $request->pdf->move(public_path('uploads/ereference'), time().'.'.$request->pdf->getClientOriginalExtension());
        }
        $input['is_active'] = 1;

        EReference::create($input);

        Flash::success('E-Reference saved successfully.');

        return redirect(route('ereference.index'));
    }

    function edit($id)
    {
        $ereference = EReference::find($id);

        if (empty($ereference)) {
            Flash::error('E-Reference not found');

            return redirect(route('ereference.index'));
        }

        return view('backend.ereference.edit')->with(['ereference' => $ereference]);
    }

    public function update($id, StoreOrUpdateEReferenceRequest $request) {
        $eReference             = EReference::find($id);
        $eReference['user_id']  = Auth::id();
        $eReference['title']    = $request->title;
        $eReference['slug']     = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');
        $eReference['content']  = strip_tags($request->content);
        if ($request->image != NULL) {
            $eReference['image'] = '/uploads/ereference/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/ereference'), time().'.'.$request->image->getClientOriginalExtension());
        }
        if ($request->pdf != NULL) {
            $eReference['attr_3'] = '/uploads/ereference/'.time().'.'.$request->pdf->getClientOriginalExtension();
            $request->pdf->move(public_path('uploads/ereference'), time().'.'.$request->pdf->getClientOriginalExtension());
        }

        $eReference->save();
        
        Flash::success('E-Reference update successfully.');

        return redirect(route('ereference.index'));
    }

    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $ereference = EReference::find($id);

            if (empty($ereference)) {
                Flash::error('E-Reference not found');

                return redirect(route('ereference.index'));
            }
            $ereference['is_active'] = 1;
            $ereference->save();

            Flash::success('E-Reference activated successfully.');
        }elseif ($request->action == 'inact') {
            $ereference = EReference::find($id);

            if (empty($ereference)) {
                Flash::error('E-Reference not found');

                return redirect(route('ereference.index'));
            }
            $ereference['is_active'] = 0;
            $ereference->save();

            Flash::success('E-Reference Inactive successfully.');
        }elseif ($request->action == 'del'){
            $ereference = EReference::find($id);

            if (empty($ereference)) {
                Flash::error('E-Reference not found');

                return redirect(route('ereference.index'));
            }
            $ereference->delete();

            Flash::success('E-Reference Delete successfully.');
        }
        

        return redirect(route('ereference.index'));
    }
}
