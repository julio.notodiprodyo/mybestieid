<?php

namespace App\Http\Controllers\Backend;

use Flash;
use Storage;
use App\Model\User;
use App\Model\Role;
use App\Model\RoleUser;
use App\Model\Channels;
use App\Model\AccessRole;
use Illuminate\Http\Request;
use App\DataTables\UserDataTable;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Controllers\Backend\Controller;

use Auth;

class UserController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserDataTable $userDatatable)
    {
        return $userDatatable->render('backend.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rolesObj = $this->roles->all();
        $listRole = Role::where('id', '!=', '2')->get();

        
        return view('backend.user.create', [
            'roles' => $listRole,
            'rolesObj' => $rolesObj,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $input = $request->all();

        if(!empty($request->image)){
            $image = $request->image->store('uploads/creators/image', 'local_public');
            $input['image'] = $image;
        }
        

        $roles = $request->input('roles');
        
        $user = $this->users->create($input);
        if($request->has('roles')){
            for ($i=0; $i < count($roles) ; $i++) { 
                RoleUser::insert(['user_id' => $user->id, 'role_id' => $roles[$i] ]);
            }            
        }

        Flash::success('User saved successfully.');

        return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->users->find($id);
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('user.index'));
        }

        $getRole = AccessRole::where(['user_id'=>$user->id])
                    ->join('roles', 'access_role.role_id', '=', 'roles.id')
                    ->get();

        $accessRole = AccessRole::where(['user_id'=>$user->id])->get();
        $listRole = Role::where('id', '!=', '2')->get();

        return view('backend.user.edit', [
            'user'  => $user,
            'roles' => $listRole,
            'access_role' => $accessRole,
            'get_role' => $getRole
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->users->find($id);
        $roles = $request->input('roles');

        if (empty($user)) {
            Flash::error('user not found');

            return redirect(route('user.index'));
        }

        $input = $request->except(['_method', '_token']);
        
        if($request->has('image')){
            $result = Storage::disk('local_public')->delete($user->image);
            $image = $request->image->store('uploads/creators/image', 'local_public');
            $input['image'] = $image;
        }

        if($request->has('roles')){
            AccessRole::where(['user_id'=>$user->id])->delete();

            for ($i=0; $i < count($roles) ; $i++) { 
                AccessRole::insert(['user_id' => $user->id, 'role_id' => $roles[$i] ]);
            }
            
            //$user->roles()->sync($request->input('roles'));
        }
        
        $user = $this->users->update($input, $id);

        Flash::success('user updated successfully.');

        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_active'] = 1;

            $creator->save();

            Flash::success('Creator activated successfully.');
        }else if ($request->action == 'inact'){
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_active'] = 0;
            $channel = Channels::where('user_id', $creator->id)->get();
            foreach ($channel as $seri) {
                $ser = Channels::find($seri->id);
                $ser['is_active'] = 0;
                $ser->save();
            }
            $creator->save();

            Flash::success('Creator inactivated successfully.');
        }else if ($request->action == 'showact') {
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_show'] = 1;

            $creator->save();

            Flash::success('Creator show successfully.');
        }else if ($request->action == 'hideact') {
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_show'] = 0;

            $creator->save();

            Flash::success('Creator hide successfully.');
        }else{
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_active'] = 1;

            $creator->delete();

            Flash::success('Creator Delete successfully.');
        }

        return redirect(route('creator.index'));
    }
}
