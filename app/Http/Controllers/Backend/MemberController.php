<?php

namespace App\Http\Controllers\Backend;

use Flash;
use App\Model\CategoryInst;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Backend\Controller;
use App\DataTables\PaymentTransactionDataTable;

class MemberController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    public function upgrade($id)
    {
        $user = $this->users->find($id);
        
        return view('member.upgrade', ['user' => $user]);
    }

    public function edit()
    {
        $user = $this->users->find(Auth::user()->id);
        return view('frontend.member-area.my-account.profile', [
            'user'        => $user
        ]);
    }

    public function update(Request $request)
    {   
        $userInput = $request->only([
            'name',
            'phone',
            'password',
            'category',
            'email',
            'gender'
        ]);
        
        if(!empty($userInput)){
            $user = $this->users->update($userInput, Auth::user()->id);
        }

        alert()->success('Member updated successfully.', 'Success!')->persistent('Close');
        return redirect(route('self.member.edit'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function listTransaction(PaymentTransactionDataTable $paymentTransactionDataTable)
    {   
        return $paymentTransactionDataTable->render('frontend.member-area.my-account.transaction');
    }
}
