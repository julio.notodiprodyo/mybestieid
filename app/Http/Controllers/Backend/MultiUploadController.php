<?php

namespace App\Http\Controllers\Backend;

use Flash;
use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Facades\Storage;
use App\Repositories\MultiUploadRepository;
use App\Http\Controllers\Backend\Controller;
use App\Http\Requests\UpdateMultiUploadRequest;

class MultiUploadController extends Controller
{
    protected $multiUploadRepository;
    
    public function __construct(MultiUploadRepository $multiUploadRepository)
    {
        $this->multiUploadRepository = $multiUploadRepository;
    }

    public function index()
    {   
        return view('backend.channel.create');
    }

    public function create(Request $request)
    {
        return view('multi_upload.create', [
                'platforms' => array_combine($request->platforms, $request->platforms),
                'video'     => $request->video,
                'title'     => $request->title
        ]);
    }

    public function getUpload()
    {
        return view('backend.multi_upload.upload');
    }

    public function postUpload(UploadRequest $request)
    {
        $input = $request->except('_token');
        $video = $request->video->store('uploads/videos', 'local_public');
        $filename = $request->video->getClientOriginalName();
        $title = substr($filename, 0, strrpos($filename, "."));
        
        // return view('multi_upload.create', [
        //     'platforms' => array_combine($input['platforms'], $input['platforms']),
        //     'video'     => $video,
        //     'title'     => $title
        // ]);

        return response()->json([
            'platforms' => $input['platforms'],
            'video'     => $video,
            'title'     => $title
        ]);
    }

    public function store(UpdateMultiUploadRequest $request)
    {
        //prepare data input
        $input = $request->except('_token', 'thumbnail');
        
        if($request->has('thumbnail')){
            $thumbnail = $request->thumbnail->store('uploads/videos', 'local_public');
            $input['thumbnail'] = $thumbnail;
        }
        
        $input['user_id'] = auth()->user()->id;
        
        //posting multiple platform
        foreach($input['platforms'] as $key => $platform){
            $function = $platform.'Post';

            try {
                $this->multiUploadRepository->{$function}($input);
                Flash::success('Upload video to '.$platform.' successfully');
            } catch (\Exception $e) {
                Flash::error('Upload video to '.$platform.' failed');
            }
            
        }

        //clean storage temp
        Storage::disk('local_public')->delete($input['video']);
        if($request->has('thumbnail')){
            Storage::disk('local_public')->delete($input['thumbnail']);
        }

        return redirect()->route('creator.multiUpload.getUpload');
    }
}
