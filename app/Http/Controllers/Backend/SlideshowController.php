<?php

namespace App\Http\Controllers\Backend;

use Flash;
use Storage;
use App\Model\Slideshow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DataTables\SlideshowDataTable;
use App\Http\Controllers\Backend\Controller;
use App\Http\Requests\StoreOrUpdateSlideshowRequest;

class SlideshowController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(SlideshowDataTable $slideshowDataTable)
    {   
        return $slideshowDataTable->render('backend.slideshow.index');
    }

    public function create()
    {
        return view('backend.slideshow.create');
    }

    public function store(StoreOrUpdateSlideshowRequest $request)
    {
        $input = $request->except('_token');

        $input['user_id'] = Auth::id();
        $input['title'] = $request->title;
        $input['attr_3'] = $request->attr_3;
        $input['content'] = strip_tags($request->content);
        $input['slug'] = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');
        if($request->has('image')){
            $path = '/uploads/slideshow/image/'.str_random(32).'.'.$request->image->getClientOriginalExtension();
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;
        }
        $input['is_active'] = 1;

        Slideshow::create($input);
        Flash::success('Slideshow saved successfully.');

        return redirect(route('slideshow.index'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function show($id)
    {
    	$slideshow = Slideshow::find($id);

        if (empty($slideshow)) {
            Flash::error('slideshow not found');

            return redirect(route('slideshow.index'));
        }

        return view('backend.slideshow.show')->with('slideshow', $slideshow);
    }

    public function edit($id)
    {
        $slideshow = Slideshow::find($id);

        if (empty($slideshow)) {
            Flash::error('slideshow not found');

            return redirect(route('slideshow.index'));
        }

        return view('backend.slideshow.edit')->with('slideshow', $slideshow);
    }

    public function update($id, StoreOrUpdateSlideshowRequest $request)
    {
        $slideshow = Slideshow::find($id);
        $slideshow['user_id'] = Auth::id();
        $slideshow['title'] = $request->title;
        $slideshow['attr_3'] = $request->attr_3;
        $slideshow['content'] = strip_tags($request->content);
        $slideshow['slug'] = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');

        if ($request->image != NULL) {
            $slideshow['image'] = '/uploads/slideshow/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/slideshow'), time().'.'.$request->image->getClientOriginalExtension());
        }
        $slideshow->save();

        Flash::success('Slideshow update successfully.');

        return redirect(route('slideshow.index'));
    }

    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $slideshow = Slideshow::find($id);

            if (empty($slideshow)) {
                Flash::error('Slideshow not found');

                return redirect(route('slideshow.index'));
            }
            $slideshow['is_active'] = 1;

            $slideshow->save();

            Flash::success('Slideshow activated successfully.');
        }elseif ($request->action == 'inact') {
            $slideshow = Slideshow::find($id);

            if (empty($slideshow)) {
                Flash::error('Slideshow not found');

                return redirect(route('slideshow.index'));
            }
            $slideshow['is_active'] = 0;

            $slideshow->save();

            Flash::success('Slideshow Inactive successfully.');
        }elseif ($request->action == 'del'){
            $slideshow = Slideshow::find($id);

            if (empty($slideshow)) {
                Flash::error('Slideshow not found');

                return redirect(route('slideshow.index'));
            }

            $slideshow->delete();

            Flash::success('Slideshow Delete successfully.');
        }

        return redirect(route('slideshow.index'));
    }

    function slideshowtable(){
        return view('backend.slideshow.table');
    }
}
