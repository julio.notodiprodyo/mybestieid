<?php

namespace App\Http\Controllers\Backend;

use Flash;
use Auth;
use App\Model\User;
use App\Model\Posts;
use App\Model\Terms;
use App\Model\Series;
use App\Model\Channels;
use App\Model\ContentUsers;
use App\Model\ContentsTerms;
use Illuminate\Http\Request;
use Alaouy\Youtube\Facades\Youtube;
use App\DataTables\PostsDataTable;
use App\Http\Controllers\Backend\Controller;
use App\Http\Requests\StoreOrUpdatePostRequest;
use App\Model\PremiumPackage;

class PostsController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(PostsDataTable $postsDataTable)
    {
        return $postsDataTable->render('backend.posts.index');
    }

    public function createManual()
    {        
        $user = User::where('is_active', 1)
        ->with('roleUser')
        ->whereHas('roleUser', function($query){
            $query->where('role_id', '6');
        })
        ->get()
        ->pluck('name', 'id')
        ->toArray();
        $tags = Terms::all()->pluck('name')->toArray();
        $channel = Channels::where('is_active', 1)->get()->pluck('title', 'attr_1')->toArray();

        return view('backend.posts.create_manual', ['user' => $user, 'channel' => $channel, 'terms' => $tags]);
    }

    public function createAutomatic()
    {
        $user = User::where('is_active', 1)
        ->with('roleUser')
        ->whereHas('roleUser', function($query){
            $query->where('role_id', '6');
        })
        ->get()
        ->pluck('name', 'id')
        ->toArray();

        $selected_user = array();
        $tags = Terms::all()->pluck('name')->toArray();

        return view('backend.posts.create_automatic', ['user' => $user, 'terms' => $tags, 'selected_user' => $selected_user]);
    }

    public function store(StoreOrUpdatePostRequest $request) {
        if($request->postType == 'automatic'){
            $input['title'] = $request->title;
            $input['subtitle'] = 'Subtitle '.$request->title;
            $input['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $request->title))))), '-'));
            $input['excerpt'] = 'Excerpt '.$request->title;
            $input['content'] = $request->content;
            $input['created_by'] = Auth::user()->id;
            $input['attr_3'] = $request->attr_3;
            $input['attr_4'] = $request->attr_4;
            $input['attr_5'] = 'youtube';
            $input['attr_6'] = $request->attr_6;
            $input['type'] = $request->category;
            $input['is_active'] = $request->is_active;
            $input['is_popular'] = 0;
            $input['post_type'] = $request->postType;
            if($request->image != NULL){
                $input['image'] = '/uploads/content/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/content'), time().'.'.$request->image->getClientOriginalExtension());
            }
            $id_posts = Posts::create($input)->id;

            // premium package
            if (sizeof($request->input('addmore')) > 0) {
                foreach ($request->input('addmore') as $key => $value) {
                    $premiumPackage                 = new PremiumPackage;
                    $premiumPackage->content_id     = $id_posts;
                    $premiumPackage->package_name   = $value['package_name'];
                    $premiumPackage->amount         = $value['amount'];
                    $premiumPackage->vat_payable    = $value['vat_payable'];
                    $premiumPackage->time_period    = $value['time_period'];
                    $premiumPackage->is_active      = 1;
                    $premiumPackage->save();
                }
            }
            // end premium package

            // save terms
            $tags = explode(',', $request->tags);
            foreach($tags as $value){
                $input_terms['name'] = $value;
                $input_terms['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($value))),'-');
                $input_terms['type'] = 'tag';

                $terms = Terms::where('name', '=', $value)->first();
                if ($terms === null) {
                    $id_terms = Terms::create($input_terms)->id;
                    $input_content_terms['content_id'] = $id_posts;
                    $input_content_terms['term_id'] = $id_terms;
                    ContentsTerms::create($input_content_terms);
                } else {
                    $input_content_terms['content_id'] = $id_posts;
                    $input_content_terms['term_id'] = $terms->id;
                    ContentsTerms::create($input_content_terms);
                }
            }
            // end save terms
            Flash::success('Content saved successfully.');
        } else if($request->postType == 'manual'){
            $input['type'] = $request->category;
            $input['title'] = $request->title;
            $input['subtitle'] = 'Subtitle '.$request->title;
            $input['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($request->title))),'-');
            $input['excerpt'] = 'Excerpt '.$request->title;
            // $input['attr_1'] = $request->attr_1;
            $input['content'] = $request->content;
            $input['created_by'] = Auth::user()->id;
            $input['is_active'] = $request->is_active;
            $input['attr_3'] = $request->attr_3;
            $input['attr_4'] = $request->attr_4;
            $input['attr_5'] = 'youtube';
            $input['attr_6'] = $request->attr_6;
            $input['is_popular'] = 0;
            $input['post_type'] = $request->postType;
            if($request->image != NULL){
                $input['image'] = '/uploads/content/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/content'), time().'.'.$request->image->getClientOriginalExtension());
            }

            $id_posts = Posts::create($input)->id;

            // premium package
            if (sizeof($request->input('addmore')) > 0) {
                foreach ($request->input('addmore') as $key => $value) {
                    $premiumPackage                 = new PremiumPackage;
                    $premiumPackage->content_id     = $id_posts;
                    $premiumPackage->package_name   = $value['package_name'];
                    $premiumPackage->amount         = $value['amount'];
                    $premiumPackage->vat_payable    = $value['vat_payable'];
                    $premiumPackage->time_period    = $value['time_period'];
                    $premiumPackage->is_active      = 1;
                    $premiumPackage->save();
                }
            }
            // end premium package

            // save terms
            $tags = explode(',', $request->tags);
            foreach($tags as $value){
                $input_terms['name'] = $value;
                $input_terms['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($value))),'-');
                $input_terms['type'] = 'tag';

                $terms = Terms::where('name', '=', $value)->first();
                if ($terms === null) {
                    $id_terms = Terms::create($input_terms)->id;
                    $input_content_terms['content_id'] = $id_posts;
                    $input_content_terms['term_id'] = $id_terms;
                    ContentsTerms::create($input_content_terms);
                } else {
                    $input_content_terms['content_id'] = $id_posts;
                    $input_content_terms['term_id'] = $terms->id;
                    ContentsTerms::create($input_content_terms);
                }
            }
            // end save terms

            Flash::success('Post Manual saved successfully.');
        }
        
        if ($request->create == 1){
            if($request->postType == 'automatic'){
                return redirect(route('admin.posts.createAutomatic'));
            } else {
                return redirect(route('admin.posts.createManual'));
            }          
        } else {
            return redirect(route('admin.posts.index'));
        }
    }

    public function editAutomatic($id)
    {
        $posts = Posts::find($id);
        $user = User::where('is_active', 1)
                ->whereHas('roles', function($query){
                    $query->where('name', 'partner');
                })
                ->get()
                ->pluck('name', 'id')
                ->toArray();
        $tags = Terms::all()->pluck('name')->toArray();
        $selected_user = ContentUsers::where('content_id', $id)->get()->pluck('user_id')->toArray();
        $content_terms = ContentsTerms::where('content_id', $id)->get();
        $terms = array();
        foreach ($content_terms as $value){
            $term = Terms::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        $posts['tags'] = implode(',', $terms);

        if (empty($posts)) {
            Flash::error('Posts not found');

            return redirect(route('admin.posts.index'));
        }

        return view('backend.posts.edit_automatic')->with(['posts'=> $posts, 'user'=> $user, 'terms' => $tags, 'selected_user' => $selected_user]);
    }

    public function editManual($id)
    {
        $posts = Posts::find($id);
        $tags = Terms::all()->pluck('name')->toArray();

        $content_terms = ContentsTerms::where('content_id', $id)->get();
        $terms = array();
        foreach ($content_terms as $value){
            $term = Terms::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        $posts['tags'] = implode(',', $terms);

        $channel = Channels::where('is_active', 1)->get()->pluck('title', 'attr_1')->toArray();
        if (empty($posts)) {
            Flash::error('Posts not found');

            return redirect(route('admin.posts.index'));
        }

        return view('backend.posts.edit_manual')->with(['posts'=> $posts, 'channel'=> $channel, 'terms' => $tags]);
    }

    public function update($id, StoreOrUpdatePostRequest $request) {
        if($request->postType == 'automatic'){
            $posts = Posts::find($id);
            $posts['title'] = $request->title;
            $posts['subtitle'] = 'Subtitle '.$request->title;
            $posts['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $request->title))))), '-'));
            $posts['excerpt'] = 'Excerpt '.$request->title;
            $posts['content'] = $request->content;
            $posts['created_by'] = Auth::user()->id;
            $posts['attr_5'] = 'youtube';
            $posts['attr_6'] = $request->attr_6;
            $posts['type'] = $request->category;
            $posts['is_active'] = $request->is_active;
            $posts['is_popular'] = 0;
            $posts['post_type'] = $request->postType;

            if($request->image != NULL){
                $posts['image'] = '/uploads/content/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/content'), time().'.'.$request->image->getClientOriginalExtension());
            }

            $posts->save();

            // delete users first
            $content_users = ContentUsers::where('content_id', $id);
            $content_users->delete();

            // save terms
            $tags = explode(',', $request->tags);

            ContentsTerms::where('content_id', $id)->delete();
            foreach($tags as $value){
                $input_terms['name'] = $value;
                $input_terms['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $value))))), '-'));
                $input_terms['type'] = 'tag';

                $terms = Terms::where('name', '=', $value)->first();
                if ($terms == null) {
                    $id_terms = Terms::create($input_terms)->id;
                    $input_content_terms['content_id'] = $id;
                    $input_content_terms['term_id'] = $id_terms;
                    ContentsTerms::create($input_content_terms);
                } else {
                    $input_content_terms['content_id'] = $id;
                    $input_content_terms['term_id'] = $terms->id;
                    ContentsTerms::create($input_content_terms);
                }

            }
            
            // end save terms

            Flash::success('Posts update successfully.');            
        }else {
            $posts = Posts::find($id);
            $posts['type'] = $request->category;
            $posts['title'] = $request->title;
            $posts['subtitle'] = 'Subtitle '.$request->title;
            $posts['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $request->title))))), '-'));
            $posts['excerpt'] = 'Excerpt '.$request->title;
            $posts['content'] = $request->content;
            // $posts['attr_1'] = $request->attr_1;
            $posts['created_by'] = Auth::user()->id;
            $posts['is_active'] = $request->is_active;
            $posts['attr_5'] = 'youtube';
            $posts['attr_6'] = $request->attr_6;
            $posts['is_popular'] = 0;
            $posts['post_type'] = $request->postType;
            if($request->image != NULL){
                $posts['image'] = '/uploads/content/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/content'), time().'.'.$request->image->getClientOriginalExtension());
            }

            $posts->save();

            // Save terms
            $tags = explode(',', $request->tags);
            ContentsTerms::where('content_id', $id)->delete();
            foreach($tags as $value){
                $input_terms['name'] = $value;
                $input_terms['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $value))))), '-'));
                $input_terms['type'] = 'tag';

                $terms = Terms::where('name', '=', $value)->first();
                if ($terms === null) {
                    $id_terms = Terms::create($input_terms)->id;
                    $input_content_terms['content_id'] = $id;
                    $input_content_terms['term_id'] = $id_terms;
                    ContentsTerms::create($input_content_terms);
                } else {
                    $input_content_terms['content_id'] = $id;
                    $input_content_terms['term_id'] = $terms->id;
                    ContentsTerms::create($input_content_terms);
                }
            }

            Flash::success('Posts update successfully.'); 
        }

        if ($request->create == 1){
            if($request->postType == 'automatic'){
                return redirect(route('admin.posts.createAutomatic'));
            } else {
                return redirect(route('admin.posts.createManual'));
            }          
        } else {
            return redirect(route('admin.posts.index'));
        }
    }

    public function updatePopular(Request $request) 
    {   
        $posts = Posts::find($request->id);
        if($posts->is_popular == 0){
            $posts['is_popular'] = 1;
        }else{
            $posts['is_popular'] = 0;
        }
        $posts->save();
        return $posts;
    }


    public function checkYoutube(Request $request)
    {
        
        $video = Posts::where('attr_6', $request->id)->first();
        if(!empty($video)){
            return response()->json([
                'result' => Youtube::getVideoInfo($request->id),
                'message' => 'Video is duplicated.',
                'status' => 'failed'
            ]);
        }

        return response()->json([
            'result' => Youtube::getVideoInfo($request->id),
            'message' => 'Get data success',
            'status' => 'success'
        ]);
    }
    
    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $posts = posts::find($id);

            if (empty($posts)) {
                Flash::error('Posts not found');

                return redirect(route('admin.posts.index'));
            }
            $posts['is_active'] = 1;

            $posts->save();

            Flash::success('Posts activated successfully.');
        }elseif ($request->action == 'inact') {
            $posts = posts::find($id);

            if (empty($posts)) {
                Flash::error('Posts not found');

                return redirect(route('admin.posts.index'));
            }
            $posts['is_active'] = 0;

            $posts->save();

            Flash::success('Posts Inactive successfully.');
        }elseif ($request->action == 'del'){
            $posts = posts::find($id);

            if (empty($posts)) {
                Flash::error('Posts not found');

                return redirect(route('admin.posts.index'));
            }

            $posts->delete();

            Flash::success('Posts Delete successfully.');
        }
        

        return redirect(route('admin.posts.index'));
    }
}
