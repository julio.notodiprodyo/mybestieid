<?php

namespace App\Http\Controllers\Website;

use App\Model\VerifyUser;
use App\Mail\VerifyMemberMail;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Website\Controller;
use App\Http\Requests\StoreMemberRegistrationRequest;

class MemberAreaController extends Controller
{
    protected $users;
    protected $roles;

     /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->middleware('guest');
    }

    public function index()
    {
        return view('frontend.member-area.auth.registration');
    }

    public function registration(StoreMemberRegistrationRequest $request)
    {
        DB::beginTransaction();
        try{
            $user = [
                'email'     => $request->email,
                'name'      => $request->name,
                'password'  => $request->password,
                'is_active' => 0
            ];
        
            $user = $this->users->create($user);

            $memberID = $this->roles->findWhere([
                'name' => 'member'
            ])->first()->id;

            $user->roles()->sync([$memberID]);
            
            VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40)
            ]);

            Mail::to($user->email)->send(new VerifyMemberMail($user));
            alert()->success('Successfully register. Please check your email to activation your account.')->persistent('Close');

            DB::commit();

            return redirect()->route('home');
        } catch (\Exception $e) {
            DB::rollback();
            
            alert()->success($e->getMessage())->persistent('Close');
            return redirect()->route('home');
        }
    }

    public function subscribe()
    {
        return view('frontend.subscribe');
    }
}
