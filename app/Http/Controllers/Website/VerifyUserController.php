<?php

namespace App\Http\Controllers\Website;

use App\Model\User;
use App\Model\VerifyUser;
use App\Http\Controllers\Website\Controller;

class VerifyUserController extends Controller
{
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(!empty($verifyUser) ){
            if($verifyUser->verified == 'N'){
                $verifyUser->verified = 'Y';
                $verifyUser->save();
                
                $user = User::find($verifyUser->user_id);
                $user->is_active = 1;
                $user->save();
                $status = 'Your account has been verified';
                $msg = '<p>Congrats '.$user->name.', your account is now verified. You can login now.</p></p>';
            }else{
                $status = "Your account is already verified"; 
                $msg = '';
            }
        }else{
            return redirect('/confirmed')->with('warning', "Sorry your email cannot be identified.");
        }

        return view('frontend.member-area.auth.verify', compact('status', 'msg'));
    }
}
