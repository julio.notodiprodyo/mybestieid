<?php

namespace App\Http\Controllers\Website;

use App\Model\Slideshow;
use App\Model\Video;
use App\Model\Category;
use App\Model\Terms;
use App\Model\Reviews;
use App\Model\Channels;
use App\Model\Comments;
use App\Model\Content;
use App\Model\Podcast;
use App\Model\ContentsTerms;
use App\Model\EReference;
use App\Model\PremiumPackageUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getRandomContent($type)
    {
        $data = Content::select('title', 'slug', 'image', 'is_hd', 'is_active')->where(['is_active' => '1', 'type' => $type])->inRandomOrder()->get();

        $query = [];
        foreach($data as $key => $value){
            $query[$key]['image'] = !empty($value->image) ? $value->image : asset('frontend/dummy_150x75.png');
            $query[$key]['slug']  = $value->slug;
            $query[$key]['is_hd'] = $value->is_hd;
            $query[$key]['title'] = $value->title;
        }
    
        return $query;
    }

    public function getInfoTag($params)
    {
        $content_terms = ContentsTerms::where('content_id', $params)->get();
        $terms = array();
        foreach ($content_terms as $value){
            $term = Terms::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        $tags = implode(' • ', $terms);

        return $tags;
    }

    public function pageNotFound()
    {
        return view('frontend.component.404');
    }

    public function getRate($params)
    {
        $query = Reviews::select('rating', 'user_id', 'created_at')->with(['users' => function ($query) {
            $query->select('id', 'name', 'email');
        }])->where(['content_id' => $params])->get();

        $avg = 0;
        foreach($query as $rev){
            $avg += $rev->rating;
        }
        
        $ratedPercentage = !empty($avg) ? ((round($avg/count($query)) / 4.5) * 100) : 1;
        $data = [
            'rate'          => $query,
            'percentage'    => $ratedPercentage,
            'total'         => !empty($avg) ? round($avg/count($query)) : 0
        ];

        return $data;
    }

    public function getfilterChannels($params, $type)
    {
        $html = '';
        foreach ($params as $item) {
            $imageChannel = !empty($item['image']) ? $item['image'] : asset('frontend/dummy_150x75.png');
            $imageVOD = !empty($item['image']) ? $item['image'] : asset('frontend/dummy_170x255.png');
            switch($type){
                case 'channel':
                    $isHD = $item['is_hd'] == 1 ? '<div><span>HD</span><p class="two">LIVE</p></div>' : '';
                    $html .= ' <a class="channel col-4 col-md-2 col-xxl-2" href="'. route('channel.slug', $item['slug']) .'">
                        <img style="width:150px; height:75px" src="'. $imageChannel .'" />'. $isHD .'   
                    </a>';
                    break;
                case 'podcast':
                    $html .= ' <a class="channel col-4 col-md-2 col-xxl-2" href="'. route('podcast.slug', $item['slug']) .'">
                        <img style="width:150px; height:75px" src="'. $imageChannel .'" />  
                    </a>';
                    break;
                default:
                    $html .= ' <a href="'. route('vod.slug', $item['slug']) .'" alt="'. $item['title'] .'" class="poster col-4 col-md-2 col-xxl-2">
                        <div>
                            <img width="170" src="'. $imageVOD .'" style="height:220px;"/>
                        <div></div></div>
                    </a>';
                    break;
            }
            
        }
        
        echo $html;
    }

    public function getDownloadPDF($data)
    {
        $html = '<div class="table-responsive ">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Release</th>
                        <th scope="col" width="140px;">Download</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'. $data['title'] .'</td>
                        <td>
                            <a target="_blank" href="'. $data['attr_3'] .'" class="btn btn-primary btn-sm"> Download !</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>';

        echo $html;
    }

    // uses regex that accepts any word character or hyphen in last name
    public function splitName($name) 
    {
        $parts = array();

        while ( strlen( trim($name)) > 0 ) {
            $name = trim($name);
            $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $parts[] = $string;
            $name = trim( preg_replace('#'.preg_quote($string,'#').'#', '', $name ) );
        }

        if (empty($parts)) {
            return false;
        }

        $parts = array_reverse($parts);
        $name = array();
        $name['first_name'] = $parts[0];
        $name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
        $name['last_name'] = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : '');
        
        return $name;
    }

    public function apiHomePage()
    {
        $slideshow  = Slideshow::select('title', 'image', 'attr_3 as url', 'content', 'is_active')->where(['is_active' => '1'])->limit(10)->get();

        $video      = Video::select('id', 'title', 'slug', 'excerpt', 'content', 'image', 'is_hd', 'is_active', 'is_featured', 'viewed', 'attr_5 as media_type', 'attr_6 as media_url_id');
        $newRelease = $video->where(['is_active' => 1])->orderBy('created_at', 'desc')->get();
        $popular    = $video->where(['is_active' => 1, 'is_popular' => 1])->get();
        $review     = Reviews::with(['video' => function ($query) {
            $query->select('id', 'title', 'slug', 'excerpt', 'content', 'image', 'is_hd', 'is_active', 'is_featured', 'is_popular', 'is_popular', 'viewed', 'attr_5 as media_type', 'attr_6 as media_url_id');
        }])->select('content_id', DB::raw('AVG(rating) as rate'))->groupBy('content_id')->orderBy('rate', 'desc')->get();                
        $topRated   = [];
        foreach($review as $key => $value){
            if(!empty($value->video)){
                $topRated[] = $value->video;
            }
        }
        $channel    = Channels::select('id', 'title', 'slug', 'excerpt', 'content', 'image', 'is_hd', 'is_active', 'is_featured', 'viewed', 'attr_5 as media_type', 'attr_6 as media_url_id')->where(['is_active' => 1, 'is_featured' => 1])->whereNotNull('order')->orderBy('order', 'asc')->get();
        $content = [
            'channel'   => [
                'data'          => $channel,
                'label'         => 'Featured LiveTV',
                'size_image'    => 'width="75" height="150"',
            ],
            'top_rated' => [
                'data'          => $topRated,
                'label'         => 'Top Rated',
                'size_image'    => 'width="170" style="height: 255px;"',
            ],
            'popular'   => [
                'data'          => $popular,
                'label'         => 'Popular',
                'size_image'    => 'width="170" style="height: 255px;"',
            ],
            'new_release'       => [
                'data'          => $newRelease,
                'label'         => 'New Releases',
                'size_image'    => 'width="170" style="height: 255px;"',
            ],
        ];

        return [
            $slideshow,
            $content
        ];
    }

    public function apiChannel()
    {
        $channel    = Channels::with(['parent' => function ($query) {
            $query->select('id', 'title', 'is_active', 'slug');
        }])->whereHas('parent', function ($query) {
            $query->where('slug', '!=', 'radio-streaming');
        })->select('title', 'slug', 'image', 'is_hd', 'order', 'viewed', 'is_active', 'id', 'parent_id')->offset(0)->limit(40)->where(['is_active' => '1'])->orderBy('created_at', 'desc')->get();

        $category   = Category::select('id', 'title', 'is_active')->where(['is_active' => '1'])->get();

        return [
            $channel,
            $category
        ];
    }

    public function apiPodcast()
    {
        $podcast    = Podcast::select('title', 'slug', 'image', 'is_hd', 'order', 'viewed', 'is_active')->offset(0)->limit(40)->where(['is_active' => '1'])->orderBy('created_at', 'desc')->get();

        return [
            $podcast
        ];
    }

    public function apiVOD()
    {
        $vod = Video::select('title', 'slug', 'image', 'order', 'created_at', 'is_active')->offset(0)->limit(40)->where(['is_active' => '1'])->orderBy('created_at', 'desc')->get();

        return [
            $vod
        ];
    }

    public function apiEReference()
    {
        $eReference = EReference::select('title', 'slug', 'is_active', 'image', 'created_at')->offset(0)->limit(40)->where(['is_active' => '1'])->orderBy('created_at', 'desc')->get();

        return [
            $eReference
        ];
    }

    public function apiDetailChannel($slug)
    {
        $data = Channels::with(['parent' => function ($query) {
            $query->select('id', 'title');
        }])->select('id', 'title', 'image', 'content', 'attr_3 as m3u8', 'attr_7 as mpeg', 'attr_4 as website_url', 'slug', 'post_type', 'parent_id')->where(['is_active' => '1', 'slug' => $slug])->first();

        if(empty($data)){ return $this->pageNotFound(); };

        Content::where(['slug' => $slug])->increment('viewed');

        $image = (!empty($data->image)) ? $data->image : asset('frontend/dummy_150x75.png');

        // for you
        $recomendation          = $this->getRandomContent('channel');
        $keyOtherRecomendation  = array_search($slug, array_column($recomendation, 'slug'));
        unset($recomendation[$keyOtherRecomendation]);
        $newOtherRecomendation  = array_values($recomendation); 
        // end for you

        // comment
        $comments = $this->getComment($slug);
        // end comment

        // rate
        $rate       = $this->getRate($data->id);
        // end rate 
        
        // info(tag)
        $tags = $this->getInfoTag($data->id);
        // end info(tag)

        return [
            $data, 
            $newOtherRecomendation, 
            $tags, 
            $image, 
            $comments, 
            $rate
        ];
    }

    public function apiDetailVOD($slug)
    {
        $data   = Video::select('id', 'title', 'content', 'image', 'attr_6 as youtube', 'slug', 'is_active')->where(['is_active' => '1', 'slug' => $slug])->first();
        if(empty($data)){ return $this->pageNotFound(); };

        Content::where(['slug' => $slug])->increment('viewed');

        $image  = (!empty($data->image)) ? $data->image : asset('frontend/dummy_170x255.png');
        
        $premium = null;
        if (Auth::check()){
            $premium = PremiumPackageUser::with('premiumPackage', 'paymentTransaction')->whereHas('premiumPackage', function ($query) use ($data){
                $query->where(['content_id' => $data->id]);
            })->whereHas('paymentTransaction', function ($query) use ($data){
                $query->where(['payment_status' => 'paid']);
            })->where(['user_id' => Auth::user()->id])->first();
        }

        // for you
        $recomendation          = $this->getRandomContent('video');
        $keyOtherRecomendation  = array_search($slug, array_column($recomendation, 'slug'));
        unset($recomendation[$keyOtherRecomendation]);
        $newOtherRecomendation  = array_values($recomendation); 
        // end for you

        // comment
        $comments = $this->getComment($slug);
        // end comment

        // rate
        $rate       = $this->getRate($data->id);
        // end rate 

        // info(tag)
        $tags = $this->getInfoTag($data->id);
        // end info(tag)

        return [
            $data, 
            $newOtherRecomendation, 
            $tags, 
            $image, 
            $comments, 
            $rate,
            $premium
        ];
    }

    public function getComment($slug){
        $comments = Comments::with('hasUser:id,name,image')->whereHas('hasContent', function($query) use ($slug){
            $query->where('slug', '=', $slug);
        })->orderBy('created_at', 'DESC')->get();
        
        return $comments;
    }

    public function apiSearch(){
        $querySearch = str_replace(
            [
              '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')',
              '[', ']', '{', '}', '+', '=', '|', '/', '?',
              "'", '"', '.', ',', '<', '>', ':', ';'
            ],
            '',
            request()->input('key')
        );      
        
        $query = [];
        if(!empty($querySearch)) {
            $search = Content::select('slug', 'title', 'image', 'is_hd', 'type')->where(function ($query) use($querySearch) {
                $query->where([
                        ['is_active', '=', 1],
                        ['title', 'LIKE', "%{$querySearch}%"]
                    ])->whereIn('type', ['channel', 'video']);
            })->get();
            
            if(count($search) > 0){
                foreach($search as $key => $value) {
                    switch ($value['type']) {
                        case 'channel':
                            $query['channel'][$key] = $value;
                            break;
                        default:
                            $query['vod'][$key] = $value;
                            break;
                    }
                }
    
                if(isset($query['vod'])){
                    $total          = count($query['vod']); // TOTAL COUNT OF THE SET, THIS IS NECESSARY SO THE PAGINATOR WILL KNOW THE TOTAL PAGES TO DISPLAY
                    $page           = request()->page ?? 1; // GET CURRENT PAGE FROM THE REQUEST, FIRST PAGE IS NULL
                    $perPage        = 40; // HOW MANY ITEMS YOU WANT TO DISPLAY PER PAGE?
                    $offset         = ($page - 1) * $perPage; // GET THE OFFSET, HOW MANY ITEMS NEED TO BE "SKIPPED" ON THIS PAGE
                    $query['data']  = array_slice($query['vod'], $offset, $perPage); // THE ARRAY THAT WE ACTUALLY PASS TO THE PAGINATOR IS SLICED
        
                    $query['paginator'] = new Paginator($query['data'], $total, $perPage, $page, [
                        'path' => Paginator::resolveCurrentPath(),
                        'pageName' => 'page',
                    ]);
                }

            }
        }

        return $query;
    }
}
