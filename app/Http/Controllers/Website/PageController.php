<?php

namespace App\Http\Controllers\Website;

use App\Model\Page;
use App\Model\Ads;
use App\Model\User;
use App\Model\Video;
use App\Model\Contact;
use App\Model\Content;
use App\Model\Comments;
use App\Model\Podcast;
use App\Model\ContentAds;
use App\Model\EReference;
use Illuminate\Http\Request;
use App\Model\PremiumPackageUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Website\Controller;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class PageController extends Controller
{
    public function index()
    {   
        list($slideshow, $content) = $this->apiHomePage();

        return view('frontend.index', compact('slideshow', 'content'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function page($slug)
    {
        $data = Page::select('content')->where('slug', '=', $slug)->first();
        if(empty($data)){ return $this->pageNotFound(); };

        return view('frontend.page', compact('data'));   
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function channel(Request $request)
    {
        list($channel, $category) = $this->apiChannel();
        
        return view('frontend.channel.index', compact('channel', 'category'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function detailChannel($slug)
    {
        list($data, $newOtherRecomendation, $tags, $image, $comments, $rate) = $this->apiDetailChannel($slug);

        return view('frontend.channel.detail', compact('data', 'newOtherRecomendation', 'tags', 'image', 'comments', 'rate'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function vod(Request $request)
    {
        $vod = Video::select('title', 'slug', 'image', 'order', 'created_at', 'is_active')->offset(0)->limit(40)->where(['is_active' => '1'])->orderBy('created_at', 'desc')->get();

        return view('frontend.vod.index', compact('vod'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function detailVod($slug)
    {
        list($data, $newOtherRecomendation, $tags, $image, $comments, $rate, $premium) = $this->apiDetailVOD($slug);

        return view('frontend.vod.detail', compact('data', 'newOtherRecomendation', 'tags', 'image', 'comments', 'rate', 'premium'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function eReference()
    {
        list($eReference) = $this->apiEReference();
        
        return view('frontend.e-reference.index', compact('eReference'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function detailEReference($slug)
    {
        $data   = EReference::select('id', 'title', 'content', 'image', 'slug', 'is_active')->where(['is_active' => '1', 'slug' => $slug])->first();
        if(empty($data)){ return $this->pageNotFound(); };

        // other e-reference
        $recomendation          = EReference::select('id', 'title', 'content', 'image', 'slug', 'is_active')->where(['is_active' => '1'])->get()->toArray();
        $keyOtherRecomendation  = array_search($slug, array_column($recomendation, 'slug'));
        unset($recomendation[$keyOtherRecomendation]);
        $newOtherRecomendation  = array_values($recomendation); 
        // end other e-reference

        return view('frontend.e-reference.detail', compact('data', 'newOtherRecomendation'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function player($slug, $hsl)
    {
        $data = Content::select('attr_3 as hls_url', 'id', 'type', 'attr_6 as youtube', 'slug')->where(['slug' => $hsl])->whereIn('type', ['channel', 'video', 'podcast'])->first();
        if(empty($data)){ return redirect()->route('home'); };
        
        if($data->type != 'channel' && $data->type != 'podcast'){
            if(auth()->check()){
                $premiumPackageUser = PremiumPackageUser::with('premiumPackage', 'paymentTransaction')->whereHas('premiumPackage', function ($query) use ($data){
                    $query->where(['content_id' => $data['id']]);
                })->whereHas('paymentTransaction', function ($query) {
                    $query->where(['payment_status' => 'paid']);
                })->where(['user_id' => Auth::user()->id])->whereDate('end_date', '>=', date('Y-m-d h:i:s'))->orderBy('created_at', 'desc')->first();
        
                if(empty($premiumPackageUser)){ 
                    alert()->error('Please upgrade to premium package to enjoy this movie.', 'Error!')->persistent('Close');
                    return redirect()->route('vod.slug', $data['slug']);
                };
            }
        }

        Content::where(['slug' => $hsl])->increment('viewed');
        $ads = '';
        // player ads 
        $contentAds = ContentAds::where('content_id', '=', !empty($data->id) ? $data->id : $data->id)->first();
        if(!empty($contentAds)){
            $ads = Ads::select('video')->where('ads_id', '=', $contentAds->ads_id)->first();
        }
        // end player ads 
        return view('frontend.component.player', compact('data', 'contentAds', 'ads'));
    }

    public function sendComments(Request $request){
        $response = Comments::create([
            'content_id' => $request->content_id,
            'comment' => $request->comment,
            'user_id' => $request->user_id,
        ]);
        $user = User::find($request->user_id);
        $response->hasUser()->associate($user);
        $result = '<div class="comment-item">'.
        '<a title="View profile" href="#" class="avatar-thumb"><img src="'.asset($response['hasUser']['image']).'"></a>'.
        '<div class="comment-text">'.
        '<a>'.$response['hasUser']['name'].'</a><span class="float-right"><i class="fa fa-clock-o">'.$response['created_at']->diffForHumans().'</i></span>'.
        '<p>'.$response['comment'].'</p>'.
        '</div>'.
        '</div>';
        return $result;
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function search()
    {
        $query = $this->apiSearch();
        
        return view('frontend.search')->with(['query' => $query]);
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function contact()
    {
        if (request()->isMethod('POST')) {
            $rules = [
                'name'    => 'required|regex:/^[A-Za-z0-9,. -:]+$/',
                'email'   => 'required|email',
                'subject' => 'required|regex:/^[A-Za-z0-9,. -:]+$/',
                'message' => 'required|regex:/^[A-Za-z0-9,. -:]+$/',
                // 'captcha' => 'required|captcha'
            ];

            $validator = validator()->make(request()->all(), $rules);
            if ($validator->fails()) {
                alert()->error('Sorry, there was a problem with your request! You should check in on some of those fields below.', 'Message')->persistent('Close');
                return redirect()->route('contact')->withInput()->withErrors($validator);
            }  

            DB::beginTransaction();
            try{
                // POST DATA TO CONTACTS
                Contact::create([
                    'name'      => request()->name,
                    'email'     => request()->email,
                    'subject'   => request()->subject,
                    'message'   => request()->message,
                ]);

                \Mail::send('vendor.notifications.contact',[
                    'name'      => request()->get('name'),
                    'email'     => request()->get('email'),
                    'subject'   => request()->get('subject'),
                    'body'      => request()->get('message')
                ], function($message){
                    $message->from(request()->email);
                    $message->subject(request()->subject);
                    $message->to('njulioiyoo@gmail.com');
                });
                
                DB::commit();

                // VALIDATION WAS SUCCESFUL SHOW SWEETALERT AND RETURN 
                alert()->success('Thank you! Your message has been sent.', 'Message')->persistent('Close');
                return redirect()->route('contact');
            } catch (\Exception $e) {
                DB::rollback();
    
                alert()->success($e->getMessage())->persistent('Close');
                return redirect()->route('home');
            }
        }
        
        return view('frontend.contact');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function podcast()
    {
        list($podcast) = $this->apiPodcast();

        return view('frontend.podcast.index', compact('podcast'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function detailPodcast($slug)
    {
        $data = Podcast::select('id', 'title', 'content', 'image', 'attr_6 as youtube', 'slug', 'is_active')->where(['is_active' => '1', 'slug' => $slug])->first();

        if(empty($data)){ return $this->pageNotFound(); };

        Content::where(['slug' => $slug])->increment('viewed');

        $image = (!empty($data->image)) ? $data->image : asset('frontend/dummy_150x75.png');

        // for you
        $recomendation          = $this->getRandomContent('podcast');
        $keyOtherRecomendation  = array_search($slug, array_column($recomendation, 'slug'));
        unset($recomendation[$keyOtherRecomendation]);
        $newOtherRecomendation  = array_values($recomendation); 
        // end for you

        // comment
        $comments = $this->getComment($slug);
        // end comment

        // rate
        $rate       = $this->getRate($data->id);
        // end rate 
        
        // info(tag)
        $tags = $this->getInfoTag($data->id);
        // end info(tag)

        return view('frontend.podcast.detail', compact('data', 'image', 'comments', 'rate', 'tags', 'newOtherRecomendation'));
    }
}
