<?php

namespace App\Http\Controllers\Website;

use App\Model\Video;
use App\Http\Controllers\Website\Controller;
use App\Model\PaymentTransaction;
use App\Model\PremiumPackage;
use App\Model\PremiumPackageUser;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{
    public function subscribe($slug)
    {
        $vod = Video::select('id', 'title', 'slug', 'is_active')->where(['is_active' => '1', 'slug' => $slug])->first();
        if(empty($vod)){ return $this->pageNotFound(); };

        $checkUser = PremiumPackageUser::with('premiumPackage', 'paymentTransaction')->whereHas('premiumPackage', function ($query) use ($vod){
            $query->where(['content_id' => $vod['id']]);
        })->where(['user_id' => Auth::user()->id])->orderBy('created_at', 'desc')->first();
        if(!empty($checkUser) && $checkUser['paymentTransaction']['payment_status'] != 'pending'){
            $premiumPackageUser = PremiumPackageUser::with('premiumPackage', 'paymentTransaction')->whereHas('premiumPackage', function ($query) use ($vod){
                $query->where(['content_id' => $vod['id']]);
            })->whereHas('paymentTransaction', function ($query) {
                $query->where(['payment_status' => 'paid']);
            })->where(['user_id' => Auth::user()->id])->whereDate('end_date', '<=', date('Y-m-d h:i:s'))->orderBy('created_at', 'desc')->first();
    
            if(empty($premiumPackageUser)){ 
                alert()->error('Your account already has been upgrade to premium.', 'Error!')->persistent('Close');
                return redirect()->route('vod.slug', $slug);
            };
        }
        
        $premiumPackage = PremiumPackage::where(['content_id' => $vod['id'], 'is_active' => 1])->get();

        return view('frontend.subscription', [
            'clientKey'         => config('app.midtrans.client_key'),
            'slug'              => $slug,
            'vod'               => $vod,
            'premiumPackage'    => $premiumPackage
        ]);
    }
}
