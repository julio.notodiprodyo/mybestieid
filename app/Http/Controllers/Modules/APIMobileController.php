<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Website\Controller;
use App\Model\Menu;
use App\Model\Page;
use App\Model\SiteConfig;
use Illuminate\Http\Request;

class APIMobileController extends Controller
{
    public function homepage()
    {
        list($slideshow, $content) = $this->apiHomePage();

        return response()->json([
            'status'    => 'success',
            'result'      => [
                'slideshow'  => $slideshow,
                'content'    => $content,
            ],
            'status_code' => 200
        ], 200);
    }

    public function channel()
    {
        list($channel, $category) = $this->apiChannel();

        return response()->json([
            'status'    => 'success',
            'result'    => [
                'channel'   => $channel,
                'category'  => $category,
            ],
            'status_code' => 200
        ], 200);
    }

    public function detailChannel(Request $request)
    {
        list($data, $newOtherRecomendation, $tags, $image, $comments, $rate) = $this->apiDetailChannel($request->slug);

        return response()->json([
            'status'    => 'success',
            'result'    => [
                'data'                      => $data,
                'new_other_recomendation'   => $newOtherRecomendation,
                'tags'                      => $tags,
                'image'                     => $image,
                'comments'                  => $comments,
                'rate'                      => $rate
            ],
            'status_code' => 200
        ], 200);
    }

    public function podcast()
    {
        list($podcast) = $this->apiPodcast();

        return response()->json([
            'status'    => 'success',
            'result'    => [
                'podcast'   => $podcast
            ],
            'status_code' => 200
        ], 200);
    }

    public function vod()
    {
        list($vod) = $this->apiVOD();

        return response()->json([
            'status'    => 'success',
            'result'    => [
                'vod'   => $vod
            ],
        ], 200);
    }

    public function detailVOD(Request $request)
    {
        list($data, $newOtherRecomendation, $tags, $image, $comments, $rate, $premium) = $this->apiDetailVOD($request->slug);

        return response()->json([
            'status'    => 'success',
            'result'    => [
                'data'                      => $data,
                'new_other_recomendation'   => $newOtherRecomendation,
                'tags'                      => $tags,
                'image'                     => $image,
                'comments'                  => $comments,
                'rate'                      => $rate,
                'premium'                   => $premium
            ],
            'status_code' => 200
        ], 200);
    }

    public function eReference()
    {
        list($eReference) = $this->apiEReference();

        return response()->json([
            'status'    => 'success',
            'result'    => [
                'e_reference'    => $eReference
            ],
            'status_code' => 200
        ], 200);
    }

    public function configuration()
    {
        $menu = Menu::select('title', 'url', 'icon')->where('is_active', '=', '1')->orderBy('order', 'asc')->get();
        $page = Page::select('title', 'slug', 'attr_3 as icon')->where(['is_active' => '1'])->get();

        $configs = SiteConfig::get();

        $siteConfig = [];
        if (sizeof($configs) > 0) {
            foreach ($configs as $key => $value) {
                $siteConfig[$value->key] = $value->value;
            }
        }

        return response()->json([
            'status'    => 'success',
            'result'    => [
                'menu' => [
                    'top_menu'      => $menu,
                    'bottom_menu'   => $page
                ],
                'site_config' => $siteConfig
            ],
            'status_code' => 200
        ], 200);
    }

    public function search()
    {
        $search = $this->apiSearch();

        return response()->json([
            'status'    => 'success',
            'result'    => [
                'search'   => $search
            ],
        ], 200);
    }
}