<?php

namespace App\Http\Controllers\Modules;

use App\Model\User;
use App\Model\Video;
use App\Model\Reviews;
use App\Model\Content;
use App\Model\Channels;
use App\Model\Podcast;
use App\Model\EReference;
use Illuminate\Http\Request;
use App\Model\PremiumPackageUser;
use App\Model\PaymentTransaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Website\Controller;

class APIController extends Controller
{
    public function __construct()
    {
        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = config('app.midtrans.server_key');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = config('app.midtrans.is_production');
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;
    }

    public function ajaxFilterData(Request $request)
    {   
        $data = $this->checkType($request->type);

        switch($request->alt){
            case 'newest':
                $channel = $data->where(['is_active' => '1'])->orderBy('created_at', 'desc')->get();
                break;
            case 'views':
                $channel = $data->where(['is_active' => '1'])->orderBy('viewed', 'desc')->get();
                break;
            case 'rating':
                if($request->type == 'channel'){$rel = 'channels';}elseif($request->type == 'podcast'){$rel = 'podcasts';}else{$rel = 'video';};
                $review = Reviews::with($rel)->select('content_id', DB::raw('AVG(rating) as rate'))->groupBy('content_id')->orderBy('rate', 'desc')->get();                
                $channel = [];
                foreach($review as $key => $value){
                    if(!empty($value->$rel)){
                        $channel[] = $value->$rel;
                    }
                }
                break;
            case 'title':
                $channel = $data->where(['is_active' => '1'])->orderBy('title', 'asc')->get();
                break;
            default:
                $channel = $data->where(['parent_id' => $request->alt, 'is_active' => '1'])->get();
                break;
        }
        
        return $this->getfilterChannels($channel, $request->type);
    }

    private function checkType($type)
    {
        switch($type){
            case 'channel':
                $data = Channels::select('title', 'slug', 'image', 'is_hd', 'order', 'viewed', 'parent_id', 'is_active')->offset(0)->limit(40);
                break;
            case 'podcast':
                $data = Podcast::select('title', 'slug', 'image', 'order', 'viewed', 'parent_id', 'is_active')->offset(0)->limit(40);
                break;
            default:
                $data = Video::select('title', 'slug', 'image', 'order',  'parent_id', 'is_active')->offset(0)->limit(40);
                break;
        }

        return $data;
    }

    public function ajaxRating(Request $request)
    {
        $data = Content::select('id', 'slug', 'user_id')->where(['slug' => $request->slug])->whereIn('type', ['channel', 'video', 'podcast'])->first();
        $check = Reviews::where([['user_id', '=', auth()->user()->id], ['content_id', '=', $data->id]])->first();
        
        if(!empty($check)){return false;};

        return Reviews::create([
            'content_id' => $data->id,
            'comment'    => $request->review,
            'rating'     => $request->rating
        ]);
    }

    public function ajaxDownloadPDF(Request $request)
    {
        $data = EReference::select('title', 'attr_3')->where(['id' => $request->id])->first();

        return $this->getDownloadPDF($data);
    }

    public function ajaxTrxPayment(Request $request)
    {
        $user   = User::with('userCity')->where(['email' => auth()->user()->email, 'is_active' => 1])->first();
        $vod    = Video::select('contents.id', 'contents.title', 'contents.slug', 'contents.is_active', 'pp.amount', 'pp.id as premium_package_id', 'pp.time_period')
        ->leftJoin('premium_package as pp', 'pp.content_id', '=', 'contents.id')
        ->where(['contents.is_active' => 1, 'contents.slug' => $request->input('id'),'pp.amount' => trim($request->input('price'), "IDR ")])->first();

        // Optional
        $billing_address = [
            'first_name'    => $this->splitName($user['name'])['first_name'],
            'last_name'     => $this->splitName($user['name'])['last_name'],
            'address'       => "-",
            'city'          => $user['userCity']['name'],
            'postal_code'   => "-",
            'phone'         => $user['phone'],
            'country_code'  => 'IDN'
        ];

        // Optional
        $shipping_address = [
            'first_name'    => $this->splitName($user['name'])['first_name'],
            'last_name'     => $this->splitName($user['name'])['last_name'],
            'address'       => "-",
            'city'          => $user['userCity']['name'],
            'postal_code'   => "-",
            'phone'         => $user['phone'],
            'country_code'  => 'IDN'
        ];

        // Optional
        $customer_details = [
            'first_name'        => $this->splitName($user['name'])['first_name'],
            'last_name'         => $this->splitName($user['name'])['last_name'],
            'email'             => $user['email'],
            'phone'             => $user['phone'],
            'billing_address'   => $billing_address,
            'shipping_address'  => $shipping_address
        ];

        $orderID = "DecxTv-TX" . rand();
        $transaction = [
            "transaction_details" => [
                "order_id"      => $orderID,
                "gross_amount"  => $vod['amount']
            ],
            "customer_details" => $customer_details,
            "item_details" => [
                [
                "id" => $vod['id'],
                "name" => $vod['title'],
                "price" => $vod['amount'],
                "quantity" => '1',
                ]
            ]
        ];

        $timePeriode = '+' . $vod['time_period'];
        $premiumPackageUser = PremiumPackageUser::create([
            'user_id'             => $user['id'],
            'premium_package_id'  => $vod['premium_package_id'],
            'start_date'          => date('Y-m-d H:i:s'),
            'end_date'            => date('Y-m-d H:i:s', strtotime($timePeriode))
        ])->id;

        PaymentTransaction::create([
            'premium_package_user_id'   => $premiumPackageUser,
            'order_id'                  => $orderID,
            'payment_status'            => 'pending',
            'status_code'               => '',
        ]);
        
        $token = $this->getSnapToken($transaction);

        return response()->json([
            'token' => $token
        ]);
    }

    private function getSnapToken($transaction)
    {
        return \Midtrans\Snap::getSnapToken($transaction);
    }

    public function paymentNotificationMidtrans()
    {
        $notif = new \Midtrans\Notification();

        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status;
        
        $payload    = json_decode(request()->getContent());
        $data       = json_decode(json_encode($payload), true);
        $name       =  $order_id;
        if(!Storage::disk('payment')->put($name.'.json',json_encode($data)));


        $paymentTransaction = PaymentTransaction::where(['order_id' => $order_id]);
        if ($transaction == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
                if ($fraud == 'challenge') {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    // echo "Transaction order_id: " . $order_id ." is challenged by FDS";
                } else {
                    // TODO set payment status in merchant's database to 'Success'
                    // echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
                }
            }
        } else if ($transaction == 'settlement') {
            $paymentTransaction->update(['payment_status' => 'paid', 'payment_type' => $type, 'status_code' => $notif->status_code]);
        } else if ($transaction == 'pending') {
            $paymentTransaction->update(['payment_status' => 'pending', 'payment_type' => $type, 'status_code' => $notif->status_code]);
        } else if ($transaction == 'deny') {
            $paymentTransaction->update(['payment_status' => 'deny', 'payment_type' => $type, 'status_code' => $notif->status_code]);
        } else if ($transaction == 'expire') {
            $paymentTransaction->update(['payment_status' => 'expire', 'payment_type' => $type, 'status_code' => $notif->status_code]);
        } else if ($transaction == 'cancel') {
            $paymentTransaction->update(['payment_status' => 'cancel', 'payment_type' => $type, 'status_code' => $notif->status_code]);
        }
    }

    public function paymentStatusMidtrans($orderId)
    {
        $status     = \Midtrans\Transaction::status($orderId);
        $data       = json_decode(json_encode($status), true);

        $html = '<p><img style="display: block; margin-left: auto; margin-right: auto;" title="Tiny Logo" src="../decx-logo.png" alt="TinyMCE Logo" width="128" height="128" /></p>
        <h2 style="text-align: center; color: #000;">Payment Status Updates!</h2>
        <h5 style="text-align: center; color: #000;">Monitor and verify payment status, so that you can respond to successful and failing payments.</h5>
        <table style="border-collapse: collapse; width: 100%;" border="1">
           <thead>
              <tr>
                 <th>Payment Type</th>
                 <th>Gross Amount</th>
                 <th>Bank</th>
                 <th>VA Number</th>
                 <th>Transaction Time</th>
              </tr>
           </thead>
           <tbody>
              <tr>
                 <td>'. strtoupper($data['payment_type']) .'</td>
                 <td>Rp. '. number_format($data['gross_amount']) .'</td>
                 <td>'. strtoupper($data['va_numbers'][0]['bank']) .'</td>
                 <td>'. $data['va_numbers'][0]['va_number'] .'</td>
                 <td>'. $data['transaction_time'] .'</td>
              </tr>
           </tbody>
        </table>
        <p>&nbsp;</p>';

        return $html;
    }
}