<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Website\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use App\Model\User;
use App\Model\UserGroup;
use App\Model\UserToken;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;
use App\Model\YoutubePartner;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;
use Session;
use Youtube;
use Socialite;
use App\Http\Controllers\Auth\LoginController;
use alchemyguy\YoutubeLaravelApi\ChannelService;
use Illuminate\Support\Facades\Redirect;

class CallbackController extends Controller
{
    public function redirectToProvider($provider)
    {
        if(!config("services.$provider")) abort('404');
        
        return Socialite::driver($provider)
            ->scopes([
                'https://www.googleapis.com/auth/youtubepartner',
                'https://www.googleapis.com/auth/youtubepartner-content-owner-readonly'
            ])
            ->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        if($provider=="youtube" && isset($_GET['code'])){
            $authCode = $_GET['code'];

            $callbackUrl = 'http://mybestie.id/auth/youtube/callback';
            $client = new \Google_Client();
            $client->setClientId(env('GOOGLE_CLIENT_ID'));
            $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
            $client->setScopes('https://www.googleapis.com/auth/youtube');
            $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
            $client->setRedirectUri($callbackUrl);
            $client->setAccessType('offline');
            $client->setApprovalPrompt('force');
            $client->setPrompt('consent select_account');
            
            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            dd($accessToken);
        }else{
            exit('No code found');
        }

        $users = Socialite::driver($provider);
        $getToken = $users->user();
        
        if($provider=="twitter"){ //Jika add AUTH twitter
            $redirect = Auth::user()->roles[0]->name;
            $UserToken  = new UserToken();
            $UserToken->user_id = Auth::user()->id;
            $UserToken->type  = 'twitter';
            $UserToken->expires_in  = '3600';
            $UserToken->access_token  = $getToken->token;
            $UserToken->token_secret  = $getToken->tokenSecret;
            $UserToken->refresh_token  = $getToken->token;
            $UserToken->save();

            Flash::success('Data has been success..');
            return redirect($redirect.'/platforms');

        }elseif(Auth::user()->roles[0]->name =='user' && $provider=="youtube"){    
            $ch = curl_init();
    
            curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/partner/v1/contentOwners?fetchMine=true&key='.env('GOOGLE_CLIENT_ID'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    
            $headers = array();
            $headers[] = 'Authorization: Bearer '.$getToken->token;
            $headers[] = 'Accept: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $contentID = json_decode($result);

            DB::table('user_token')->insert([
                'user_id'=>Auth::user()->id,
                'type'=>'youtube',
                'access_token' => $getToken->accessTokenResponseBody['access_token'],
                'refresh_token' => $getToken->refreshToken,
                'expires_in' => $getToken->accessTokenResponseBody['expires_in'],
                'scopes' => $getToken->accessTokenResponseBody['scope'],
                'token_type' => $getToken->accessTokenResponseBody['token_type'],
                'token_secret' => $getToken->token,
                'refresh_token' => $getToken->refreshToken,
            ]);

            foreach ($contentID->items as $cid) {
                $data = $cid;

                //dd($data);

                if(!empty($data->id)){
                    $id = $data->id;
                }else{
                    $id = '';
                }
                if(!empty($data->displayName)){
                    $displayName = $data->displayName;
                }else{
                    $displayName = '';
                }
                if(!empty($data->primaryNotificationEmails[0])){
                    $primaryNotificationEmails = $data->primaryNotificationEmails[0];
                }else{
                    $primaryNotificationEmails = '';
                }
                if(!empty($data->conflictNotificationEmail)){
                    $conflictNotificationEmail = $data->conflictNotificationEmail;
                }else{
                    $conflictNotificationEmail = '';
                }
                if(!empty($data->disputeNotificationEmails[0])){
                    $disputeNotificationEmails = $data->disputeNotificationEmails[0];
                }else{
                    $disputeNotificationEmails = '';
                }
                if(!empty($data->fingerprintReportNotificationEmails[0])){
                    $fingerprintReportNotificationEmails = $data->fingerprintReportNotificationEmails[0];
                }else{
                    $fingerprintReportNotificationEmails = '';
                }

                $youtubePartner[] = [
                    'user_id'                    => Auth::user()->id,
                    'content_id'                 => $id,
                    'display_name'               => $displayName,
                    'type'                       => $provider,
                    'email_primary'              => $primaryNotificationEmails,
                    'email_conflict'             => $conflictNotificationEmail,
                    'email_dispute'              => $disputeNotificationEmails,
                    'email_fingerprintreport'    => $fingerprintReportNotificationEmails,  
                ];
            }
            YoutubePartner::insert($youtubePartner);

            return redirect(route('connect.cms'));

        }elseif(Auth::check()){
            $redirect = Auth::user()->roles[0]->name;
            $model = User::with('accessrole')->where('provider_id', $user->id)->first();

            if(empty($model) && ($provider=="youtube")){  //save user socialmedia YOUTUBE
                DB::table('user_token')->insert([
                    'user_id'=>Auth::user()->id,
                    'type'=>'youtube',
                    'access_token' => $user->accessTokenResponseBody['access_token'],
                    'expires_in' => $user->accessTokenResponseBody['expires_in'],
                    'scopes' => $user->accessTokenResponseBody['scope'],
                    'token_type' => $user->accessTokenResponseBody['token_type'],
                    'token_secret' => $user->token,
                    'refresh_token' => $user->refreshToken,
                ]);

                Flash::success('Data has been success..');
                return redirect($redirect.'/platforms');

            }else{
                /*Tambah akses role dari Auth sosial media*/
                $roless = $model->roles[0]->name;
                if($roless =="creator" || $roless =="songwriter" || $roless =="singer" || $roless =="influencer" ){
                    $authMaster =  Session::get('group_master');
                    $UserGroup  = new UserGroup();
                    $UserGroup->user_master = $authMaster->id;
                    $UserGroup->user_child  = $model->id;
                    $UserGroup->save();

                    if($UserGroup){
                        Flash::success('Data has been registered.');
                        Session::forget('user_group');

                        $updateGroup = UserGroup::where(['user_master'=>$authMaster->id])->get();
                        session(['user_group'=>$updateGroup]);
                        return redirect($redirect.'/addaccount');

                    }else{
                        Flash::error('Data has been registered.');
                        return redirect($redirect.'/addaccount');
                    }
                }else{
                    Flash::error('User cant to be Access.');
                    return redirect($redirect.'/addaccount');    
                }   
            }
        }else{
            /* Tambah akun baru dari Auth Youtube*/
            $authUser = $this->authLogin->findOrCreateUser($user, $provider);
            Auth::login($authUser, true);
            return redirect()->route('creators.validate', auth()->user()->provider_id);
        }
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        //dd($user);
        $authUser  = User::where('provider_id', $user->id)->orWhere('email', $user->nickname)->first();
        $authUser2 = User::where('id', Auth::id())->first();
        //dd($authUser2);
        if ($authUser) {
            //dd('already youtube login');
            return $authUser;
        } else if ($authUser2 && !$authUser) {
            //dd('login member');
            $u              = User::find(Auth::id());
            $u->provider_id = $user->id;
            $u->save();
            return $authUser2;
        } else {
            // dd('register');
            $user = User::create([
                'name_channel' => $user->nickname,
                'email'        => empty($user->email) ? $user->nickname : $user->email,
                'provider'     => $provider,
                'provider_id'  => $user->id,
                'image'        => $user->avatar,
                'slug'         => str_slug($user->nickname),
            ]);

            if (!empty($user->id)) {
                $u = User::where('id', $user->id)->first();
                $u->roles()->attach(5);
            }

            return $user;

        }
    }
}
