<?php

namespace App\Http\Controllers\Auth;

use Socialite;
use Carbon\Carbon;
use App\Model\User;
use App\Model\RoleUser;
use App\Model\UserGroup;
use App\Model\AccessRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Website\Controller;
use Dotenv\Exception\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;
    
    protected $maxAttempts = 5; // change to the max attemp you want.
    protected $decayMinutes = 3;// change to the minutes you want.
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // protected function sendFailedLoginResponse(Request $request)
    // {
    //     throw ValidationException::withMessages([
    //         'email' => [trans('auth.failed')],
    //     ]);
    // }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('home');
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        $user = Auth::user();
        $getUser = User::where(['id'=>$user->id])->first();
        $role = AccessRole::select('roles.id','roles.name')->join('roles', 'roles.id', '=', 'access_role.role_id')->where(['access_role.user_id'=>$user->id])->get();
        $roleMenu = AccessRole::select('roles.id','roles.name')->join('roles', 'roles.id', '=', 'access_role.role_id')->where(['access_role.user_id'=>$user->id, 'roles.name'=>'admin'])->get();
        $UserGroup = UserGroup::where(['user_master'=>$user->id])->get();

        session(['user_role'    => $role]);
        session(['menu_role'    => $roleMenu]);
        session(['user_group'   => $UserGroup]);
        session(['group_master' => $getUser]);

        return '/';
    }

    public function validateLogin(Request $request)
    {
        $encrypt     = $request->password;
        $last_two    = substr($encrypt, -2);
        $encrypt1    = rtrim($encrypt, $last_two);
        $random_char = substr($encrypt1, -14);
        $encrypt2    = rtrim($encrypt1, $random_char);
        $hashed      = base64_decode($encrypt2 . $last_two);
        $hashed2     = base64_decode($hashed);

        $request['password'] = $hashed2;

        $rules = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];

        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            alert()->error('Sorry, there was a problem with your request! You should check in on some of those fields below.', 'Error!')->persistent('Close');
            return redirect()->route('login')->withInput()->withErrors($validator);
        }     

    }

    public function authenticated(Request $request, $user) 
    {
        $user->last_sign_in = Carbon::now()->toDateTimeString();
        $user->save();
        
        $request->session()->flash('success_login', 'You have been logged in, '.$user->name.'');
        return redirect()->intended($this->redirectPath());
    }

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        if(!config("services.$provider")) abort('404');
        
        return Socialite::driver($provider)
            ->scopes([
                
                'https://www.googleapis.com/auth/youtubepartner',
            ])
            ->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->scopes([
            'https://www.googleapis.com/auth/youtubepartner',
            'https://www.googleapis.com/auth/youtubepartner-content-owner-readonly'
        ])->user();

        //dd($user);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/partner/v1/contentOwners?fetchMine=true&key=AIzaSyA-KUB0wyU0w0dIJCCkmF6ShS73-syL9zA');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authorization: Bearer '.$user->token;
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $contentID = json_decode($result, true);
        return($contentID);

        $authUser = $this->findOrCreateUser($user, $provider);

        //dd($authUser);

        Auth::login($authUser, true);

        return redirect()->route('creators.validate', auth()->user()->provider_id);
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        //dd($user);
        $authUser  = User::where('provider_id', $user->id)->orWhere('email', $user->nickname)->first();
        $authUser2 = User::where('id', Auth::id())->first();
        //dd($authUser2);
        if ($authUser) {
            //dd('already youtube login');
            return $authUser;
        } else if ($authUser2 && !$authUser) {
            //dd('login member');
            $u              = User::find(Auth::id());
            $u->provider_id = $user->id;
            $u->save();
            return $authUser2;
        } else {
            // dd('register');
            $user = User::create([
                'name_channel' => $user->nickname,
                'email'        => empty($user->email) ? $user->nickname : $user->email,
                'provider'     => $provider,
                'provider_id'  => $user->id,
                'image'        => $user->avatar,
                'slug'         => str_slug($user->nickname),
            ]);

            if (!empty($user->id)) {
                $u = User::where('id', $user->id)->first();
                $u->roles()->attach(5);
            }

            return $user;

        }
    }
}
