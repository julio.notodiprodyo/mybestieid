<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateSlideshowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|max:100|regex:/^[a-zA-Z0-9\s]*$/',
            'attr_3'    => 'max:250',
            'image'     => 'image','mimes:jpg,png,jpeg,gif','max:1000',
            'is_active' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'attr_3.regex'     => 'The URL format is invalid.',
            'attr_3.required'  => 'URL field is required.',
            'attr_3.max'       => 'URL field may not be greater than 250 characters.'
        ];
    }
}
