<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMemberRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'                 => 'required|email|unique:users,email',
            'name'                  => 'required|regex:/^[a-zA-Z0-9\s]*$/',
            'password'              => 'required|regex:/^[a-zA-Z0-9\s]*$/|min:8',
            'privacypolicy'         => 'required'
        ];
    }
}
