<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(isset(request()->parent_id)){
            return [
                'title'     => 'required|max:50',
                'url'     => 'required|max:70',
                // 'parent_id'      => 'required',
                'is_active'      => 'required',
            ];
        }else{
            return [
                'title'     => 'required|max:50',
                'url'     => 'required|max:70',
                // 'position'      => 'required',
                'is_active'      => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            // 'parent_id.required' => 'Parent field is required.'
        ];
    }
}
