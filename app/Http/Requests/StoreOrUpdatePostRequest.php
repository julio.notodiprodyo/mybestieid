<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreOrUpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        if(request()->postType == 'manual'){
            return [
                'title'     => 'required|max:125',
                'attr_6'    => ['max:60','regex:/^[a-zA-Z0-9_\-]*$/','nullable'],
                'image'     => 'image|mimes:jpg,png,jpeg|max:1000'
            ];
        }else{
            if (Auth::user()->roles->first()->name == 'admin') {
                return [
                    'attr_6'        => ['max:60','regex:/^[a-zA-Z0-9_\-]*$/','nullable'],
                    'title'         => 'required|max:125|string',
                    'image'         => 'image|mimes:jpg,png,jpeg|max:1000'
                ];
            } else {
                return [
                    'attr_6'        => ['max:60','regex:/^[a-zA-Z0-9_\-]*$/','nullable'],
                    'title'         => 'required|max:125|string',
                    'image'         => 'image|mimes:jpg,png,jpeg|max:1000'
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'attr_6.required'            => 'ID/URL Youtube field is required.',
            'attr_6.max'            => 'ID/URL Youtube field may not be greater than 60 characters.',
            'attr_6.regex'            => 'ID/URL Youtube format is invalid.',
        ];
    }
}
