<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateEReferenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'title'     => 'required|max:50|regex:/^[a-zA-Z0-9\s]*$/',
                'is_active' => 'required',
            ];
    }

    public function messages()
    {
        return [
            'title.regex'       => 'Title format is invalid.',
            'title.required'    => 'Title field is required.',
            'title.max'         => 'Title field may not be greater than 60 characters.'
        ];
    }
}
