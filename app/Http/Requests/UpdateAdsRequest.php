<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // var_dump(request()->type);exit();
        if (request()->category == 1) {
            return [
                'title'     => 'required|max:100|regex:/^[a-zA-Z0-9\s]*$/',
                'category'  => 'required',
                'position'  => 'required',
                'script'  => 'required',
            ];
        }else{
            if (request()->type == 0) {
                return [
                    'title'     => 'required|max:100|regex:/^[a-zA-Z0-9\s]*$/',
                    'type'      => 'required',
                    'image'     => 'image|mimes:jpg,png,jpeg,gif|max:1000',
                    'youtube_id'=> ['max:250', 'required', 'regex:/((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\&\.\/\?\:@\-_=#])*/'],
                    'target'    => 'required',
                    'position'  => 'required',
                    'status'    => 'required',
                    'start_date'     => 'required|date',
                    'end_date'       => 'required|date',
                ];
            }else{
                return [
                    'title'     => 'required|max:100',
                    'type'      => 'required',
                    'video'     => 'mimes:mp4,avi,mov,wmv,wma|max:8192',
                    'youtube_id'=> ['max:250', 'required', 'regex:/((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\&\.\/\?\:@\-_=#])*/'],
                    'target'    => 'required',
                    'position'  => 'required',
                    'status'    => 'required',
                    'start_date'     => 'required|date',
                    'end_date'       => 'required|date',
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'youtube_id.regex' => 'The URL format is invalid.',
            'youtube_id.required' => 'URL field is required.',
            'youtube_id.max' => 'URL field may not be greater than 250 characters.'
        ];
    }
}
