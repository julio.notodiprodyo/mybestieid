<?php

use App\Model\Comments;
use App\Model\User;
use App\Model\Content;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fakerID = Faker::create('id_ID');

        // $this->call(UsersTableSeeder::class);
        // $user = Faker::create('App\Model\User');
        // for($i = 1 ; $i <= 1000 ; $i++){
        // 	DB::table('users')->insert([
        //         'is_active'         => 1,
        //         'remember_token'    => str_random(10),
        //         'name'              => $fakerID->name(),
        //         'email'             => $user->unique()->safeEmail,
        //         // 'image'             => $user->imageUrl(null, null, 'people'),
        //         'password'          => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        //     ]);
        // }

        // $rate = Faker::create('App\Model\Reviews');
        // for($i = 1 ; $i <= 1000 ; $i++){
        // 	DB::table('reviews')->insert([
        //         'content_id'        => Content::whereIn('type', ['channel', 'video'])->get()->random()->id,
        //         // 'comment'           => $rate->text,
        //         'rating'            => $rate->numberBetween(1,5),
        //         'user_id'           => User::all()->random()->id,
        //         'created_at'        => \Carbon\Carbon::now()->subDays(rand(0, 90))->format('Y-m-d')
        //     ]);
        // }
        
        // $comment = Faker::create('App\Model\Comments');
        // for($i = 1 ; $i <= 50000 ; $i++){
        	// DB::table('comments')->insert([
            //     'title' => $comment->sentence(),
            //     'content_id' => Content::whereIn('type', ['channel', 'video'])->get()->random()->id,
            //     'comment' => $fakerID->sentence(),
            //     'user_id' => User::all()->random()->id,
            //     'created_at' => \Carbon\Carbon::now(),
            //     'Updated_at' => \Carbon\Carbon::now(),
            // ]);
        // }
    }
}
