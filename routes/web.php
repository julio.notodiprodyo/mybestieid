<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::group(['namespace' => 'Auth'], function() {
//     Route::get('/auth/{provider}', 'CallbackController@redirectToProvider');
//     Route::get('/auth/{provider}/callback', 'CallbackController@handleProviderCallback');
// });

Route::get('/change_auth/{roles}', 'Backend\PersonalController@changeRole')->name('change_auth');
Route::get('/change_account/{id}', 'Backend\PersonalController@changeAccount')->name('change_account');
Route::get('/logout/', 'Backend\PersonalController@logout')->name('pesonal.logout');


Route::group(['namespace' => 'Backend', 'middleware' => ['auth']], function () {
    Route::get('/admin/add_account', 'PersonalController@formAccount')->name('personal.add_account');
    Route::post('/post/add_account', 'PersonalController@postAccount');
});

Route::group(['namespace' => 'Modules'], function () {
    Route::group(['prefix' => 'api'], function () {
        Route::post('/ajax/rating', 'APIController@ajaxRating')->name('ajax.rating'); 
        Route::post('/ajax/filter-data', 'APIController@ajaxFilterData')->name('ajax.filter.data');   
        Route::get('/ajax/download-pdf/{slug}', 'APIController@ajaxDownloadPDF')->name('ajax.download.pdf');   
        Route::post('/ajax/trx/payment', 'APIController@ajaxTrxPayment')->name('ajax.trx.payment'); 
        Route::post('/ajax/transaction/store', 'APIController@createTransaction')->name('createTransaction.subscribe');   
    });
});    

Route::group(['namespace' => 'Website'], function () {
    Route::get('', 'PageController@index')->name('home');
    Route::get('/page/{slug}', 'PageController@page')->name('page.slug');
    Route::get('/player/{slug}/{hsl}', 'PageController@player')->name('player.hsl');
    Route::post('/comment/send', 'PageController@sendComments')->name('comments.send');
    Route::get('/search', 'PageController@search')->name('page.search');
    
    Route::group(['prefix' => 'subscription', 'middleware' => ['auth']], function () {
        Route::get('/{slug}', 'SubscriptionController@subscribe')->name('subscribe');
    });

    Route::match(['GET','POST'], 'contact', 'PageController@contact')->name('contact');
    Route::group(['prefix' => 'channel'], function () {
        Route::get('', 'PageController@channel')->name('channel');
        Route::get('{slug}', 'PageController@detailChannel')->name('channel.slug');
    });
    Route::group(['prefix' => 'vod'], function () {
        Route::get('', 'PageController@vod')->name('vod');
        Route::get('{slug}', 'PageController@detailVod')->name('vod.slug');
    });
    Route::group(['prefix' => 'podcast'], function () {
        Route::get('', 'PageController@podcast')->name('podcast');
        Route::get('{slug}', 'PageController@detailPodcast')->name('podcast.slug');
    });
    Route::group(['prefix' => 'transaction'], function () {
        Route::get('', 'TransactionController@index')->name('index.transaction');
    });
    Route::group(['prefix' => 'e-reference'], function () {
        Route::get('', 'PageController@eReference')->name('eReference');
        Route::get('{slug}', 'PageController@detailEReference')->name('eReference.slug');
    });
    Route::group(['prefix' => 'signup'], function () {
        Route::get('', 'MemberAreaController@index')->name('index.member.registration');
        Route::post('store', 'MemberAreaController@registration')->name('store.member.registration');
    });
    Route::group(['prefix' => 'profile'], function () {
        Route::get('', 'MemberAreaController@profile')->name('profile.member');
    });
    Route::get('/verify/{token}', 'VerifyUserController@verifyUser')->name('verifyUser');    
});

Route::get('bank', 'Backend\BankController@index')->name('bank.index');

Route::fallback(function($e) {
    return view('frontend.component.404', ['404' => '404']);
});

/*++++++++++++++  ROLE ADMINISTRATOR  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::group(['namespace' => 'Backend', 'middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::group(['middleware' => 'role:admin|finance|partner', 'prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('admin.dashboard.index');
    });

    Route::group(['middleware' => 'role:partner', 'prefix' => 'partner'], function () {
        Route::get('', 'ReportController@partner')->name('admin.partner.index');
        Route::post('report-transaction', 'ReportController@reportTransaction')->name('admin.report.transaction.index');
    });

    // Route::group(['middleware' => 'role:admin', 'prefix' => 'google'], function () {
    //     Route::get('', 'GoogleSheetController@index')->name('admin.google.index');
    //     Route::get('/create', 'GoogleSheetController@create')->name('admin.google.create');
    //     Route::get('/Oauth', 'GoogleSheetController@Oauth')->name('admin.google.Oauth');
    //     Route::get('/updateEntry/{file}', 'GoogleSheetController@updateEntry')->name('admin.google.updateEntry');
    //     Route::get('/readSpreadsheet', 'GoogleSheetController@readSpreadsheet')->name('admin.google.readSpreadsheet');
    //     Route::get('/spreadsheet', 'GoogleSheetController@spreadsheet')->name('admin.google.spreadsheet');
    // });

    // Route::get('/addaccount', 'PersonalController@formAccount')->name('personal.addaccount');
    // Route::post('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');
    //Route::get('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');


    Route::group(['middleware' => 'role:admin|finance', 'prefix' => 'channel'], function () {
        Route::get('', 'ChannelsController@index')->name('channel.index');
        Route::get('/create', 'ChannelsController@create')->name('channel.create');
        Route::post('', 'ChannelsController@store')->name('channel.store');
        // Route::get('/test', 'ChannelsController@apa')->name('channel.create');
        Route::get('/{type}/edit', 'ChannelsController@edit')->name('channel.edit');
        Route::put('/{type}', 'ChannelsController@update')->name('channel.update');
        Route::get('/{type}', 'ChannelsController@show')->name('channel.show');
        Route::post('/update-featured', 'ChannelsController@updateFeatured')->name('admin.channel.updateFeatured');
        Route::delete('/{type}', 'ChannelsController@destroy')->name('channel.destroy');
    });

    Route::group(['middleware' => 'role:admin|finance', 'prefix' => 'ads'], function () {
        Route::get('', 'AdsController@index')->name('ads.index');
        Route::get('/create', 'AdsController@create')->name('ads.create');
        Route::post('', 'AdsController@store')->name('ads.store');
        // Route::get('/test', 'AdsController@apa')->name('ads.create');
        Route::get('/{type}/edit', 'AdsController@edit')->name('ads.edit');
        Route::put('/{type}', 'AdsController@update')->name('ads.update');
        Route::get('/{type}', 'AdsController@show')->name('ads.show');
        Route::delete('/{type}', 'AdsController@destroy')->name('ads.destroy');
    });

    Route::group(['middleware' => 'role:admin|finance', 'prefix' => 'slideshow'], function () {
        Route::get('', 'SlideshowController@index')->name('slideshow.index');
        Route::get('/create', 'SlideshowController@create')->name('slideshow.create');
        Route::post('', 'SlideshowController@store')->name('slideshow.store');
        Route::get('/{type}/edit', 'SlideshowController@edit')->name('slideshow.edit');
        Route::put('/{type}', 'SlideshowController@update')->name('slideshow.update');
        Route::get('/{type}', 'SlideshowController@show')->name('slideshow.show');
        Route::delete('/{type}', 'SlideshowController@destroy')->name('slideshow.destroy');
    });
    
    Route::group(['middleware' => 'role:admin|finance', 'prefix' => 'ereference'], function () {
        Route::get('', 'EReferenceController@index')->name('ereference.index');
        Route::get('/create', 'EReferenceController@create')->name('ereference.create');
        Route::post('', 'EReferenceController@store')->name('ereference.store');
        Route::get('/{type}/edit', 'EReferenceController@edit')->name('ereference.edit');
        Route::put('/{type}', 'EReferenceController@update')->name('ereference.update');
        // Route::get('/{type}', 'EReferenceController@show')->name('ereference.show');
        Route::delete('/{type}', 'EReferenceController@destroy')->name('ereference.destroy');
    });

    Route::group(['middleware' => 'role:admin', 'prefix' => 'category'], function () {
        Route::get('', 'CategoryController@index')->name('category.index');
        Route::get('/create', 'CategoryController@create')->name('category.create');
        Route::post('', 'CategoryController@store')->name('category.store');
        Route::get('/{type}/edit', 'CategoryController@edit')->name('category.edit');
        Route::put('/{type}', 'CategoryController@update')->name('category.update');
        // Route::get('/{type}', 'CategoryController@show')->name('category.show');
        // Route::delete('/{type}', 'CategoryController@destroy')->name('category.destroy');
    });

    Route::group(['middleware' => 'role:admin', 'prefix' => 'menu'], function () {
        Route::get('', 'MenuController@index')->name('menu.index');
        Route::get('/create-parent', 'MenuController@create_parent')->name('menu.create-parent');
        Route::get('/create-child', 'MenuController@create_child')->name('menu.create-child');
        Route::post('', 'MenuController@store')->name('menu.store');
        Route::get('/{type}/edit', 'MenuController@edit')->name('menu.edit');
        Route::put('/{type}', 'MenuController@update')->name('menu.update');
        // Route::get('/{type}', 'CategoryController@show')->name('category.show');
        // Route::delete('/{type}', 'CategoryController@destroy')->name('category.destroy');
    });

    Route::group(['middleware' => 'role:admin', 'prefix' => 'account_management'], function () {
        Route::get('', 'AccountManController@index')->name('account_management.index');
        Route::get('delete/{master}/{child}', 'AccountManController@DeleteAction')->name('account_management.delete');
    });

    Route::group(['middleware' => 'role:admin|finance', 'prefix' => 'posts'], function () {
        Route::get('', 'PostsController@index')->name('admin.posts.index');
        Route::get('/create-automatic', 'PostsController@createAutomatic')->name('admin.posts.createAutomatic');
        Route::get('/create-manual', 'PostsController@createManual')->name('admin.posts.createManual');
        Route::post('', 'PostsController@store')->name('admin.posts.store');
        Route::get('/{type}/edit-automatic', 'PostsController@editAutomatic')->name('admin.posts.editAutomatic');
        Route::get('/{type}/edit-manual', 'PostsController@editManual')->name('admin.posts.editManual');
        Route::put('/{type}', 'PostsController@update')->name('admin.posts.update');
        // Route::get('/{type}', 'PostsController@show')->name('posts.show');
        Route::get('checkYoutube', 'PostsController@checkYoutube')->name('admin.posts.check');
        Route::post('/update-popular', 'PostsController@updatePopular')->name('admin.posts.updatePopular');
        Route::post('/get-series', 'PostsController@getSeries')->name('admin.posts.getSeries');
        Route::delete('/{type}', 'PostsController@destroy')->name('admin.posts.destroy');
    });

    Route::group(['middleware' => 'role:admin', 'prefix' => 'config'], function () {
        Route::get('', 'WebConfigController@index')->name('config.index');
        Route::get('/create', 'WebConfigController@create')->name('config.create');
        Route::post('', 'WebConfigController@store')->name('config.store');
        Route::get('/{type}/edit', 'WebConfigController@edit')->name('config.edit');
        Route::put('/{type}', 'WebConfigController@update')->name('config.update');
        Route::get('/{type}', 'WebConfigController@show')->name('config.show');
    });

    Route::group(['middleware' => 'role:admin'], function(){
        Route::resource('user', 'UserController');
    });
    
    Route::group(['middleware' => 'role:admin'], function(){
        Route::resource('role', 'RoleController');
    });

    Route::group(['middleware' => 'role:admin|finance', 'prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('admin.personal.index');
        Route::get('/create', 'PersonalController@create')->name('admin.personal.create');
        Route::post('', 'PersonalController@store')->name('admin.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('admin.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('admin.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('admin.personal.show');

        Route::get('/addaccount', 'PersonalController@logout')->name('admin.personal.addaccount');
    });

    Route::group(['middleware' => 'role:admin', 'prefix' => 'page'], function () {
        Route::get('', 'PageController@index')->name('page.index');
        Route::get('/create', 'PageController@create')->name('page.create');
        Route::post('', 'PageController@store')->name('page.store');
        Route::get('/{id}/edit', 'PageController@edit')->name('page.edit');
        Route::put('/{id}', 'PageController@update')->name('page.update');
        Route::get('/{id}', 'PageController@show')->name('page.show');
    });
});
/*++++++++++++++  END ROLE ADMINISTRATOR  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::group(['namespace' => 'Backend', 'middleware' => ['role:member', 'auth'], 'prefix' => 'my-account'], function () {
    Route::get('profile', 'MemberController@edit')->name('self.member.edit');
    Route::put('/member-update', 'MemberController@update')->name('self.member.update');
    Route::get('/list-transaction', 'MemberController@listTransaction')->name('list.transaction');
});

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Auth::routes();
