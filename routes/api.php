<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('payment-notification-midtrans', 'Modules\APIController@paymentNotificationMidtrans')->name('payment.notification.midtrans');   
Route::post('payment-status-midtrans/{orderId}', 'Modules\APIController@paymentStatusMidtrans')->name('payment.status.midtrans');   


Route::group(['namespace' => 'Modules', 'prefix' => 'v1'], function () use ($router) {
    Route::get('/', 'APIMobileController@homepage');
    Route::get('/channel', 'APIMobileController@channel');
    Route::get('/detail-channel', 'APIMobileController@detailChannel');
    Route::get('/podcast', 'APIMobileController@podcast');
    Route::get('/vod', 'APIMobileController@vod');
    Route::get('/detail-vod', 'APIMobileController@detailVOD');
    Route::get('/e-reference', 'APIMobileController@eReference');

    Route::get('/configuration', 'APIMobileController@configuration');
    Route::get('/search', 'APIMobileController@search');
});

